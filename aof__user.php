<?php 
require('api/inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('api/inc/session_chk.php');
require('_html.head.inc.php');

// $RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
  
?>
<style>
  #overlay{
    position:fixed;
    top:0;
    left:0;
    right:0;
    bottom:0;
    background:rgba(0,0,0,0.6);
    padding-top:200px;
  }
</style>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid">
  <div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main id="app" class="bg-eee col-12 col-md-9 col-xl-10 p-2">

<div>
  <form id="formCategory" name="category">
    <article class="container">
    <!--Add User item | start -->
      <div class="alert alert-danger" style="transition-duration:2s" v-if="errorMsg">{{ errorMsg }}</div>
      <div class="alert alert-success"style="transition-duration:2s" v-if="successMsg">{{ successMsg }}</div>

      <div class="row">
        
        <div class="col-12 col-md-4 d-flex align-items-center py-1">
          <button class="btn btn-block btn-primary" style="cursor:pointer" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">เพิ่มผู้ใช้งาน</button>
        </div>
      </div>
      <div class="collapse mt-3" id="collapseExample">
        <div class="align-items-center w-50">
          <form method="post">
            <div class="form-group">
              <label for="exampleInputEmail1">ชื่อผู้ใช้งาน</label>
              <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="ชื่อผู้ใช้งาน" v-model="newUser.name">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">รหัสผ่าน</label>
              <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="รหัสผ่าน" v-model="newUser.password">
            </div>
            <button type="button" class="btn btn-primary" @click="addUser()">เพิ่มผู้ใช้งาน</button>
          </form>
        </div>
      </div>
    </article>
  </form>
</div>
<hr>
<div class="container">
  <div class="row">
    <div class="col-lg-4">
      <table style="font-size: 0.8rem;" id="editable_table" class="table table-bordered table-striped table-sm bg-white ">
        <thead>
          <tr class="text-center">
            <th width="50">#</th>
            <th>ชื่อ</th>
            <th width="120">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(user,index) in users">
            <td class="text-right align-middle">{{index+1}}</td>
            <td class="font-weight-bold align-middle" style="font-size:1.0rem;">{{ user.username }}</td>
            <td class="text-left align-middle">
              <button class="btn btn-danger btn-sm" type="button" @click="editPassword=true; selectUser(user);">เปลี่ยนรหัสผ่าน</button>
            </td>
          </tr>
          
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- Edit Password -->

<div id="overlay" v-if="editPassword">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">เปลี่ยนรหัสผ่าน</h5>
        <button type="button" class="close" @click="editPassword=false">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-4">
        <form action="#" method="post">
          <div class="form-group">
            <input type="text" name="name" class="form-control" v-model="currentUser.username" readonly>
          </div>
          <div class="form-group">
            <input type="password" name="password" class="form-control" v-model="currentUser.password" placeholder="รหัสผ่านใหม่">
          </div>
          <div class="form-group">
            <button class="btn btn-primary btn-sm btn-block" type="button" @click="editPassword=false; updateUser();">บันทึก</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>





</main>
  
<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>

<script>
  var  app = new Vue({
    el:'#app',
    data:{
      errorMsg:'',
      successMsg:'',
      editPassword:false,
      users:[],
      newUser:{name:"",password:""},
      currentUser:{}
    },
    mounted:function(){
      this.getAllUsers();
    },
    methods:{
      getAllUsers(){
        axios.get('./api/user/index.php?action=read')
          .then(function (response) {
            app.users = response.data.users; 
          })
          .catch(function (error) {
            app.errorMsg = response.data.message;
          })
      },
      addUser(){
        var formData = app.toFormData(app.newUser);
        axios.post('./api/user/index.php?action=create', formData,)
          .then(function(response){
            app.newUser = {name:"",password:""};
            app.successMsg = response.data.message;
            app.getAllUsers();
            setTimeout(() => {
              app.successMsg = '';
            }, 2000);
          })
          .catch(function (error) {
            app.errorMsg = response.data.message;
          })
      },
      updateUser(){
        var formData = app.toFormData(app.currentUser);
        axios.post('./api/user/index.php?action=update', formData,)
          .then(function(response){
            app.currentUser = {};
            if(response.data.error){
              app.errorMsg = response.data.message;
              setTimeout(() => {
                app.errorMsg = '';  
              }, 2000);
            }else{
              app.successMsg = response.data.message;
              app.getAllUsers();
              setTimeout(() => {
                app.successMsg = '';  
              }, 2000);
            }
          })
  
      },
      toFormData(obj){
        var fd = new FormData();
        for(var i in obj){
          fd.append(i,obj[i]);
        }
        return fd;
      },
      selectUser(user){
        app.currentUser = user
      }
    }

  });

</script>