<!-- Header | start -->
<?php // Title header
foreach($CFG_MODULE as $k => $v) { 
  if($SCRIPTNAME_=== $v['url_main'] || in_array($SCRIPTNAME_, $v['url_list']) ) {
    $headerTitle = $CFG_MODULE[$k]['name'];
    $modID = strtoupper($CFG_MODULE[$k]['id']);
  }
}


$sqlBrand = "select brand_name val,brand_name_disp txt,hash from `{$DBNAME__}`.`@master_brand`
 where system_id = {$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']} and record_status = '1' order by crt_dtm;";
 #$qBrand = mysqliQuery($sqlBrand);

// $i = 0;
// foreach($qBrand['res'] as $k => $v) {
//   if($i == 0 && !isset($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname'])) {
//     $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname'] = $v['val'];
//     $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname_disp'] = $v['txt'];
//     $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash'] = $v['hash'];
//   }
//   $i++;
// }
// #echo "xxxx",$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash'];
// unset($i);
?>

<div class="w-100 d-block d-md-none bg-cus1 text-cus1-txt text-center font-weight-bold fsz_rem09 py-1"><?=$CFG_CUST['name']?></div>
<header class="d-flex bg-primary text-white align-items-center">

  <div class="d-none d-md-block"><span class="d-flex px-3 bg-cus1 justify-content-center align-items-center" style="height: 4rem"
  ><span class="text-cus1-txt font-weight-bold fsz_rem15"><?=$CFG_CUST['name']?></span></span></div>
  <span class="d-block pl-3 font-weight-bold fsz_rem15"><?=$headerTitle?></span>

<span class="ml-auto d-flex align-items-center pr-2">
  <span id="profilePhoto" class="d-none d-md-flex rounded-circle bg-white justify-content-center align-items-center"
  ><span class="d-block font-weight-bold fsz_rem20 text-primary"><?=strtoupper($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username'][0])?></span></span>
  
  <div class="btn-group p-1">
    <button type="button" class="btn btn-primary text-white d-inline-block px-2 fsz_rem09 dropdown-toggle"
    data-toggle="dropdown"
    ><?=ucfirst(strtolower($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']))?></button>
    <div class="mt-2 dropdown-menu dropdown-menu-right">
      <h6 class="dropdown-header">User menu</h6>
      <button class="dropdown-item" type="button" onclick="location.href='_sys.changepwd.php'">Change password</button>
      <!-- <div class="dropdown-divider"></div> -->
      
      <!-- <h6 class="dropdown-header">Brand: <b><?//$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname_disp']?></b></h6> -->
      <?php //if($qBrand['c'] > 1) { ?>
      <!-- <button class="dropdown-item" type="button" onclick="location.href='_sys.changebrand.php'">Change brand</button> -->
      <?php //} ?>
      <div class="dropdown-divider"></div>
      <button class="dropdown-item" type="button" onclick="location.href='_sys.signout.php'">Log out</button>
    </div>
  </div>
</span>
</header>
<!-- Header | finish -->


<!-- edit
13-23
46 47 48

 -->