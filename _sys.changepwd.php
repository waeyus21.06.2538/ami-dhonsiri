<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main class="bg-eee col-12 col-md-9 col-xl-8 py-0 px-1">
<!-- Header | start -->
<div class="container mb-2">
  <div class="row">
    <div class="col-md-12 py-3 bg-white font-weight-bold fsz_rem15 pt-4 text-center">Change password</div>
  </div class="row">
</div>
<!-- Header | finish -->


<form name="main" method="post" action="_sys.changepwd.update.php">
<div class="container"><div class="row">

<div class="col-12 col-md-4 p-1">New password (6-20 char)</div>
<div class="col-12 col-md-8 p-1">
  <input class="form-control" id="p1" name="p1" type="password" autofocus required pattern=".{6,20}" autocomplete="new-password">
</div>

<div class="col-12 col-md-4 p-1">Confirm new password</div>
<div class="col-12 col-md-8 p-1">
  <input class="form-control" id="p2" name="p2" type="password" required autocomplete="new-password">
</div>


<div class="col-12 p-1">
  <input class="b p-1 tc form-control" id="systxt" type="text" readonly="1" value="Set a new password">
</div>

<div class="col-12 p-1 d-flex align-items-center text-right">
  <button class="btn btn-info" type="button" onclick="window.history.back();">Back</button>
  <button class="btn btn-secondary ml-auto mr-2" type="reset">Reset</button>
  <button class="btn btn-primary px-4" type="submit">Submit</button>
</div>

</div></div>
</form>
</main>
<!-- MAIN end -->


<?php require('_html.right_menu.inc.php'); ?>
</div></div>
<!-- Body | finish -->


<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>
<script>
function sizingPrep() {

}

window.onresize = function() {
  sizingPrep();
}

function checkPasswordMatch() {
  var password = $("#p1").val();
  var confirmPassword = $("#p2").val();

  if(password != confirmPassword) {
    $("#systxt").val("Passwords do not match!");
  } else {
    $("#systxt").val("Passwords match.");
  }
}

window.addEventListener('keydown', function(e) {
  if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
    if (e.target.nodeName === 'INPUT' && e.target.type !== 'textarea' && e.target.tabIndex != '-1') { 
      e.preventDefault();
      //focusNextElement();
      return false;
    }
  }
}, true);

$(document).ready(function(){
  $("#p1, #p2").keyup(checkPasswordMatch);
  sizingPrep();
});
</script>