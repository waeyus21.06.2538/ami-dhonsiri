<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');
#var_dump($_SESSION);
$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
$username = $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username'];
#echo $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_co'],'<hr>';
$DB = "dhonsiri";
$sqlI = "insert into `{$DB}`.`qo_header_tmp`
select '','',date(now()),now(),'0','Name','','Company'
,'7','credit','{$username}','{$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_co']}','s000','th','position','1','1','1'
from dual where not exists (select * from `{$DB}`.`qo_header_tmp` where username = '{$username}');";

$qI = mysqliQuery($sqlI);
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main class="bg-eee col-12 col-md-9 col-xl-10 p-2">

<div>
  <div id="qoHeader">
  <form name="qo" method="post" target="_blank" action="./api/item/" id="formQo">
    <article class="container">
    <div class="row">
      <div class="col-12 col-md-2 d-flex align-items-center py-1">สินค้า</div>
      <div class="col-12 col-md-6 d-flex align-items-center py-1">
        <select class="w-100" id="itemm" placeholder="2"></select>
      </div>
      
      <div class="col-6 col-md-2 d-flex align-items-center py-1">
        <button type="button" id="btnAddItemQo" disabled
        class="btn btn-primary mr-auto btn-block" onclick="addItemQo()">Add</button>
      </div>
      <div class="col-6 col-md-2 d-flex align-items-center py-1">
        <button class="btn btn-primary mr-auto btn-block" type="button" v-if="records > 0"
        onclick="previewPdf();" target="_blank">Preview</button>
        <button class="btn btn-primary mr-auto btn-block" type="button" v-else
        disabled>Preview</button>
      </div>
      
    </div>
    </article>



    <article class="container" id="qoH">
    <div class="row">
    
      <div class="col-12 col-md-2 d-flex align-items-center">เรียน<input
      type="hidden" id="formQoType" name="type" value="update_header"><input
      type="hidden" name="username" value="<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>"></div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><input v-model="header.to"
      type="text" name="to" class="form-control" placeholder="กรุณาระบุเรียน" required></div>
    
      <div class="col-12 col-md-2 d-flex align-items-center">เบอร์โทร / อีเมล</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><input v-model="header.contact"
      type="text" name="contact" class="form-control" placeholder="กรุณาระบุเบอร์โทร / อีเมล"></div>
      
      <div class="col-12 col-md-2 d-flex align-items-center">ตำแหน่ง</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><input v-model="header.position"
      type="text" name="position" class="form-control" placeholder="กรุณาระบุตำแหน่ง"></div>
      
      <div class="col-12 col-md-2 d-flex align-items-center">แสดงกำหนดส่ง</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1">
      <span><input type="checkbox" v-model="header.show_deliver" value="1" true-value="1" false-value="0" name="show_deliver"> แสดง</span></div>

      <div class="col-12 col-md-2 d-flex align-items-center">บริษัท</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><input v-model="header.company" placeholder="กรุณาระบุบริษัท"
      type="text" name="company" class="form-control" required></div> 

      <div class="col-12 col-md-2 d-flex align-items-center">ยืนราคา (วัน)</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><input v-model="header.price_hold_days" placeholder="กรุณาระบุยืนราคา(วัน)"
      type="number" name="price_hold_days" class="form-control" required></div>      


      <!-- <div class="col-12 col-md-2 d-flex align-items-center">ยืนราคา (วัน)</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><select name="price_hold_days" v-model="header.price_hold_days" class="form-control">
        <option v-for="day in price_hold_days_list" :value="day">{{ day }} วัน</option>
      </select></div> -->
      
      <div class="col-12 col-md-2 d-flex align-items-center">บริษัทที่นำเสนอ</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><select name="header_co" v-model="header.header_co" class="form-control">
        <option v-for="co in coList" :value="co.key1">{{ co.val }}</option>
      </select></div>
      
      <div class="col-12 col-md-2 d-flex align-items-center">ผู้เสนอราคา</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1"><select name="approver" v-model="header.approver" class="form-control">
        <option v-for="name in approver" :value="name.key1">{{ name.val }}</option>
      </select></span>
      </div>


      <div class="col-12 col-md-2 d-flex align-items-center">แสดงส่วนลด</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1">
      <span><input type="checkbox" value="1" v-model="header.show_discount" true-value="1" false-value="0" name="show_discount"> แสดง</span></div>
      
      <div class="col-12 col-md-2 d-flex align-items-center">ชำระเงิน</div>
      <div class="col-12 col-md-4 d-flex align-items-center justify-content-around py-1">
      <span><input type="radio" name="mode" v-model="header.mode" value="credit"> Credit</span>
      <span><input type="radio" name="mode" v-model="header.mode" value="cash"> Cash</span>
      </div>

      
      <div class="col-12 col-md-2 d-flex align-items-center">หมายเหตุ</div>
      <div class="col-12 col-md-10 d-flex align-items-center py-1"><textarea type="text" name="remark" class="form-control" placeholder="กรุณาระบุหมายเหตุ"
      rows="2" v-model="header.remark"></textarea></div>


      <div class="col-12 col-md-2 d-flex align-items-center">แสดงลายเซ็นลูกค้า</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1">
      <span><input type="checkbox" value="1" v-model="header.show_custsign" true-value="1" false-value="0" name="show_custsign"> แสดง</span></div>

      <div class="col-12 col-md-6 d-flex align-items-center justify-content-end py-2">
        <button class="btn btn-primary px-5" type="button" disabled v-if="saveFlag == 0">Save</button>
        <button class="btn btn-primary px-5" type="button" v-else onclick="saveConfirm();">Save</button>
      </div>

      <div class="col-12 col-md-2 d-flex align-items-center">แสดงยอดรวม</div>
      <div class="col-12 col-md-4 d-flex align-items-center py-1">
      <span><input type="checkbox" value="1" v-model="header.show_total" true-value="1" false-value="0" name="show_total"> แสดง</span></div>
    
    
    </div>
    </article>
  </form>
  </div>


<hr>


<style>
table input.form-control, table select.form-control { font-size: 0.9rem; padding: 1px 4px !important; text-align: right; }
</style>




<article id="result">


  <table class="table table-bordered table-sm table-striped bg-white">
    <thead>
      <tr style="text-align: center; font-size: 0.8rem; font-weight: bold;">
          <td width="35">#</td>
          <td width="50">Brand</td>
          <td>สินค้า</td>
          <td width="70">Qty</td>
          <td width="100">Price</td>
          <td width="50">ส่วนลด<br>(%)</td>
          <td width="90">ราคา<br>สุทธิ</td>
          <td width="100">ราคารวม</td>
          <td width="70">กำหนดส่ง<br>(วัน)</td>
          <td width="120">Action</td>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(item,index) in items" style="font-size: 0.85rem;">
          <td class="text-right">{{ index+1 }}</td>
          <td>{{ item.cmdy_code }}</td>
          <td>{{ item.item_code }}
          <br>{{ item.item_name_th }}</td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_qty'"
            name="qty" type="text" class="form-control" :value="item.qty">
          </td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_price'"
            name="price" type="text" class="form-control" :value="item.price">
          </td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_discount'"
            name="discount" type="text" class="form-control" :value="item.discount">
          </td>
          <td class="text-right">{{ (item.price*(100-parseInt(item.discount))/100).toLocaleString(undefined, {minimumFractionDigits: 2}) }}</td>
          <td class="text-right font-weight-bold">{{ (item.qty * item.price*(100-parseFloat(item.discount))/100).toLocaleString(undefined, {minimumFractionDigits: 2}) }}</td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_shipping_day'"
            name="shipping_day" type="number" class="form-control" :value="item.shipping_day">
          </td>          
          <td>
            <button class="btn btn-primary btn-sm" type="button" v-on:click="saveQO(item.cmdy_code+'|'+item.item_code)">บันทึก</button>
            <button class="btn btn-danger btn-sm" type="button" v-on:click="delQO(item.cmdy_code+'|'+item.item_code)">ลบ</button>
          </td>
      </tr>
    </tbody>
    <!--
    <tfoot>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
    </tfoot>
    -->
  </table>

</article>

</div>

</main>



</div></div>
<!-- Body | finish -->


<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>
<script>
function next(){
  var po = document.getElementById('po');
  var poValue = po.options[po.selectedIndex].value;
  //console.log(poValue);
}


var qoH = new Vue({
  el: '#qoHeader',
  data: {
    saveFlag: 0,
    // price_hold_days_list: [7,15,30,60,90,120,150,180],
    coList: [],
    approver: [],
    records: 0,
    header: {
      to: "",
      contact: "",
      company: "",
      approver: "",
      header_co: "",
      header_lang: "",
      mode: "",
      price_hold_days: "",
      remark: "",
      show_discount: "",
      show_deliver: "",
      show_custsign: "",
      show_total: "",
      position: "",
    }
  },
  mounted(){
    this.get()
  },
  methods: {
    getRecords: function() { 
        axios({ method: 'get', url: './api/item/',params: { type: 'qo_header_tmp', username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>', } })
        .then(function(response){
          qoH.records = response.data.result[0].c
        })
    },
    get: function() {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_header_tmp',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
        } //end params
      })
      .then(function(response){
        console.log(response.data)
        
        axios({ method: 'get', url: './api/item/',params: { type: 'qo_co',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>', } })
        .then(function(response){
          console.log(response.data.result)
          qoH.coList = response.data.result
        })
        .catch(function(error){ console.log(error) })
        
        axios({ method: 'get', url: './api/item/',params: { type: 'qo_approver',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',        } })
        .then(function(response){ qoH.approver = response.data.result })
        .catch(function(error){ console.log(error) })

console.log(response.data.result[0]);

        qoH.header = response.data.result[0]
        qoH.records = response.data.result[0].c
        //console.log(qoH.header)
      })
      .catch(function(error){
        console.log(error)
      })
    },
  }
});




var appDet = new Vue({
  el: '#result',
  data: {
    items: []
  },
  mounted(){
    this.get()
  },
  methods: {
    get: function() {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_detail_tmp',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
        } //end params

      })
      .then(function(response){
        appDet.items = response.data.result
        qoH.getRecords()
        //console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    },
    saveQO: function(itemCode) {
      var itemDiscount = document.getElementById(itemCode+'_discount').value
      var itemPrice = document.getElementById(itemCode+'_price').value
      var itemQty = document.getElementById(itemCode+'_qty').value
      var itemShippingDay = document.getElementById(itemCode+'_shipping_day').value
      
      
      
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_detail_tmp_save',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
          item_code: itemCode,
          item_discount: itemDiscount,
          item_price: itemPrice,
          item_qty: itemQty,
          item_shipping_day: itemShippingDay,
        } //end params

      })
      .then(function(response){
        appDet.get()
      })
      .catch(function(error){
        console.log(error)
      })
    },
    
    delQO: function(itemCode) {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_detail_delete',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
          item_code: itemCode,
        } //end params

      })
      .then(function(response){
        console.log( '[' + 'Q20180611111' + '] deleted!' )
        //tt.get()
        appDet.get()
       // console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    }
  }
})

function getItemCode() {
  var item = document.getElementById('itemm');
  var itemValue = item.options[item.selectedIndex].value;
  console.log(itemValue)
  return itemValue
}


function addItemQo() {
  
  var itemCode = getItemCode()
  console.log(itemCode)
  axios({
    method: 'get',
    url: './api/item/',
    params: {
      type: 'insert_item_tmp',
      username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
      item_code: itemCode,
    } //end params

  })
  .then(function(response){
    appDet.get()
    $select[0].selectize.clear()
    // console.log(response)
  })
  .catch(function(error){
    console.log(error)
  })

}


var $select = $('#itemm').selectize({
  valueField: 'item_key',
  searchField: 'item_code',
  placeholder: 'ใส่รหัสอย่างน้อย 4 หลัก',
  onClear: function() {
    $('#btnAddItemQo').attr('disabled',true)
  },
  onDelete: function() {
    $('#btnAddItemQo').attr('disabled',true)
  },
  onItemAdd:  function() {
    $('#btnAddItemQo').attr('disabled',false)
  },
  render: {
    option: function(data, escape) {
      return '<div>'
      + '<strong>'       
      + escape(data.item_code) + ' - ' 
      + '</strong>(' 
      + escape(data.cmdy_name) + ') ' 
      + escape(data.item_name_th)
      + '</div>';
    },
    item: function(data, escape){
      return '<div>'
      + '<strong>'       
      + escape(data.item_code) + '- ' 
      + '</strong>(' 
      + escape(data.cmdy_name) + ') ' 
      + escape(data.item_name_th)
      + '</div>';
    }
  },
  options: [],
  persist: false,
  loadThrottle: 600,
  create: false,
  allowEmptyOption: true,
  load: function(query, callback) {
    if(!query.length) return callback()
    $.ajax({
      url: './api/item/',
      type: 'GET',
      dataType: 'json',
      data: {
        type: 'detail',
        keyword: query,
      },
      error: function() {
        callback();
      },
      success: function(res) {
        callback(res.result);
        console.log(res.result)
      }
    });
  }
})

function previewPdf() {
  var qof = document.getElementById('formQoType')
  var f = document.getElementById('formQo')
  console.log(qof.value)
  qof.value = "update_header"
  console.log(qof.value)
  $('#formQo').submit();
  //f.submit();
  
  var url = 'pdf.php'
  var win = window.open(url, '_blank');
  win.focus();
  qoH.saveFlag = 1
}

function savePdf() {
  var qof = document.getElementById('formQoType')
  var f = document.getElementById('formQo')  
  qof.value = "update_header"
  $('#formQo').submit();
  
  setTimeout(function(){
    qof.value = "gen_qo"
    $('#formQo').submit();
  }, 250);

  
  setTimeout(function(){
    var url = 'index_quotation.php'
    location.href = url
  }, 250);
}

$('form').ajaxForm();





function saveConfirm() {
  var r = confirm("กรุณายืนยันอีกครั้ง")
  if (r == true) {
    console.log('Ok')
    savePdf()
  } else {
    console.log('Noooooo')
  }
}
</script>