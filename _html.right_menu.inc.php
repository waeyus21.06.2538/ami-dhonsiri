<!-- Right Menu | start -->
<div id="rightMenu" class="d-none d-xl-block bg-white col-xl-2 p-2">
  <div class="d-flex px-0 py-2 align-items-center justify-content-center">
    <span class="px-2 w-100 d-block font-weight-bold">Notification</span>
    <a href="index_history.php" class="ml-auto btn btn-sm btn-primary">View all</a>
  </div>

  <ul class="p-2 bg-eee rounded">
    <li class="d-block p-2 fsz_rem08" style="border-left: 4px solid #fff;">No new </li>
  </ul>

</div>
<!-- Right Menu | finish -->