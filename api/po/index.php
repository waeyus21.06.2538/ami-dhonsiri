<?php
require('../inc/require.php');
header('Content-Type: application/json;charset=UTF-8');

$type = isset($_GET['type']) ? $_GET['type'] : 'header';
$po_no = isset($_GET['po_no']) ? $_GET['po_no'] : '999999';
$vndr = isset($_GET['vendor_code']) ? $_GET['vendor_code'] : '000001';


$sql = []; $sqlType = []; $i = 0;
switch($type){
  case 'header':
    $sql[$i] = "select aa.po_no,date,vendor_code,count(seq) c,sum(qty) qty,ab.record_status from (
      select a.*,date,vendor_code from `tesco`.`po_detail` a left join `tesco`.`po_header` b on a.po_no = b.po_no
      ) aa left join `tesco`.`appointment_detail` ab on aa.po_no = ab.po_no and ab.record_status = '0'
      where vendor_code = '{$vndr}' and record_status is null
      group by po_no,date,vendor_code order by po_no;";
    $sqlType[$i] = 'select';
    $i++;
    break;
  case 'insert_appointment':
    $sql[$i] = "INSERT INTO `tesco`.`appointment_detail`(`po_no`,`crt_dtm`) VALUES ('{$po_no}',now());";
    $sqlType[$i] = 'insert';
    $i++;
    break;
    case 'appointment_detail':
    $sql[$i] = "select a.*,b.date
    ,(select sum(qty) from `tesco`.`po_detail` c where a.po_no = c.po_no) qty
    from `tesco`.`appointment_detail` a left join `tesco`.`po_header` b on a.po_no = b.po_no where a.record_status = '0';";
    $sqlType[$i] = 'select';
    $i++;
    break;
    case 'appointment_detail_delete':
    $sql[$i] = "update `tesco`.`appointment_detail` a set record_status = '9',del_dtm = now() where a.record_status = '0' and a.po_no = '{$po_no}';";
    #echo $sql[$i];
    $sqlType[$i] = 'update';
    $i++;
    break;
  default: 
    #echo 2;
}


$res = [];
for($ii=0; $ii<$i; $ii++) {
  if($sqlType[$ii]=='select' || $sqlType[$ii]=='insert' || $sqlType[$ii]=='delete' || $sqlType[$ii]=='update'):
    $res = mysqliQuery($sql[$ii]);
  endif;
}

#var_dump($res);
/*
foreach($res as $key => $val){
  echo $key, ' = ', $val, '<br>';

} */

$res2 = [];
$res2['records'] = $res['c'];
#$res2['startRecord'] = $limitStart+1;
#$res2['endRecord'] = $limitStart+$q['c'];
#$res2['allRecords'] = $res[0]['c'];
#$res2['page'] = $page;
$res2['result'] = $res['res'];
$result = json_encode($res2);
echo $result;