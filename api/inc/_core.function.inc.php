<?php
$lineBreaks = array("<br />","<br>","<br/>","<br />","&lt;br /&gt;","&lt;br/&gt;","&lt;br&gt;");
function br2lb($text) {
  global $lineBreaks;
  $text2 = str_ireplace($lineBreaks, "", $text);  
  echo $text2;
}

// === [ M ] ===
function mysqliCharset($conn,$charset='utf8') {
  $cs1 = "SET character_set_results={$charset}";
  mysqli_query($conn,$cs1) or die('Error query: ' . mysqli_error());
  $cs2 = "SET character_set_client = {$charset}";
  mysqli_query($conn,$cs2) or die('Error query: ' . mysqli_error());
  $cs3 = "SET character_set_connection = {$charset}";
  mysqli_query($conn,$cs3) or die('Error query: ' . mysqli_error());
}

function mysqliConn() {
  global $DBhost__,$DBusr__,$DBpwd__,$DBname__,$DBport__;
  $conn = new mysqli($DBhost__['mysqli'], $DBusr__['mysqli'], $DBpwd__['mysqli'], $DBname__['mysqli'], $DBport__['mysqli']);
  if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
  }
  return $conn;
}


function mysqliQuery($sqlStatement,$params = '') {
  $sqlStatementName = var_name($sqlStatement);
  $queryType = strtolower(substr( trim(trim($sqlStatement)) ,0,6));
  $conn = mysqliConn();
  mysqliCharset($conn);  

  $conn->autocommit(FALSE);
  $stmt = $conn->prepare($sqlStatement);
  if(!$stmt ) {
    $conn->rollback();
    echo '<b>['.$sqlStatementName.']</b> '.mysqli_error($conn);
    echo '<br>'.$sqlStatement; die;
  }

  if($params != '') {
    $param0 = array_column($params,0);
    $param1 = array_column($params,1);
    $types = implode('',$param0);
    array_unshift($param1,$types);
    call_user_func_array( array($stmt, 'bind_param') , refValues($param1));
  }

  if($queryType == 'select'): #select sql

    # http://php.net/manual/en/mysqli-stmt.fetch.php
    $metaResults = $stmt->result_metadata();
    $fields = $metaResults->fetch_fields();
    $statementParams = '';
    foreach($fields as $field) {
      if(empty($statementParams)): $statementParams.="\$column['".$field->name."']";
      else: $statementParams.=", \$column['".$field->name."']";
      endif;
    }
    $statment = "\$stmt->bind_result($statementParams);";
    $rc = eval($statment);
    if(false===$rc) { die('bind_param() failed: ' . htmlspecialchars($stmt->error)); }
  endif;  #select sql

  $rc = $stmt->execute();
  if ( false===$rc ) { die('execute() failed: ' . htmlspecialchars($stmt->error)); }

  if($queryType == 'select'):  #select sql
    $q = array(); $q['res'] = array();
    $stmt->store_result();
    $q['c'] = $stmt->num_rows();
    while($stmt->fetch()) {
      foreach($column as $k => $v) {
        $r[$k] = $v;
      }
      $q['res'][] = $r;
    }
  endif; #select sql

  $stmt->close();
  $conn->commit();
  $conn->close();
  return $q;  
}


function mysqlQuery($sqlStatement,$esc=false) {
  $sqlStatementName = var_name($sqlStatement);
  global $yyyz, $user_id;
  include('_core.conn.mysql.inc.php');
  if($esc) { $sqlStatement = mysqli_real_escape_string($conn,$sqlStatement); }
  $q = mysqli_query($conn, $sqlStatement);
  if($q) {
  } else {
    $mysqlError = mysqli_error($conn);
    die('<hr><center><b>'.$sqlStatementName.':</b> [ '.$sqlStatement.' ] | <b>Mysql Error: </b> [ '.$mysqlError.' ]</center><hr>');
  }
  mysqli_close($conn);
  return $q;
}


function mysqlQueryT($sqlStatement,$esc=false) {
  $sqlStatementName = var_name($sqlStatement);
  global $yyyz, $user_id;
  include('_core.conn.mysql.inc.php');

  $C = count($sqlStatement); $iC = 0;
  $q = mysqli_query($conn, "begin");

  foreach($sqlStatement as $k => $v):
    if($esc) { $v = mysqli_real_escape_string($conn,$v); }
    $qx = mysqli_query($conn, $v);
    if($qx) { $iC++; } else {
      $mysqlError = mysqli_error($conn);
      print('<div style="text-align: center; padding: 8px; border: 1px solid #aaa; margin: 4px;"><b>'.$sqlStatementName.' [ '.$k.' ]:</b> [ '.$v.' ] | <b>Mysql Error: </b> [ '.$mysqlError.' ]</div>');
    }
  endforeach;

  $t = false;
  if($C == $iC) {
    $q = mysqli_query($conn, "commit");
    $t = true;  
  } else {
    $q = mysqli_query($conn, "rollback");
    die('Rolled back.<hr>');
  }
  mysqli_close($conn);
  return $t;
}

function mysqlQueryESC($sqlStatement) {
  $sqlStatementName = var_name($sqlStatement);
  global $yyyz, $user_id;
  include('_core.conn.mysql.inc.php');
  $sqlStatement = mysqli_real_escape_string($conn, $sqlStatement);
  $q = mysqli_query($conn, $sqlStatement);
  if($q) {
  } else {
    $mysqlError = mysqli_error($conn);
    die('<hr><center><b>'.$sqlStatementName.':</b> [ '.$sqlStatement.' ] | <b>Mysql Error: </b> [ '.$mysqlError.' ]</center><hr>');
  }
  mysqli_close($conn);
  return $q;
}


// === [ P ] ===
function pathConverSlash($path) {
  $newPath = str_replace("\\","/",$path);
  $newPathArr = explode('/',$newPath);
  $newPath = join(DIRECTORY_SEPARATOR ,$newPathArr);
  return $newPath;
}

// === [ R ] ===
function refValues($arr){
# https://stackoverflow.com/a/16120923/3676008
  if(strnatcmp(phpversion(),'5.3') >= 0) { //Reference is required for PHP 5.3+
    $refs = array();
    foreach($arr as $key => $value)
      $refs[$key] = &$arr[$key];
      return $refs;
  }
  return $arr;
}

// === [ S ] ===
function swapKEY($type,$in) {
  switch($type) {
    case "MMtoC1": $list = [ "01"=>"A", "02"=>"B", "03"=>"C", "04"=>"D", "05"=>"E", "06"=>"F", "07"=>"G", "08"=>"H", "09"=>"I", "10"=>"J", "11"=>"K", "12"=>"L" ];
      break;
    default: $list = [];
  }
  return $list[$in];
}

function showImg1($path,$filename) {
  $x = str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $path).''.$filename;
  return $x;
}

// === [ V ] ===
function var_name($var) {
  foreach($GLOBALS as $var_name => $value) {
    if ($value === $var) {
      return $var_name;
    }
  }
  return false;
}



?>