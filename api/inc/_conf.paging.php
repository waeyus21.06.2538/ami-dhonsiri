<? // Paging ===========
$PGDefault = 6;
$PGlimit = array();
$PGlimit = [6,12,24,48,96,120];
//for($i = 9; $i < 101; $i++) { if($i%10==0) { array_push($PGlimit,($i)); } }
$countPGlimit = count($PGlimit);

if(($_GET['PGrecords_limit']) && (!$_COOKIE['PGrecords_limit'])) {
  $PGrecords_limit = $_GET['PGrecords_limit'];
  setcookie('PGrecords_limit', $PGrecords_limit, time()+(60*60*24*7));
} else
if((($_GET['PGrecords_limit']) && ($_COOKIE['PGrecords_limit'])) && ($_GET['PGrecords_limit'] != $_COOKIE['PGrecords_limit'])) {
  $_COOKIE['PGrecords_limit'] = $_GET['PGrecords_limit'];
  $PGrecords_limit = $_GET['PGrecords_limit'];
  setcookie('PGrecords_limit', '', 0);
  setcookie('PGrecords_limit', $PGrecords_limit, time()+(60*60*24*7));
} else
if((($_GET['PGrecords_limit']) && ($_COOKIE['PGrecords_limit'])) && ($_GET['PGrecords_limit'] == $_COOKIE['PGrecords_limit'])) {
  $PGrecords_limit = $_GET['PGrecords_limit'];
} else
if(!$_GET['PGrecords_limit'] && ($_COOKIE['PGrecords_limit'])) {
  $PGrecords_limit = $_COOKIE['PGrecords_limit'];
} else
if(!$_GET['PGrecords_limit'] && (!$_COOKIE['PGrecords_limit'])) {
  $PGrecords_limit = $PGDefault;
}

$PGpage = $_GET['PGpage'] ?: 1;


$PGstart = $PGrecords_limit * ($PGpage-1);
$PGstart_1 = $PGstart+1;
$PGend_1 = $PGstart + $PGrecords_limit;
$PGpages_count = ceil($C/$PGrecords_limit);
$PGnext_page1 = explode('/',$_SERVER['SCRIPT_NAME']);
$PGCXX = count($PGnext_page1)-1;
$PGnext_page2 = explode('?',$PGnext_page1[$PGCXX]);
$PGnext_page = $PGnext_page2[0];
?>
<script>
function PGnext() {
 var f = document.getElementById('pagingMain'); var page = document.getElementById('PGpage');
 page.value = parseInt(page.value) + 1; f.submit();}
function PGback() {
 var f = document.getElementById('pagingMain'); var page = document.getElementById('PGpage');
 page.value = parseInt(page.value) - 1; f.submit(); }
function PGgoToPage(PGpage) {
 var f = document.getElementById('pagingMain'); var page = document.getElementById('PGpage');
 page.value = PGpage; f.submit(); }
</script>
