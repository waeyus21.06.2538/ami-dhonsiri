<?php
$sysTable = [];
$sysTable['module'] = "`{$DBNAME__}`.`@@system_module`";
$sysTable['module_page'] = "`{$DBNAME__}`.`@@system_module_page`";

$menu = []; $iMenu = 0;
$sysMenu = "select * from {$sysTable['module']} where record_status = '1';";
$qsMenu = mysqlQuery($sysMenu);
while($rs = mysqli_fetch_assoc($qsMenu)) {
  $menu[$iMenu]['txt'] = $rs['module_name'];
  $menu[$iMenu]['url'] = $rs['filename'].".".$rs['file_ext'];
  $menu[$iMenu]['ico'] = $rs['icon'];
  $menu[$iMenu]['idx'] = $rs['module_code'];
  $iMenu++;
}

$sysFileInfo0 = "select module_code m from {$sysTable['module']} where system_code = '{$SYSCODE__}'
and left(filename,10) like ('{$scriptName10}%') limit 1;";
$qFileInfo0 = mysqlQuery($sysFileInfo0);
$r = mysqli_fetch_assoc($qFileInfo0);
$MOD_IDX_ = $r['m'];

$sysFileInfo = "select icon,module_name mn from {$sysTable['module']}
where system_code = '{$SYSCODE__}' and module_code = '{$MOD_IDX_}' limit 1;";
$qFileInfo = mysqlQuery($sysFileInfo);
$r = mysqli_fetch_assoc($qFileInfo);
$PAGE_ICO_ = $r['icon'];
$PAGE_TITLE_ = $r['mn'];

$sysModPage = "SELECT * FROM {$sysTable['module_page']} where system_code = '{$SYSCODE__}' and module_code = '{$MOD_IDX_}' order by page_code;";
$qModPage = mysqliQuery($sysModPage);
$modPage = []; $iMP = 0;
if($qModPage['c']>0):
  foreach($qModPage['res'] as $k => $v):
    $modPage[$iMP]['txt'] = $v['page_name'];
    $modPage[$iMP]['url'] = $v['filename'].".".$v['file_ext'];
    $modPage[$iMP]['ico'] = $v['icon'];
    $modPage[$iMP]['idx'] = $v['page_code'];
    $iMP++;
  endforeach;
endif;
?>