<?php
global $DBhost__,$DBusr__,$DBpwd__,$DBname__,$DBport__;
$conn = mysqli_connect($DBhost__['mysqli'], $DBusr__['mysqli'], $DBpwd__['mysqli'],$DBname__['mysqli'],$DBport__['mysqli']);
if (mysqli_connect_errno()) { echo "Failed to connect to Database: " . mysqli_connect_error(); }

$cs1 = "SET character_set_results=utf8";
mysqli_query($conn,$cs1) or die('Error query: ' . mysqli_error());
$cs2 = "SET character_set_client = utf8";
mysqli_query($conn,$cs2) or die('Error query: ' . mysqli_error());
$cs3 = "SET character_set_connection = utf8";
mysqli_query($conn,$cs3) or die('Error query: ' . mysqli_error());

?>
