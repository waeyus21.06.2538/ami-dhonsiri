<?php
require('../inc/require.php');
header('Content-Type: application/json;charset=UTF-8');

$DB = "dhonsiri";
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'detail';

#$keyword = isset($_REQUEST['term']) ? $_REQUEST['term'] : '__';
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';

$approver = isset($_REQUEST['approver']) ? $_REQUEST['approver'] : 's000';
$header_co = isset($_REQUEST['header_co']) ? $_REQUEST['header_co'] : '1';

switch($header_co) {
  case '2': $prefix = 'S'; break;
  case '3': $prefix = 'R'; break;
  case '12': $prefix = 'P'; break;
  default: $prefix = 'D';
}

$header_lang = isset($_REQUEST['header_lang']) ? $_REQUEST['header_lang'] : 'th';

$qo_no = isset($_REQUEST['qo_no']) ? $_REQUEST['qo_no'] : '999999';
$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : 'xxx';
$item_code = isset($_REQUEST['item_code']) ? $_REQUEST['item_code'] : '999999';

$items = explode('|',$item_code);

$item_qty = isset($_REQUEST['item_qty']) ? $_REQUEST['item_qty'] : 1;
$item_price = isset($_REQUEST['item_price']) ? $_REQUEST['item_price'] : 1000;
$item_discount = isset($_REQUEST['item_discount']) ? $_REQUEST['item_discount'] : 0;
$item_shipping_day = isset($_REQUEST['item_shipping_day']) ? $_REQUEST['item_shipping_day'] : 7;


$header_to = isset($_REQUEST['to']) ? $_REQUEST['to'] : "";
$header_position = isset($_REQUEST['position']) ? $_REQUEST['position'] : "";
$header_contact = isset($_REQUEST['contact']) ? $_REQUEST['contact'] : "";
$header_company = isset($_REQUEST['company']) ? $_REQUEST['company'] : "";
$header_remark = isset($_REQUEST['remark']) ? $_REQUEST['remark'] : "";
$header_price_hold_days = isset($_REQUEST['price_hold_days']) ? $_REQUEST['price_hold_days'] : "7";
$header_mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : "credit";
$header_show_discount = isset($_REQUEST['show_discount']) ? $_REQUEST['show_discount'] : "0";
$header_show_deliver = isset($_REQUEST['show_deliver']) ? $_REQUEST['show_deliver'] : "0";
$header_show_custsign = isset($_REQUEST['show_custsign']) ? $_REQUEST['show_custsign'] : "0";
$header_show_total = isset($_REQUEST['show_total']) ? $_REQUEST['show_total'] : "0";

/* for paging */
$record['page'] = $_REQUEST['page'] ?: 1;
$record['page_limit'] = $_REQUEST['page_limit'] ?: 10;
$record['start'] = ($record['page']-1)*$record['page_limit'];

#$vndr = isset($_REQUEST['vendor_code']) ? $_REQUEST['vendor_code'] : '000001';

// var_dump($_REQUEST);
// exit();

$sql = []; $sqlType = []; $i = 0;
switch($type){
    case 'detail':
      if( strlen($keyword) >= 4 ) {
        $sql[$i] = "select concat(a.cmdy_code,'|',a.item_code) item_key,ifnull(b.cmdy_name,a.cmdy_code) cmdy_name,a.*
        from `{$DB}`.`@ms_item` a left join `{$DB}`.`@ms_cmdy` b on a.cmdy_code = b.cmdy_code
        where a.record_status = 'A'
        and (a.cmdy_code like '{$keyword}%' or a.item_code like '{$keyword}%' or a.item_name_th like '{$keyword}%') ;";
      } else {
        $sql[$i] = "select '-type more-' cmdy_name;";
      }
        $sqlType[$i] = 'select';
        $i++;
    break;

    case 'insert_item_tmp':
      $sqlPrc = "select default_price d from `{$DB}`.`@ms_item` where cmdy_code = '{$items[0]}' and item_code = '{$items[1]}';";
      $qPrc = mysqliQuery($sqlPrc);
      $prc = ($qPrc['res'][0]['d'] > 1) ? $qPrc['res'][0]['d'] : 999;

      $sql[$i] = "INSERT INTO `{$DB}`.`qo_detail_tmp`(`qo_no`,`username`,`cmdy_code`,`item_code`,`crt_dtm`,`price`,`record_status`)
      VALUES ('','{$username}','{$items[0]}','{$items[1]}',now(),{$prc},'1')
      on duplicate key update
      qo_no = '',`username` = '{$username}',`cmdy_code` = '{$items[0]}',`item_code` = '{$items[1]}',`crt_dtm`= now(),`price` = {$prc},qty = 1,record_status = '1'
      ;";
      $sqlType[$i] = 'insert';
      $i++;
    break;
/*
    case 'qo_header':
    $sql[$i] = "select a.* from `{$DB}`.`qo_header_tmp` a where a.username = '{$username}';";
    $sqlType[$i] = 'select';
    $i++;
    break;
*/ 
    case 'update_header':
      $sql[$i] = "update `{$DB}`.`qo_header_tmp` a set
      `to` = '{$header_to}',contact = '{$header_contact}',company = '{$header_company}', position = '{$header_position}', remark = '{$header_remark}'
      ,price_hold_days = '{$header_price_hold_days}', `mode` = '{$header_mode}'
      ,show_discount = '{$header_show_discount}',show_deliver = '{$header_show_deliver}',show_custsign = '{$header_show_custsign}', show_total = '{$header_show_total}'
      ,header_co = '{$header_co}',approver = '{$approver}',header_lang = '{$header_lang}'
      where a.username = '{$username}';";
      // echo $sql[$i];
      $sqlType[$i] = 'update';
      $i++;
    break;


    case 'qo_approver':
      $sqlCo = "select company from `{$DB}`.`@user_backend` where username = '{$username}';";
      $qCo = mysqliQuery($sqlCo);
      $coList = explode(",",$qCo['res'][0]['company']);
      $sqlApp = "select key1,key2 from `{$DB}`.`@code_general` where keytype = 'qo_approver' and record_status = '1';";
      $qApp = mysqliQuery($sqlApp);
      $appList = []; $appListOK = [];
      $iA = 0;

      foreach($qApp['res'] as $k => $v) {
        $appList[$iA]['code'] = $v['key1'];
        $appList[$iA]['co'] = explode(",",$v['key2']);
        foreach($coList as $kc => $vc) {
          if(in_array($vc,$appList[$iA]['co']) && !in_array($appList[$iA]['code'],$appListOK)):
            $appListOK[] = $appList[$iA]['code'];
          endif;
        }
        $iA++;
      }

      $appListTxt = "'".implode("','",$appListOK)."'";
      $sql[$i] = "select key1,val,val2 from `{$DB}`.`@code_general` where keytype = 'qo_approver' and key1 in ({$appListTxt}) and record_status = '1';";
      $sqlType[$i] = 'select'; $i++;
    break;
    
    case 'qo_co':
      $sqlCo = "select company from `{$DB}`.`@user_backend` where username = '{$username}';";
      $qCo = mysqliQuery($sqlCo);
    
      $sql[$i] = "select key1,val from `{$DB}`.`@code_general` where keytype = 'company_detail' and record_status = '1'
      and key1 in ({$qCo['res'][0]['company']}) and key2 = 'th' order by key1;";
      #echo $sql[$i];
      $sqlType[$i] = 'select';
      $i++;
    break;
    
    case 'qo_header_tmp':
      $sql[$i] = "select a.*
      ,(select count(*) c from `{$DB}`.`qo_detail_tmp` b where b.record_status = '1' and b.username = a.username) c
      from `{$DB}`.`qo_header_tmp` a where a.username = '{$username}';";
      $sqlType[$i] = 'select';
      $i++;
    break;
    
    case 'qo_detail_tmp':
      $sql[$i] = "select a.*,b.cmdy_code,b.item_name_th from `{$DB}`.`qo_detail_tmp` a
      left join `{$DB}`.`@ms_item` b on a.cmdy_code = b.cmdy_code and a.item_code = b.item_code
      where a.username = '{$username}' and a.record_status = '1' order by a.crt_dtm desc;";
      #echo $sql[$i];
      $sqlType[$i] = 'select';
      $i++;
    break;
    
    case 'qo_detail_delete':
      $sql[$i] = "delete from `{$DB}`.`qo_detail_tmp` where username = '{$username}'
      and cmdy_code = '{$items[0]}' and item_code = '{$items[1]}' and record_status = '9';";
      $sqlType[$i] = 'delete';
      $i++;
      $sql[$i] = "update `{$DB}`.`qo_detail_tmp` a set record_status = '9',del_dtm = now() where a.username = '{$username}'
      and cmdy_code = '{$items[0]}' and item_code = '{$items[1]}' and a.record_status = '1';";
      #echo $sql[$i];
      $sqlType[$i] = 'update';
      $i++;
    break;
      
    case 'qo_detail_tmp_save':
      $sql[$i] = "update `{$DB}`.`qo_detail_tmp` a set qty = '{$item_qty}', price = '{$item_price}', discount = '{$item_discount}'
      ,shipping_day = {$item_shipping_day}
      where a.record_status = '1' and a.username = '{$username}'
      and a.cmdy_code = '{$items[0]}' and a.item_code = '{$items[1]}';";
      #echo $sql[$i];
      $sqlType[$i] = 'update';
      $i++;
    break;
    

    case 'qo_list':
      $keywordColumn = [ 'a.qo_no','a.to','a.contact','a.company'];
      if($keyword  != '') {
        $whereKeyword .= " and ( ";
        $i = 0;
        foreach($keywordColumn as $k => $v) {
          if($i > 0) { $whereKeyword .= " or "; }
          $whereKeyword .= " {$v} like ('%{$keyword}%') ";
          $i++;
        }
        $whereKeyword .= " ) ";
      } else { $whereKeyword = ""; }
    
      $sql[$i] = "select --
      a.*,(select count(*) from `{$DB}`.`qo_detail` b where a.qo_no = b.qo_no) c_items --
      from `{$DB}`.`qo_header` a where a.username = '{$username}' {$whereKeyword} order by a.crt_dtm desc
      limit {$record['start']},{$record['page_limit']};";
      #echo $sql[$i];
      $sqlType[$i] = 'select_count';
      $i++;
    break;
    
    case 'gen_qo':
      $sql[$i] = "call `{$DB}`.gen_doc_real('{$prefix}', 'qo','save', ".date('Y').", '".date('m')."', 5,'{$username}',@a);";
      #echo $sql[$i];
      $sqlType[$i] = 'select';
      $i++;
    break;
    
  default: 
    #echo 2;
}

/*
$res = [];
for($ii=0; $ii<$i; $ii++) {
  if($sqlType[$ii]=='select' || $sqlType[$ii]=='insert' || $sqlType[$ii]=='delete' || $sqlType[$ii]=='update'):
    $res = mysqliQuery($sql[$ii]);
  endif;
}
*/

#var_dump($res);
/*
foreach($res as $key => $val){
  echo $key, ' = ', $val, '<br>';

} */

#foreach($_REQUEST as $k => $v) { echo $k,' = ',$v,'<br>'; } echo '<hr>'; var_dump($sql);

$res = [];
for($ii=0; $ii<$i; $ii++) {
  if($sqlType[$ii]=='select' || $sqlType[$ii]=='insert' || $sqlType[$ii]=='delete' || $sqlType[$ii]=='update'):
    $res = mysqliQuery($sql[$ii]);
  elseif($sqlType[$ii]=='select_count'):
    $x = explode("limit",$sql[$ii]);
    $newSql = explode("--",$x[0]);
    #var_dump($newSql);
    $sqlC = $newSql[0] . ' count(*) c '.$newSql[2];
    $qC = mysqliQuery($sqlC);
    $res = mysqliQuery($sql[$ii]);
  endif;
}



$res2 = [];
$res2['records'] = $res['c'];
$res2['allRecords'] = $qC['res'][0]['c'];
$res2['startRecord'] = $record['start']+1;
$res2['endRecord'] = ($record['start']+$record['page_limit'] >= $res2['allRecords'])
 ? $res2['allRecords'] : $record['start']+$record['page_limit'];
$res2['page'] = $record['page']*1;
$res2['totalPage'] = ceil($res2['allRecords']/$record['page_limit']);
$res2['limit'] = $record['page_limit'];
$res2['result'] = $res['res'];
$result = json_encode($res2);
echo $result;