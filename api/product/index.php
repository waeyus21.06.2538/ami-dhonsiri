<?php
require('../inc/require.php');
header('Content-Type: application/json;charset=UTF-8');
// $result = json_encode($res2);
// var_dump($_REQUEST);
// exit();

$DB = "dhonsiri";
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';

$search_item = isset($_REQUEST['itemValue']) ? $_REQUEST['itemValue'] : '';

$item_code = isset($_REQUEST['item_code']) ? $_REQUEST['item_code'] : '';
$cmdy_code = isset($_REQUEST['cmdy_code']) ? $_REQUEST['cmdy_code'] : '';

$item_name_th   = isset($_REQUEST['item_name_th']) ? $_REQUEST['item_name_th'] : '';
$unit           = isset($_REQUEST['unit']) ? $_REQUEST['unit'] : '';
$default_price  = isset($_REQUEST['default_price']) ? $_REQUEST['default_price'] : '';
$co             = isset($_REQUEST['co']) ? $_REQUEST['co'] : '';
$dept           = isset($_REQUEST['dept']) ? $_REQUEST['dept'] : '';

// var_dump($search_item);
// exit();
/* for paging */
$record['page'] = $_REQUEST['page'] ?: 1;
$record['page_limit'] = $_REQUEST['page_limit'] ?: 10;
$record['start'] = ($record['page']-1)*$record['page_limit'];

$sql = []; $sqlType = []; $i = 0; $yus_sql = [];
switch($type){
    case 'backend':
      $where_query = '';
      if(!empty($search_item)){
        $newsearch_item = explode("|",$search_item);
        $where_query .= " and cmdy_code = '{$newsearch_item[0]}' and item_code = '{$newsearch_item[1]}' ";
      }

      $yus_sql[$i] = "select --
      * --
      from `{$DB}`.`@ms_item` where record_status = 'A' $where_query 
      limit {$record['start']},{$record['page_limit']}";

        $sqlType[$i] = 'select_count';
        $i++;
    break;

    case 'item_delete':
      $yus_sql[$i] = "update `{$DB}`.`@ms_item` a set record_status = 'I' where a.cmdy_code = '{$cmdy_code}'
      and a.item_code = '{$item_code}';";

      
      // var_dump($yus_sql[$i]);
      // exit();

      $sqlType[$i] = 'update';
      $i++;
    break;

    case 'add_item':
      $yus_sql[$i] = "INSERT INTO `{$DB}`.`@ms_item`(`record_status`,`cmdy_code`,`item_code`,`item_name_th`,`unit`,`co`,`dept`,`default_price`)
      VALUES ('A', '{$cmdy_code}', '{$item_code}', '{$item_name_th}', '{$unit}', '{$co}', '{$dept}', '{$default_price}')";

      // var_dump($yus_sql[$i]);
      // exit();

      $sqlType[$i] = 'insert';
      $i++;
    break;

    case 'check_item':
      $yus_sql[$i] = "select --
      * --
      from `{$DB}`.`@ms_item` where record_status = 'A' and cmdy_code = '{$cmdy_code}' and item_code = '{$item_code}';"; 

      $sqlType[$i] = 'select_count';
      $i++;
    break;

    case 'update_item':
      $yus_sql[$i] = "update `{$DB}`.`@ms_item` a set item_name_th = '{$item_name_th}',
      unit = '{$unit}', co = '{$co}', dept = '{$dept}', default_price = '{$default_price}' where a.cmdy_code = '{$cmdy_code}'
      and a.item_code = '{$item_code}';";
      
      $sqlType[$i] = 'update';
      $i++;
    break;
    
  default: 
    #echo 2;
}

$res = [];
for($ii=0; $ii<$i; $ii++) {
  if($sqlType[$ii]=='select' || $sqlType[$ii]=='insert' || $sqlType[$ii]=='delete' || $sqlType[$ii]=='update'):
    // $res = mysqliQuery($sql[$ii]);
    $res = mysqliQuery($yus_sql[$ii]);
  elseif($sqlType[$ii]=='select_count'):
    // $x = explode("limit",$sql[$ii]);
    $x = explode("limit",$yus_sql[$ii]);
    $newSql = explode("--",$x[0]);
    // var_dump($newSql);
    // exit();
    $sqlC = $newSql[0] . ' count(*) c '.$newSql[2];
    // var_dump($sqlC);
    // exit();
    $qC = mysqliQuery($sqlC);
    // $res = mysqliQuery($sql[$ii]);
    $res = mysqliQuery($yus_sql[$ii]);
  endif;
}



$res2 = [];
$res2['records'] = $res['c'];
$res2['allRecords'] = $qC['res'][0]['c'];
$res2['startRecord'] = $record['start']+1;
$res2['endRecord'] = ($record['start']+$record['page_limit'] >= $res2['allRecords'])
 ? $res2['allRecords'] : $record['start']+$record['page_limit'];
$res2['page'] = $record['page']*1;
$res2['totalPage'] = ceil($res2['allRecords']/$record['page_limit']);
$res2['limit'] = $record['page_limit'];
$res2['result'] = $res['res'];
$result = json_encode($res2);
echo $result;