<?php
require('../inc/require.php');
header('Content-Type: application/json;charset=UTF-8');

# Result type
$type = isset($_GET['type']) ? $_GET['type'] : 'calendar';

# Default Month and Yesr to current
$month = isset($_GET['month']) ? $_GET['month'] : date('mm');
$newMonth = str_pad($month,  2, 0, STR_PAD_LEFT);
$year = isset($_GET['year']) ? $_GET['year'] : date('YYYY');

# Concat into ISO Date String
$stringDate = "{$year}-{$newMonth}-01";
$firstDate = strtotime($stringDate);

# Find current month's days and the DOW of 1st
$dow = date('w', $firstDate);
$daysNo = cal_days_in_month(CAL_GREGORIAN, $month, $year);

# Find previous month's days
$prevmonthM = date("m",mktime(0,0,0,date("m", strtotime($stringDate))-1,1,date("Y", strtotime($stringDate))));
$prevmonthY = date("Y",mktime(0,0,0,date("m", strtotime($stringDate))-1,1,date("Y", strtotime($stringDate))));
$daysNoPrev = cal_days_in_month(CAL_GREGORIAN, $prevmonthM, $prevmonthY);

# Find total days of current + previous month
$totalDays = $daysNo + $dow;
$weeksNo = ceil($totalDays/7);
// echo $totalDays," ",$weeksNo;


# $weeks = day list, separate into each week
$weeks = [];
# $counterDay = calendar's total, $counterDayInMonth = current month total
$counterDay = 0;
$counterDayInMonth = 0;

# $startCurrentMonth = flag
$startCurrentMonth = 0;



$i = 0; # $i = week's index
for($i; $i < $weeksNo; $i++): # Loop by week

  $ii = 0; #  $ii = day's index, 7 = days in week
  for($ii; $ii<7; $ii++): #loop by day in week
    
    if($i == 0) { # for 1st week
      if($counterDay == $dow) { $startCurrentMonth = 1; }
      if($startCurrentMonth == 1): #for current month
        $weeks[$i][$counterDay-(($i)*7)]['day'] = $counterDayInMonth+1;
        $weeks[$i][$counterDay-(($i)*7)]['month'] = 'current';
        $weeks[$i][$counterDay-(($i)*7)]['week'] = $i+1;
        $counterDayInMonth++;
      else:
        #example (Jun'18): $daysNoPrev - $dow + $ii + 1; // 31 - 5 + 0 + 1 = 27
        $weeks[$i][$counterDay-(($i)*7)]['day'] = $daysNoPrev - $dow + $ii + 1;
        $weeks[$i][$counterDay-(($i)*7)]['month'] = 'prev';
        $weeks[$i][$counterDay-(($i)*7)]['week'] = $i+1;
      endif;
    } else {
      if($startCurrentMonth == 1) {
        if($counterDayInMonth+1 > $daysNo) {
          $weeks[$i][$counterDay-(($i)*7)]['day'] = $counterDayInMonth+1-$daysNo;
          $weeks[$i][$counterDay-(($i)*7)]['month'] = 'next';
          $weeks[$i][$counterDay-(($i)*7)]['week'] = $i+1;
        } else {
          $weeks[$i][$counterDay-(($i)*7)]['day'] = $counterDayInMonth+1;
          $weeks[$i][$counterDay-(($i)*7)]['month'] = 'current';
          $weeks[$i][$counterDay-(($i)*7)]['week'] = $i+1;
        }
        $counterDayInMonth++;
      }
    }
    
    #echo 'week: ',$i+1,' | day: ',$counterDay," | dow: ",$counterDay-(($i)*7),'<br>';
    $counterDay++;
  endfor; #end | Loop by day in week
endfor; #end | Loop by week


$result = [];
$result['weeksNo'] = $i;
$result['dayList'] = $weeks;

echo json_encode($result);