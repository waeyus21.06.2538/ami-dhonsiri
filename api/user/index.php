<?php   
require('../inc/_core.credential.inc.php');
require('../inc/_core.conn.mysql.inc.php');
header('Content-Type: application/json;charset=UTF-8');

$DB =  "dhonsiri";
$result = array('error'=>false);
$action ='';

if(isset($_GET['action'])){
  $action = $_GET['action'];
}

if($action == 'read'){
  $sql = $conn->query("SELECT username FROM `{$DB}`.`@user_backend` WHERE `active`=1");
  $users = array();
  while($row = $sql->fetch_assoc()){
    array_push($users,$row);
  }
  $result['users'] = $users;
}

if($action == 'create'){

  $name = $_POST['name'];
  $password = mysqli_real_escape_string($conn,$_POST['password']);
  $password = password_hash($password,PASSWORD_DEFAULT);

  $sql = $conn->query("INSERT INTO `{$DB}`.`@user_backend`(`system_id`,`username`,`password`,`role`,`active`,`company`) 
                      VALUES (3,'{$name}','{$password}','user','1','1,2') ");
  if($sql){
    $result['message'] = 'เพิ่มผู้ใช้สำเร็จ';
  }else{
    $result['error'] = true;
    $result['message'] = 'ไม่สามารถเพิ่มผู้ใช้ได้';
  }

  $sqlGroup = $conn->query("SELECT MAX(group_id) FROM `{$DB}`.`@user_group`");
  while($rowGroup = $sqlGroup->fetch_assoc()){
    $group =  $rowGroup['MAX(group_id)'];
  }
  $groupId = $group+1;
  $sqlGroupIn = $conn->query("INSERT INTO `{$DB}`.`@user_group`(`system_id`,`group_id`,`username`)
                              VALUES (3,'$groupId','{$name}')");
}

if($action == 'update'){

  $name = $_POST['name'];
  $password = mysqli_real_escape_string($conn,$_POST['password']);
  $password = password_hash($password,PASSWORD_DEFAULT);

  $sql = $conn->query("UPDATE `{$DB}`.`@user_backend` SET `password`='{$password}' WHERE `username`='{$name}'");
  if($sql){
    $result['message'] = 'เปลี่ยนรหัสผ่านสำเร็จ';
  }else{
    $result['error'] = true;
    $result['message'] = 'ไม่สามารถเปลี่ยนรหัสผ่านได้';
  }
}
echo json_encode($result);
?>