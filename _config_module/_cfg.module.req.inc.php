<?php

$CFG_MODULE = [];

$CFG_MODULE['HOM']['id'] = "Hom";
$CFG_MODULE['HOM']['name'] = "หน้าแรก";
$CFG_MODULE['HOM']['url'] = "index_main.php";
$CFG_MODULE['HOM']['url_main'] = "index_main.php";
$CFG_MODULE['HOM']['has-sub'] = 0;
$CFG_MODULE['HOM']['url_list'] = [];

$CFG_MODULE['PUR']['id'] = "Quo";
$CFG_MODULE['PUR']['name'] = "ใบเสนอราคา";
$CFG_MODULE['PUR']['url'] = "index_quotation.php";
$CFG_MODULE['PUR']['url_main'] = "index_quotation.php";
$CFG_MODULE['PUR']['has-sub'] = 0;
$CFG_MODULE['PUR']['url_list'] = [
  "index_quotation.php","index_quotation_create.php"
];

/*
$CFG_MODULE['CUS']['id'] = "Cus";
$CFG_MODULE['CUS']['name'] = "Customer";
$CFG_MODULE['CUS']['url'] = "index_customer.php";
$CFG_MODULE['CUS']['url_main'] = "index_customer.php";
$CFG_MODULE['CUS']['has-sub'] = 0;
*/

// $CFG_MODULE['PRD']['id'] = "Prd";
// $CFG_MODULE['PRD']['name'] = "Product";
// $CFG_MODULE['PRD']['url'] = "index_product.php";
// $CFG_MODULE['PRD']['url_main'] = "index_product.php";
// $CFG_MODULE['PRD']['has-sub'] = 0;
// $CFG_MODULE['PRD']['url_list'] = [
//   "index_product_add.php","index_product_edit.php"
// ];
$CFG_MODULE['PRD']['id'] = "Prd";
$CFG_MODULE['PRD']['name'] = "สินค้า";
$CFG_MODULE['PRD']['url'] = "yus_product.php";
$CFG_MODULE['PRD']['url_main'] = "yus_product.php";
$CFG_MODULE['PRD']['has-sub'] = 0;
$CFG_MODULE['PRD']['url_list'] = [
  "yus_product_add.php","index_product_edit.php"
];


/*
$CFG_MODULE['FRT']['id'] = "Frt";
$CFG_MODULE['FRT']['name'] = "Front";
$CFG_MODULE['FRT']['url'] = "index_front.php";
$CFG_MODULE['FRT']['url_main'] = "index_front.php";
$CFG_MODULE['FRT']['has-sub'] = 1;

  $CFG_MODULE['FRT']['sub'][0]['id'] = "Frt01";
  $CFG_MODULE['FRT']['sub'][0]['name'] = "Carousel";
  $CFG_MODULE['FRT']['sub'][0]['url'] = "index_front_carousel.php";
  $CFG_MODULE['FRT']['sub'][0]['url_main'] = "index_front.php";
  
  $CFG_MODULE['FRT']['sub'][1]['id'] = "Frt02";
  $CFG_MODULE['FRT']['sub'][1]['name'] = "Story";
  $CFG_MODULE['FRT']['sub'][1]['url'] = "index_front_story.php";
  $CFG_MODULE['FRT']['sub'][1]['url_main'] = "index_front.php";
  
  $CFG_MODULE['FRT']['sub'][2]['id'] = "Frt03";
  $CFG_MODULE['FRT']['sub'][2]['name'] = "Page info";
  $CFG_MODULE['FRT']['sub'][2]['url'] = "index_front_info.php";
  $CFG_MODULE['FRT']['sub'][2]['url_main'] = "index_front.php";
*/

// $CFG_MODULE['USR']['id'] = "Usr";
// $CFG_MODULE['USR']['name'] = "ผู้ใช้งาน";
// $CFG_MODULE['USR']['url'] = "index_user.php";
// $CFG_MODULE['USR']['url_main'] = "index_user.php";
// $CFG_MODULE['USR']['has-sub'] = 0;

$CFG_MODULE['USR']['id'] = "Usr";
$CFG_MODULE['USR']['name'] = "ผู้ใช้งาน";
$CFG_MODULE['USR']['url'] = "aof__user.php";
$CFG_MODULE['USR']['url_main'] = "aof__user.php";
$CFG_MODULE['USR']['has-sub'] = 0;
$CFG_MODULE['USR']['url_list'] = [];
/*
$CFG_MODULE['AP']['name'] = "Account Payable";
$CFG_MODULE['AR']['name'] = "Account Receivable";
*/