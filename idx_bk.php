<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="favicon_v.ico">
  <title></title>
  <link rel="stylesheet" href="css/main.css">
  <style>


.grid { position: relative; }
.item { position: absolute; }
.item.muuri-item-hidden { z-index: 0; }
.item.muuri-item-releasing { z-index: 2; }
.item.muuri-item-dragging { z-index: 3; }
.item.muuri-item-dragging .item-content, .item.muuri-item-releasing .item-content { background: #ffe; color: #00f; }
.item-content { border: 2px solid #00f; }


.leftMenu-nav a { color: #888; }
.leftMenu-nav a:hover,.leftMenu-nav a:active { color: #00f; background-color: #ffe; }
.leftMenu-nav a.active { color: #000 !important; background-color: #eef; }
#leftMenu { border-right: 1px solid #ddd; }
#leftSearchBox { border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; }
#leftMenu, #rightMenu { height: calc(100vh - 4rem); top: 4rem; position: sticky; }
header { min-height: 4rem; position: sticky; top: 0; z-index: 1071; }
#profilePhoto { width: 48px; height: 48px; }
  </style>
</head>

<body>

<?php

var_dump($_SESSION);

?>

<header class="d-flex bg-light text-primary align-items-center">

<?php  
// Header
foreach($CFG_MODULE as $k => $v) { if($SCRIPTNAME_===$v['url']) { $headerTitle = $CFG_MODULE[$k]['name']; } }
?>
  <span class="d-flex px-3 bg-cus1 justify-content-center align-items-center" style="height: 4rem"
  ><span class="text-cus1-txt font-weight-bold fsz_rem15"><?=$CFG_CUST['name']?></span></span>
  <span class="d-block pl-3 font-weight-bold fsz_rem15"><?=$headerTitle?></span>

<span class="ml-auto d-flex align-items-center pr-2">
  <span id="profilePhoto" class="d-flex rounded-circle bg-primary justify-content-center align-items-center"
  ><span class="d-block font-weight-bold fsz_rem20 text-white">G</span></span>
  <a href="#" class="text-primary d-inline-block px-2 fsz_rem09">Gonatee</a>
</span>

</header>


<!-- Body | start -->
<div class="container-fluid">
<div class="row">

<!-- Left Menu | start -->
<div id="leftMenu" class="col-12 col-md-3 col-xl-2 px-0">

<div id="leftSearchBox" class="w-100 bg-white px-3 py-2">
  <label for="search" class="d-flex p-1 m-0 fsz_rem08 justify-content-center align-items-center">
    <img src="../img/v_logo.svg" alt="VMS Logo" height="18" class="mr-1">Search</label>
  <input type="text" name="search" class="form-control" placeholder="Search">
</div>


<?php foreach($CFG_MODULE as $k => $v): ?>

<div class="leftMenu-nav w-100 px-0">
  <a href="#" class="d-block px-3 py-2 font-weight-bold fsz_rem09<?=($SCRIPTNAME_===$v['url'])?' active':''?>"><?=$v['name']?></a>
</div>

<?php endforeach; ?>

</div>
<!-- Left Menu | finish -->


<main class="col-12 col-md-9 col-xl-8">


<div class="container">
  <div class="row grid grid-1 p-0 m-0">

<?php foreach($CFG_MODULE as $k => $v) { ?>
  <div class="item col-4 col-md-3 p-2 m-0">
    <div class="item-content px-2 py-4 rounded"><?=$v['name']?></div>
  </div>
<?php } ?>

  </div>

  <div class="row grid grid-2 p-0 m-0">

<?php foreach($CFG_MODULE as $k => $v) { ?>
  <div class="item col-4 col-md-3 p-2 m-0">
    <div class="item-content px-2 py-4 rounded"><?=$v['name']?></div>
  </div>
<?php } ?>

  </div>


</div>


</main>




<!-- Right Menu | start -->
<div id="rightMenu" class="bg-eee d-none d-xl-block col-xl-2">
Right

</div>
<!-- Right Menu | finish -->




</div>
</div>
<!-- Body | finish -->
















<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>
<script>



var grid1 = new Muuri('.grid-1', {
  dragEnabled: true,
  dragContainer: document.body,
//  dragSort: true,
  
  dragSort: function () {
    return [grid1, grid2]
  }
  
});
/*
var grid2 = new Muuri('.grid-2', {
  dragEnabled: true,
  dragContainer: document.body,
  dragSort: true,
});
*/


var grid2Hash = {};
var grid2 = new Muuri('.grid-2', {
  dragEnabled: true,
  dragContainer: document.body,
  dragSort: true
})
.on('receive', function (data) {
  grid2Hash[data.item._id] = function (item) {
    if (item === data.item) {
      var clone = cloneElem(data.item.getElement());
      data.fromGrid.add(clone, {index: data.fromIndex});
      data.fromGrid.show(clone);
    }
  };
  grid2.once('dragReleaseStart', grid2Hash[data.item._id]);
})
.on('send', function (data) {
  var listener = grid2Hash[data.item._id];
  if (listener) {
    grid2.off('dragReleaseStart', listener);
  }
})

function cloneElem(elem) {
  console.log(elem)
  var clone = elem.cloneNode(true);
  //clone.setAttribute('style', 'display:none;');
  //clone.setAttribute('class', 'item');
  //clone.children[0].setAttribute('style', '');
  return clone;
}

</script>