<?php
require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');


require_once('lib/tcpdf/tcpdf.php');


$username = $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username'];
$DOC_NO = isset($_GET['qo_no']) ? $_GET['qo_no'] : 'XXX';

$DB = "dhonsiri";

$sql = "select a.*,b.cmdy_code,b.item_name_th,b.unit from `{$DB}`.`qo_detail` a
 left join `{$DB}`.`@ms_item` b on a.cmdy_code = b.cmdy_code and a.item_code = b.item_code
 where qo_no = '{$DOC_NO}';";
#echo $sql;
$q = mysqliQuery($sql);


$sqlH = "select a.*,date_add(a.qo_date, interval convert(convert(price_hold_days,char),signed) day) end_hold_date
from `{$DB}`.`qo_header` a where a.qo_no = '{$DOC_NO}';";
$qH = mysqliQuery($sqlH);

$sqlCo = "select val,val2,val3,val4,val5 from `{$DB}`.`@code_general` where keytype = 'company_detail' and key1 = '{$qH['res'][0]['header_co']}' and key2 = '{$qH['res'][0]['header_lang']}';";
$qCo = mysqliQuery($sqlCo);

$sqlSigned = "select key1,val,val2,val3 from `{$DB}`.`@code_general` where keytype = 'qo_approver' and key1 = '{$qH['res'][0]['approver']}';";
$qsigned = mysqliQuery($sqlSigned);

$sqlD = "select sum(discount) d from `{$DB}`.`qo_detail` a
 where a.record_status = '2' and a.qo_no = '{$DOC_NO}';";
$qD = mysqliQuery($sqlD);
$disTxt = ($qD['res'][0]['d'] == 0) ? "ราคา" : "ราคา<br>/ส่วนลด";

$config = [];
$config['orientation'] = 'P'; // P, L
$config['unit'] = 'mm';
$config['format'] = 'A4';
$config['unicode'] = true;
$config['encoding'] = 'UTF-8';
$config['diskcache'] = false;
$config['pdfa'] = false;

$config['doc_creator'] = ucfirst($qH['res'][0]['username']);
$config['doc_no'] = $DOC_NO;
$config['doc_author'] = ucfirst($qH['res'][0]['username']);
$config['doc_create_dtm'] = $qH['res'][0]['crt_dtm'];
$config['doc_type'] = 'Quotation';
$config['doc_type_abbr'] = 'QO';
$config['doc_subject'] = 'Quotation No:'.$DOC_NO;
$config['doc_keywords'] = 'Quotation No:'.$DOC_NO;

$config['company_name'] = $qCo['res'][0]['val'];
$config['company_addr1'] = $qCo['res'][0]['val2'];
$config['company_addr2'] = $qCo['res'][0]['val3'];
$config['company_addr3'] = $qCo['res'][0]['val4'];
$config['company_logo'] = $qCo['res'][0]['val5'];


// in mm.
$config['margin_left'] = 15;
$config['margin_right'] = 15;
$config['margin_top'] = 33;
$config['margin_bottom'] = 17;
$config['doc_width'] = 210 - ($config['margin_left'] + $config['margin_right']);
$config['doc_height'] = 297 - ($config['margin_top'] + $config['margin_bottom']);




class MYPDF extends TCPDF {
  public function Header() {
    GLOBAL $config;
    // Logo
    $image_file = $config['company_logo'];
    $this->Image($image_file, 15, 5, 25, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    
    /*
    $image_file = 'draft.jpg';
    $this->Image($image_file, 45, 60, 120, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    */
    
    // Set font
    // Title
    $this->SetX((32+15));
    
    // CODE 128 AUTO
    $this->SetFont('thsarabunnew', 'b', 16);
    $this->SetXY(155,26);
    $this->Cell(0, 0, "เลขที่: ".$config['doc_no'], 0, 1);
    
    $this->SetXY(139,17);
    $this->write1DBarcode($config['doc_no'], 'C128', '', '', '', 8, 0.4, $style, 'N');
    
    $this->SetFont('thsarabunnew', 'b', 36);
    $this->SetXY(148,5);
    $this->Cell(0, 0, "ใบเสนอราคา", 0, 1);
    
    $this->SetFont('thsarabunnew', 'b', 24);
    $this->SetXY(45,8);
    $this->Cell(0, 0, $config['company_name'], 0, 1);
    
    $this->SetFont('thsarabunnew', '', 14);
    $this->SetXY(45,17);
    $this->Cell(0, 0, $config['company_addr1'], 0, 1);
    $this->SetXY(45,21);
    $this->Cell(0, 0, $config['company_addr2'], 0, 1);
    
    $this->SetXY(45,25);
    $this->Cell(0, 0, $config['company_addr3'], 0, 1);
    
    $this->SetXY(15,26);
    $this->SetDrawColor(160,160,160);
    $this->MultiCell(178, 5, '', B, 'R', 0, 1, '', '', true);
  }

  // Page footer
  public function Footer() {
    GLOBAL $config;
    // Position at 15 mm from bottom
    $this->SetXY(15,282);
    $this->SetDrawColor(160,160,160);
    $this->MultiCell(178, 5, '', T, 'R', 0, 1, '', '', true);
    // Set font
    $this->SetFont('thsarabunnew', '', 12);
    $this->SetY(280);
    // Page number
    $this->SetDrawColor(0,0,0);
    $this->Cell(0, 10, 'หน้า '.$this->getAliasNumPage().' / '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    
    $this->SetXY(15,280);
    $this->Cell(0, 10, 'จัดทำโดย '.$config['doc_author'].' ('.$config['doc_create_dtm'].')', 0, false, 'L', 0, '', 0, false, 'T', 'M');
  }
}


// create new PDF document
$pdf = new MYPDF($config['orientation'],$config['unit'],$config['format'],$config['unicode'],$config['encoding'],$config['diskcache'],$config['pdfa']);

// set document information
$pdf->SetCreator($config['doc_creator']);
$pdf->SetAuthor($config['doc_author']);
$pdf->SetTitle($config['doc_type'].' - '.$DOC_NO);
$pdf->SetSubject($config['doc_subject']);
$pdf->SetKeywords($config['doc_keywords']);

// Margin and breaks
$pdf->SetMargins($config['margin_left'],$config['margin_top'],$config['margin_right']);
$pdf->SetAutoPageBreak(true, $margin = 15 );


$pdf->setFontSubsetting(false);


#echo 1;
$pdf->AddPage();







$y = substr($qH['res'][0]['qo_date'],0,4)+543;
$yH = substr($qH['res'][0]['end_hold_date'],0,4)+543;

$dt = substr($qH['res'][0]['qo_date'],8,2).'/'.substr($qH['res'][0]['qo_date'],5,2).'/'.$y;
$dtH = substr($qH['res'][0]['end_hold_date'],8,2).'/'.substr($qH['res'][0]['end_hold_date'],5,2).'/'.$yH;





$tbl .= <<<EOD
<table cellspacing="0" cellpadding="2" border="0">
<tr class="bd" style="font-size: 16px;">
  <td width="395"></td>
  <td width="110">วันที่:&nbsp;&nbsp;{$dt}</td>
</tr>
<tr class="bd" style="font-size: 16px;">
  <td width="75">เรียน</td>
  <td width="430">{$qH['res'][0]['to']}</td>
</tr>
<tr class="bd" style="font-size: 16px;">
  <td width="75">เบอร์/อีเมล</td>
  <td width="430">{$qH['res'][0]['contact']}</td>
</tr>
<tr class="bd" style="font-size: 16px;">
  <td width="75">บริษัท</td>
  <td width="430">{$qH['res'][0]['company']}</td>
</tr>
</table>
EOD;

$pdf->SetFont('thsarabunnew', 'b', 24);
$pdf->writeHTML($tbl, false, false, false, false, '');



$tbl = <<<EOD
<table cellspacing="0" cellpadding="2" border="0">
<tr class="bd" style="font-size: 20px; text-align: center">
  <td width="505">บริษัทฯ มีความยินดีขอเสนอราคาสินค้าและบริการ ดังนี้</td>
</tr>
</table>
EOD;

$pdf->SetFont('thsarabunnew', 'b', 40);
$pdf->writeHTML($tbl, 0, false, false, false, '');






# Table start

$tbl = <<<EOD
<style>
tr.bd td { border: 1px solid #888; background-color: #ddd; }
td { border: 0px solid #777; }
tr.row0 td { background-color: #fafafa; }
</style>
<table cellspacing="0" cellpadding="2" border="0">
EOD;

if($qH['res'][0]['show_discount'] == '0') {
  $tbl .= '<thead>
  <tr class="bd" style="font-size: 16px; text-align: center; font-weight: bold;">
    <td width="30">ลำดับ</td>
    <td width="35">ยี่ห้อ</td>
    <td width="225">สินค้า</td>
    <td width="45">จำนวน</td>
    <td width="65">ราคาสุทธิ</td>
    <td width="65">ราคารวม</td>
    <td width="40">กำหนด<br>ส่ง</td>
  </tr>
</thead>
<tbody>';
} else {
  $tbl .= '<thead>
  <tr class="bd" style="font-size: 16px; text-align: center;">
    <td width="30">ลำดับ</td>
    <td width="35">ยี่ห้อ</td>
    <td width="160">สินค้า</td>
    <td width="45">จำนวน</td>
    <td width="65">'.$disTxt.'</td>
    <td width="65">ราคาสุทธิ</td>
    <td width="65">ราคารวม</td>
    <td width="40">กำหนด<br>ส่ง</td>
  </tr>
</thead>
<tbody>';

}




$i = 1;
$sumTotal = 0;

#for($i; $i<=40; $i++) {
foreach($q['res'] as $k => $v) {
  $ii = $i%2;
  $finalPrc = $v['price']*((100-$v['discount'])/100);
  $total = $v['qty']*$finalPrc;
  $total2 = number_format($total,2);
  $finalPrc2 = number_format($finalPrc,2);
  
  $discountTxt = ($v['discount'] > 0) ? "<br>{$v['discount']}%" : "";
  $shipTxt = ($v['shipping_day'] == '0') ? "ทันที": $v['shipping_day']."  วัน";
  
  
if($qH['res'][0]['show_discount'] == '0') {  
  $tbl .= <<<EOD
<tr nobr="true" class="row$ii" style="font-size: 16px;">
  <td width="30" style="text-align: right;">$i</td>
  <td width="35" style="text-align: center;">{$v['cmdy_code']}</td>
  <td width="225" class="itemTop" colspan="2">{$v['item_code']}
  <br>&nbsp;&nbsp;{$v['item_name_th']}</td>
  <td width="45" style="text-align: right;">{$v['qty']} {$v['unit']}</td>
  <td width="65" style="text-align: right;">{$finalPrc2}</td>
  <td width="65" style="text-align: right;">{$total2}</td>
  <td width="40" style="text-align: right;">{$shipTxt}</td>
</tr>
EOD;
} else {
  $tbl .= <<<EOD
<tr nobr="true" class="row$ii" style="font-size: 16px;">
  <td width="30" style="text-align: right;">$i</td>
  <td width="35" style="text-align: center;">{$v['cmdy_code']}</td>
  <td width="160" class="itemTop" colspan="2">{$v['item_code']}
  <br>&nbsp;&nbsp;{$v['item_name_th']}</td>
  <td width="45" style="text-align: right;">{$v['qty']} {$v['unit']}</td>
  <td width="65" style="text-align: right;">{$v['price']}{$discountTxt}</td>
  <td width="65" style="text-align: right;">{$finalPrc2}</td>
  <td width="65" style="text-align: right;">{$total2}</td>
  <td width="40" style="text-align: right;">{$shipTxt}</td>
</tr>
EOD;
}

  $sumTotal = $sumTotal + $total;
  $i++;
}

$sumTotal2 = number_format($sumTotal,2);

$vatTotal = number_format($sumTotal*0.07,2);
$sumTotalVat = number_format($sumTotal*1.07,2);

$tbl .= <<<EOD
</tbody>
</table>
EOD;





$pdf->SetFont('thsarabunnew', '', 10);
$pdf->writeHTML($tbl, false, false, false, false, '');




$i = $i-1;
$tbl = <<<EOD
<style>
tr.bd td { border: 1px solid #888; background-color: #ddd; }
</style>
<table cellspacing="0" cellpadding="2" border="0">
  <tr nobr="true" class="bd" style="font-size: 16px; text-align: center; font-weight: bold;">
    <td width="335" style="text-align: right;">สินค้าและบริการทั้งหมด [  $i  ] รายการ รวมราคา
      <br>ภาษีมูลค่าเพิ่ม 7%
      <br>ราคาสุทธิ (รวมภาษี)
    </td>
    <td width="130" style="text-align: right;">$sumTotal2
      <br>$vatTotal
      <br>$sumTotalVat
    </td>
    <td width="40"></td>
  </tr>
</table>
EOD;

$pdf->SetFont('thsarabunnew', '', 10);
$pdf->writeHTML($tbl, true, false, false, false, '');









#$priceDt = "29/06/2561";
$cashRemark = ($qH['res'][0]['mode'] == 'cash') ? "<br>ราคานี้สำหรับเงินสดเท่านั้น": "";
$remark = isset($qH['res'][0]['remark']) ? "<br>".str_replace("\n","<br>",$qH['res'][0]['remark']) : "";

# Remark
$tbl = <<<EOD
<table cellspacing="0" cellpadding="2" border="0">
<tr class="bd" style="font-size: 16px;">
  <td width="75">หมายเหตุ</td>
  <td width="430">กำหนดยืนราคา {$qH['res'][0][price_hold_days]} วัน (ถึงวันที่ {$dtH}){$cashRemark}{$remark} </td>
</tr>
</table>
EOD;


$pdf->writeHTML($tbl, true, false, false, false, '');

$filename = file_exists('signed/'.$qsigned['res'][0]['key1'].'.jpg') ? 'signed/'.$qsigned['res'][0]['key1'].'.jpg' : 'signed/s000.jpg';

# Signed
$tbl = <<<EOD
<table cellspacing="0" cellpadding="2" border="0">
<tr nobr="true" class="bd" style="font-size: 16px;">
  <td width="305"></td>
  <td width="200" style="text-align: center;">ผู้เสนอราคา
  <br><br><img src="{$filename}" height="60">
  <br>.................................................
  <br>{$qsigned['res'][0]['val2']}
  <br>{$qsigned['res'][0]['val3']}
  </td>
</tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');


$path = $_SERVER['DOCUMENT_ROOT']."/dhonsiri/_generated/doc/qo/".date('Y')."/".date('m')."/".date('d')."/";
$outfile = $path.$DOC_NO.'.pdf';
if(!file_exists($path)) { mkdir($path, 0777, true); }
if(!file_exists($outfile)) { $pdf->Output($outfile, 'F'); }

$pdf->Output($DOC_NO.'.pdf', 'I');