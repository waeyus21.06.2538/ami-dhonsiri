<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;

$title = "Manage inventory";
$product = $_GET['product_code'] ?: "";
?>
<body>
<form name="main" action="index_product_inven.if.exec.php" method="post">

<div class="container-fluid"><div class="row">
<?php include('index_product_iframe_title.if.php'); ?>

<!-- action | start -->


<style>
.dz-preview.dz-image-preview { background: none !important; margin: 0 0 8px 8px !important; }
.dz-remove { color: #ff0 !important; }
</style>

  <div class="col-4 px-2 py-1 m-0">
    <button type="submit" class="btn btn-primary col-6">Save</button>
  </div>

</div>


<div class="row bg-white pt-0 pb-3 px-2">
<?php

$sqlSize = "select key1val v,key1ref r from `vms_master`.`@cfg_inventory_dimension`
 where system_id = {$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']}
 and brand_name = '{$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname']}' and key1 = 'size' order by ordering;";
$qSize = mysqliQuery($sqlSize);

$sqlColor = "select key1val v,key1ref r from `vms_master`.`@cfg_inventory_dimension`
 where system_id = {$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']}
 and brand_name = '{$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname']}' and key1 = 'color' order by ordering;";
$qColor = mysqliQuery($sqlColor);
?>



<table class="w-100 table table-sm table-borered  fsz_rem08">
<thead class="thead-dark">
  <tr>
    <th>Color/Size</th>
<?php foreach($qSize['res'] as $k => $v) { ?>
    <th class="text-center font-weight-bold"><?=$v['v']?></th>
<?php } ?>
  <tr>
</thead>
<tbody>
<?php foreach($qColor['res'] as $k => $v) { ?>
  <tr>
    <td class="d-flex align-items-center">
      <span class="d-inline-block mr-1"
      style="width: 20px; height: 20px; border-radius: 50%; border: 1px solid #000; background-color: #<?=$v['r']?>;"></span>
      <span><?=$v['v']?></span>
    </td>
  <?php foreach($qSize['res'] as $kk => $vv) {
    $q = 0;
    $sqlInven = "select q from (select qty q from `vms_master`.`@inventory`
    where 1=1 and system_id = {$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']}
    and brand_name = '{$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname']}' and product_code = '{$product}'
    and key1val = '{$vv['v']}' and key2val = '{$v['v']}' union select 0 q ) a limit 1";
    #echo $sqlInven;
    $qInven = mysqliQuery($sqlInven);
    $q = $qInven['res'][0]['q'];
    
    ?>
    <td>

    <div class="input-group">
      <span class="input-group-prepend d-flex align-items-center">
        <span class="px-1 input-group-text"><span class="d-block font-weight-bold"><?=$vv['v']?></span></span>
      </span>
    
    <input name="inv|<?=$v['v']?>|<?=$vv['v']?>"
    class="fsz_rem07 form-control text-right" style="min-width: 50px; border-left: 4px solid #<?=$v['r']?> !important;"
    value="<?=$q?>" type="number" min="0" max="10000">
    </div>
    
    
    </td>
  <?php } ?>
  </tr>
<?php } ?>
</tbody>
<!--
<tfoot>


</tfoot>
-->
</table>

<input name="product_code" value="<?=$product?>" type="hidden">

<!-- action | finish -->


</div></div>



</form>

<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>



<script>
<?php include('index_product_iframe_title.vue.php'); ?>



function moveBlock(movingType,classname,obj) {
  var numItems = $('.'+classname).length;
  var listItem = document.getElementById(obj);
  var listNo = $('.'+classname).index(listItem);

  if(numItems > 1) {

  if(movingType == 'before') {
    var newNo = listNo-1;
    if(newNo >= 0) {
      $('#'+obj).insertBefore( $('.'+classname+':eq('+newNo+')' ) );
      //console.log(newNo);
      //ajaxCall('movingBlock','',id,'POST','',movingType,newNo);
    }
  } else if(movingType == 'after') {
    var newNo = listNo+1;
    if(newNo < numItems) {
      $('#'+obj).insertAfter( $('.'+classname+':eq('+newNo+')' ) );
      //ajaxCall('movingBlock','',id,'POST','',movingType,newNo);
    }
  }
  
  //var d = document.getElementsByClassName(classname)
  var photo_ids = []
  
  $('.'+classname).each(function() {
    //console.log( $(this).attr('data-photo-id') )
    photo_ids.push($(this).attr('data-photo-id'))
    //$(this).removeClass('d-none');
    //$(this).addClass('d-'+display);
  });
  
  var params = {
    usr: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
    photo_ids: photo_ids,
    type: 'setPhotoOrdering',
  }
  axiosCallReload(params,'./api/product/','noreload')
  console.log( photo_ids )
  
  }
}



<?php switch($RESULT) {
  case 1: $notifyTxt = "Inventory updated successfully!"; break;
  default: $notifyTxt = "";
} ?>

$.notify('<?=$notifyTxt?>', {
  position: 'bottom center',
  className: 'success'
});

<?php if($RESULT>0) { ?>window.history.replaceState(null, null, window.location.pathname);<?php } ?>
</script>