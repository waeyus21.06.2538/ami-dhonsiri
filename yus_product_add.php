<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$DB = "dhonsiri";
$is_edit = isset($_GET['item_code']) ? 1:0;
$is_cmdy_code = isset($_GET['cmdy_code']) ? $_GET['cmdy_code']:'';
$is_item_code = isset($_GET['item_code']) ? $_GET['item_code']:'';

// $title = ($is_edit == 0) ? "Add new product": "Update product";
$title = ($is_edit == 0) ? "เพิ่มสินค้า": "แก้ไข้สินค้า";

$yus_sql = "select * from `{$DB}`.`@ms_item` where item_code = '{$is_item_code }' and cmdy_code = '{$is_cmdy_code}';";
$res = mysqliQuery($yus_sql);
// echo '<pre>';
// var_dump($res['res'][0]['co']);
// echo '</pre>';
?>

<body>
  <?php require('_html.header.inc.php'); ?>
  <!-- Body | start -->
  <div class="container-fluid">
    <div class="row">
      <?php require('_html.left_menu.inc.php'); ?>


      <main class="bg-eee col-12 col-md-9 col-xl-10 p-0"
        style="border-right: 1px solid #ddd; border-left: 1px solid #ddd;">
        <!-- Header | start -->
        <div class="container mb-2">
          <div class="row">
            <div class="col-md-12 py-3 bg-white font-weight-bold fsz_rem15 pt-4 text-center"><?=$title?></div>
          </div>
          <!-- Header | finish -->

          <form id="form-submit-product" action="#" method="post">
            <div class="container">
              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
                  for="th_productName">รหัสสินค้า</label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <input type="text" class="form-control" id="item_code" name="item_code" placeholder="รหัสสินค้า"
                    value="<?=!empty($res['res'][0]['item_code'])? $res['res'][0]['item_code'] : ''?>" required
                    <?=$is_edit == 0? '' : 'disabled';?>>
                  <div
                    class="input-group-append text-center align-items-center <?=$is_edit == 0? '' : 'd-none';?>">
                    <small class="text-danger ml-2">กรุณาป้อนรหัสสินค้าและยี่ห้อ <br>แล้วกดตรวจสอบ </small>
                  </div>
                </div>
              </div>

              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
                  for="th_productName">ยี่ห้อ</label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <input type="text" class="form-control" id="cmdy_code" name="cmdy_code" placeholder="ยี่ห้อ"
                    value="<?=!empty($res['res'][0]['cmdy_code'])? $res['res'][0]['cmdy_code'] : ''?>" required
                    <?=$is_edit == 0? '' : 'disabled';?>>
                  <div
                    class="input-group-append text-center align-items-center <?=$is_edit == 0? '' : 'd-none';?>">
                    <button style="width: 155px;" type="button" id="btn-check" class="btn btn-primary ml-2">ตรวจสอบ</button>
                  </div>
                </div>
              </div>
              <hr>

              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
                  for="th_productName">ชื่อสินค้า</label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <input type="text" class="form-control" id="" name="item_name_th" placeholder="ชื่อสินค้า"
                    value="<?=!empty($res['res'][0]['item_name_th'])? $res['res'][0]['item_name_th'] : ''?>" required>
                  <!-- <div class="input-group-append fsz_rem06 d-none d-sm-flex text-right align-items-center">
                    <span class="w-100 p-1" id="th_productName_counter">100/100</span>
                  </div> -->
                </div>
              </div>

              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
                  for="th_productName">หน่วย</label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <input type="text" class="form-control" id="" name="unit" placeholder="หน่วย"
                    value="<?=!empty($res['res'][0]['unit'])? $res['res'][0]['unit'] : ''?>" required>
                </div>
              </div>

              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
                  for="th_productName">ราคา</label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <input type="number" class="form-control" id="" name="default_price" placeholder="ราคา"
                    value="<?=!empty($res['res'][0]['default_price'])? $res['res'][0]['default_price'] : ''?>" required>
                </div>
              </div>

              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center" for="th_productName">บริษัท
                  <?php echo $res['res'][0]['co']; ?></label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <select name="co" class="form-control" required>
                    <?php 
                      if(!empty($res['res'][0]['co'])){
                        if($res['res'][0]['co'] == '1'):
                          echo '<option value="">--เลือก--</option>';
                          echo '<option value="1" selected>บริษัท ธนศิริดีเซล จำกัด</option>';
                          echo '<option value="2">บริษัท ธนเสริม จำกัด</option>';
                          echo '<option value="3">บริษัท รีแดท (ไทยเเลนด์) จำกัด</option>';
                        endif;

                        if($res['res'][0]['co'] == '2'):
                          echo '<option value="">--เลือก--</option>';
                          echo '<option value="1">บริษัท ธนศิริดีเซล จำกัด</option>';
                          echo '<option value="2" selected>บริษัท ธนเสริม จำกัด</option>';
                          echo '<option value="3">บริษัท รีแดท (ไทยเเลนด์) จำกัด</option>';
                        endif;

                        if($res['res'][0]['co'] == '3'):
                          echo '<option value="">--เลือก--</option>';
                          echo '<option value="1">บริษัท ธนศิริดีเซล จำกัด</option>';
                          echo '<option value="2">บริษัท ธนเสริม จำกัด</option>';
                          echo '<option value="3" selected>บริษัท รีแดท (ไทยเเลนด์) จำกัด</option>';
                        endif;
                      }else{
                        echo '<option value="">--เลือก--</option>';
                        echo '<option value="1">บริษัท ธนศิริดีเซล จำกัด</option>';
                        echo '<option value="2">บริษัท ธนเสริม จำกัด</option>';
                        echo '<option value="3">บริษัท รีแดท (ไทยเเลนด์) จำกัด</option>';
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="row py-1">
                <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
                  for="th_productName">แผนก</label>
                <div class="input-group col-12 col-sm-9 px-2">
                  <input type="text" class="form-control" id="" name="dept" placeholder="แผนก"
                    value="<?=!empty($res['res'][0]['dept'])? $res['res'][0]['dept'] : ''?>" required>
                </div>
              </div>

            </div>

            <div class="container">
              <div class="row">
                <div class="col-12 p-1">
                  <input type="hidden" name="type" value="<?=$is_edit == 0? 'add_item' : 'update_item';?>">
                  <?php if($is_edit != 0){?>
                  <input type="hidden" name="item_code"
                    value="<?=!empty($res['res'][0]['item_code'])? $res['res'][0]['item_code'] : ''?>">
                  <input type="hidden" name="cmdy_code"
                    value="<?=!empty($res['res'][0]['cmdy_code'])? $res['res'][0]['cmdy_code'] : ''?>">
                  <?php }?>
                  <button id="submit" class="btn btn-primary btn-block px-5" type="submit"
                    <?=$is_edit == 0? 'disabled' : '';?>>บันทึก</button>
                </div>
              </div>
            </div>
          </form>

          <!-- --------------modal แสดงรายละเอียดสินค้า---------------------- -->
          <div class="modal fade" id="exampleModalCenter" style="z-index: 9999;" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">รายละเอียดสินค้า</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th class="border-0">รหัสสินค้า</th>
                        <td id="text_item_code" class="text-left border-0">---</td>
                      </tr>
                      <tr>
                        <th class="border-0">ยี่ห้อ</th>
                        <td id="text_cmdy_code" class="text-left border-0">---</td>
                      </tr>
                      <tr>
                        <th class="border-0">ชื่อสินค้า</th>
                        <td id="text_item_name_th" class="text-left border-0">---</td>
                      </tr>
                      <tr>
                        <th class="border-0">หน่วย</th>
                        <td id="text_unit" class="text-left border-0">---</td>
                      </tr>
                      <tr>
                        <th class="border-0">ราคา</th>
                        <td id="text_default_price" class="text-left border-0">---</td>
                      </tr>
                      <tr>
                        <th class="border-0">บริษัท</th>
                        <td id="text_co" class="text-left border-0">---</td>
                      </tr>
                      <tr>
                        <th class="border-0">แผนก</th>
                        <td id="text_dept" class="text-left border-0">---</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="modal-footer justify-content-between">
                  <div class="text-danger">
                    <small><u>หมายเหตุ</u></small>
                    <p>สินค้านี้มีอยู่แล้วในระบบ</p>
                  </div>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">ปิด</button>
                </div>
              </div>
            </div>
          </div>
          <!-- ------------------------------------ -->

      </main>
    </div>
  </div>
  <!-- Body | finish -->



  <?php require('_html.footer.inc.php'); ?>
  <?php require('_html.footer_js.req.inc.php'); ?>
</body>

</html>

<script>
  var post_url = './api/product/index.php'
  $("#form-submit-product").submit(function (e) {
    e.preventDefault();
    $.ajax({
      type: "POST",
      url: post_url,
      data: $("#form-submit-product").serialize(),
      success: function (response) {
        if (response.records == null && response.result == null) {
          $.notify("บันทึกข้อมูลสำเร็จ", {
            position: 'bottom center',
            className: 'success'
          });
          setTimeout(function () {
            window.location.replace("yus_product.php");
          }, 3000);
        } else {
          $.notify("บันทึกข้อมูลไม่สำเร็จ", {
            position: 'bottom center',
            className: 'error'
          });
        }
      },
      error: function (response) {
        console.log('An error occurred.');
        $.notify("บันทึกข้อมูลไม่สำเร็จ", {
          position: 'bottom center',
          className: 'error'
        });
      },
    });
  });

  $(document).on('click', '#btn-check', function () {
    var item_code = $('#item_code').val();
    var cmdy_code = $('#cmdy_code').val();

    // alert(item_code + ' ' +cmdy_code );
    if (item_code != '' && cmdy_code != '') {
      $.ajax({
        type: "POST",
        url: post_url,
        data: {
          type: 'check_item',
          item_code: item_code,
          cmdy_code: cmdy_code
        },
        success: function (response) {
          if (response.records < 1) {
            console.log('OK');
            $('#submit').prop('disabled', false);
          } else {
            console.log('False');

            // set value 
            $('#text_item_code').text(': '+response.result[0].item_code);
            $('#text_cmdy_code').text(': '+response.result[0].cmdy_code);
            $('#text_item_name_th').text(': '+response.result[0].item_name_th);
            $('#text_unit').text(': '+response.result[0].unit);
            $('#text_default_price').text(': '+response.result[0].default_price);
            $('#text_dept').text(': '+response.result[0].dept);

            // set company
            if(response.result[0].co == '1'){
              $('#text_co').text(': บริษัท ธนศิริดีเซล จำกัด');
            }else if(response.result[0].co == '2'){
              $('#text_co').text(': บริษัท ธนเสริม จำกัด');
            }else if(response.result[0].co == '3'){
              $('#text_co').text(': บริษัท รีแดท (ไทยเเลนด์) จำกัด');
            }else{
              $('#text_co').text(': ไม่มีค่า');
            }
            
            // show modal
            $('#exampleModalCenter').modal('show');

            $('#submit').prop('disabled', true);
            $.notify("รหัสสินค้าและยี่ห้อ นี้มีอยู่แล้วในระบบ", {
              position: 'bottom center',
              className: 'error'
            });
          }
        },
        error: function (response) {
          console.log('An error occurred.');
        },
      });
    } else {
      if (item_code == '') {
        $('#item_code').focus();
      } else {
        $('#cmdy_code').focus();
      }
    }

  });
</script>