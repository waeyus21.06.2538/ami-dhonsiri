<?php
require('../inc/require.php');
header('Content-Type: application/json;charset=UTF-8');

$DB = "dhonsiri";
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'detail';
$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '__';
$qo_no = isset($_REQUEST['qo_no']) ? $_REQUEST['qo_no'] : '999999';
$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : 'xxx';
$item_code = isset($_REQUEST['item_code']) ? $_REQUEST['item_code'] : '999999';

$items = explode('|',$item_code);

$item_qty = isset($_REQUEST['item_qty']) ? $_REQUEST['item_qty'] : 1;
$item_price = isset($_REQUEST['item_price']) ? $_REQUEST['item_price'] : 1000;
$item_discount = isset($_REQUEST['item_discount']) ? $_REQUEST['item_discount'] : 0;
$item_shipping_day = isset($_REQUEST['item_shipping_day']) ? $_REQUEST['item_shipping_day'] : 7;



$header_to = isset($_REQUEST['to']) ? $_REQUEST['to'] : "";
$header_contact = isset($_REQUEST['contact']) ? $_REQUEST['contact'] : "";
$header_company = isset($_REQUEST['company']) ? $_REQUEST['company'] : "";
$header_remark = isset($_REQUEST['remark']) ? $_REQUEST['remark'] : "";
$header_price_hold_days = isset($_REQUEST['price_hold_days']) ? $_REQUEST['price_hold_days'] : "7";
$header_mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : "credit";
$header_show_discount = isset($_REQUEST['show_discount']) ? $_REQUEST['show_discount'] : "0";



#$vndr = isset($_REQUEST['vendor_code']) ? $_REQUEST['vendor_code'] : '000001';


$sql = []; $sqlType = []; $i = 0;
switch($type){
  case 'detail':
    if( strlen($keyword) >= 4 ) {
      $sql[$i] = "select ifnull(b.cmdy_name,'') cmdy_name,a.*
      from `{$DB}`.`@ms_item` a left join `{$DB}`.`@ms_cmdy` b on a.cmdy_code = b.cmdy_code
      where a.record_status = 'A'
      and (a.cmdy_code like '{$keyword}%' or a.item_code like '{$keyword}%' or a.item_name_th like '{$keyword}%') ;";
    } else {
      $sql[$i] = "select '-type more-' cmdy_name;";
    }
      $sqlType[$i] = 'select';
      $i++;
    break;
    case 'insert_item':
    $sqlPrc = "select default_price d from `{$DB}`.`@ms_item` where cmdy_code = '{$items[0]}' and item_code = '{$items[1]}';";
    #echo $sqlPrc;
    $qPrc = mysqliQuery($sqlPrc);
    #var_dump($qPrc);
    $prc = ($qPrc['res'][0]['d'] > 1) ? $qPrc['res'][0]['d'] : 999;
    
    #$prc = ( $qPrc['res'][0]['d']*1 > 0) ? $q['res'][0]['default_price'] : 1000;
    #var_dump($prc);
  
    $sql[$i] = "INSERT INTO `{$DB}`.`qo_detail_tmp`(`username`,`cmdy_code`,`item_code`,`crt_dtm`,`price`) VALUES ('{$username}','{$items[0]}','{$items[1]}',now(),{$prc});";
    $sqlType[$i] = 'insert';
    $i++;
    break;

    case 'qo_header':
    $sql[$i] = "select a.* from `{$DB}`.`qo_header_tmp` a where a.username = '{$username}';";
    $sqlType[$i] = 'select';
    $i++;
    break;
    
    case 'update_header':
    $sql[$i] = "update `{$DB}`.`qo_header_tmp` a set
    `to` = '{$header_to}',contact = '{$header_contact}',company = '{$header_company}', remark = '{$header_remark}'
    ,price_hold_days = '{$header_price_hold_days}', `mode` = '{$header_mode}',show_discount = '{$header_show_discount}'
    wherea.username = '{$username}';";
    echo $sql[$i];
    $sqlType[$i] = 'update';
    $i++;
    break;
    
    case 'qo_detail':
    $sql[$i] = "select a.*,b.cmdy_code,b.item_name_th from `{$DB}`.`qo_detail_tmp` a
    left join `{$DB}`.`@ms_item` b on a.cmdy_code = b.cmdy_code and a.item_code = b.item_code
    where a.username = '{$username}' and a.record_status = '0';";
    $sqlType[$i] = 'select';
    $i++;
    break;
    
    case 'qo_detail_delete':
    $sql[$i] = "update `{$DB}`.`qo_detail_tmp` a set record_status = '9',del_dtm = now() where a.record_status = '0' and a.username = '{$username}'
    and cmdy_code = '{$items[0]}' and item_code = '{$items[1]}';";
    #echo $sql[$i];
    $sqlType[$i] = 'update';
    $i++;
    break;
    
    case 'qo_detail_save':
    $sql[$i] = "update `{$DB}`.`qo_detail_tmp` a set qty = '{$item_qty}', price = '{$item_price}', discount = '{$item_discount}'
    ,shipping_day = {$item_shipping_day}
    where a.record_status = '0' and a.username = '{$username}'
    and a.cmdy_code = '{$items[0]}' and a.item_code = '{$items[1]}';";
    #echo $sql[$i];
    $sqlType[$i] = 'update';
    $i++;
    break;
  default: 
    #echo 2;
}


$res = [];
for($ii=0; $ii<$i; $ii++) {
  if($sqlType[$ii]=='select' || $sqlType[$ii]=='insert' || $sqlType[$ii]=='delete' || $sqlType[$ii]=='update'):
    $res = mysqliQuery($sql[$ii]);
  endif;
}


#var_dump($res);
/*
foreach($res as $key => $val){
  echo $key, ' = ', $val, '<br>';

} */

$res2 = [];
$res2['records'] = $res['c'];
#$res2['startRecord'] = $limitStart+1;
#$res2['endRecord'] = $limitStart+$q['c'];
#$res2['allRecords'] = $res[0]['c'];
#$res2['page'] = $page;
$res2['result'] = $res['res'];
$result = json_encode($res2);
echo $result;
