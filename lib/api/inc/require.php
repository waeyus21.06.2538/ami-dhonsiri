<?php
require('_core.credential.inc.php');

if(!session_id()) { session_start(); }

$session_id = session_id();
date_default_timezone_set('Asia/Bangkok');

// SYSTEM VAR
$LANG__ = 'en';
$BASELANG__ = 'en';
$BASECURRENCY__ = 'THB';

$ROOT_LEVEL = isset($ROOT_LEVEL) ? $ROOT_LEVEL : 0;
$F_PFX_ = "";
for($i=0; $i<$ROOT_LEVEL; $i++) { $F_PFX_ .= "../"; }

$SYSTEM_TYPE = 'fr_';
$SESSION_PREFIX = $SYSTEM_TYPE.'matino_';
$SESSION_PREFIX_LEN = strlen($SESSION_PREFIX);

/*
if( isset($_SESSION[$SESSION_PREFIX.'ABS_PATH__']) ) { unset($_SESSION[$SESSION_PREFIX.'ABS_PATH__']); }
if( isset($_SESSION[$SESSION_PREFIX.'ABS_PATH_INC__']) ) { unset($_SESSION[$SESSION_PREFIX.'ABS_PATH_INC__']); }
if( isset($_SESSION[$SESSION_PREFIX.'PATH_BACKEND__']) ) { unset($_SESSION[$SESSION_PREFIX.'PATH_BACKEND__']); }

if (strtolower(PHP_SHLIB_SUFFIX) === 'dll') {
  $_SESSION[$SESSION_PREFIX.'ABS_PATH__'] = "d:/_src/_www/new/";
  $_SESSION[$SESSION_PREFIX.'ABS_PATH_INC__'] = "d:/_src/_www/new/inc/";
  $_SESSION[$SESSION_PREFIX.'PATH_BACKEND__'] = "http://localhost/new/";
} else {
  $_SESSION[$SESSION_PREFIX.'ABS_PATH__'] = "/var/www/html/_core";
  $_SESSION[$SESSION_PREFIX.'ABS_PATH_INC__'] = "/var/www/html/_core/inc";
  $_SESSION[$SESSION_PREFIX.'PATH_BACKEND__'] = "https://vms.matinoshoes.com/";
}
*/

//$BACKEND_RPATH__ = $F_PFX_."../new/";
$BACKEND_RPATH__ = "";

$LANGLIST__ = [
  'th'=>'THA',
  'en'=>'ENG'
];

$ROOT_DS_ = '';
$DS__ = DIRECTORY_SEPARATOR;
//$DS__ = '\\';

$today_ = date('Y-m-d');
$dt = $today_;
$full_date_ =  $today_;

$full_time_ = date('H:i:s');
$tm = $full_time_;

$now_ = date('Y-m-d H:i:s');
$dtm_ = $now_;
$dtm = $now_;

#$TIMEOUT_minute = 15;
$TIMEOUT_minute = 45;

$ip_ = getenv('HTTP_CLIENT_IP')?:
getenv('HTTP_X_FORWARDED_FOR')?:
getenv('HTTP_X_FORWARDED')?:
getenv('HTTP_FORWARDED_FOR')?:
getenv('HTTP_FORWARDED')?:
getenv('REMOTE_ADDR');

$ip_no_ = "inet_aton('".$ip_."')";
#$remote_ip_ = "inet_aton('".$_SERVER['REMOTE_ADDR']."')";
 
$leapYear_ = date('L'); $year_ = date('Y');
$month_ = date('m'); $monthLong_ = strtolower(date('F'));
$day_ = strtolower(date('D')); $dow_ = date('w');
$dayLong_ = strtolower(date('l')); $date_ = date('d');
$dateJ_ = date('j'); $hour_ = date('H');

// COOKIE's aging
$secInMin = 60; $minInHour = 60; $hourInDay = 24; $dayInWeek = 7;
$secInHour = $secInMin * $minInHour;
$secInDay = $secInHour * $hourInDay;
$secInWeek = $secInDay * $dayInWeek;
$COOKIE_aging_day = 14;

// DATETIME in Server local timezone
$dd__ = date('d'); $mm__ = date('m'); $yyyy__ = date('Y'); $yy__ = date('y');
$hh__ = date('H'); $ii__ = date('i'); $ss__ = date('s'); $dow__ = date('w');
$now__ = date('Y-m-d H:i:s'); $today__ = date('Y-m-d'); $timenow__ = date('H:i:s');
$dt__ = date('Y-m-d'); $tm__ = date('H:i:s');
$timeZone__ = "+07:00";
$ogUpdatedTime = date('Y-m-d')."T".date('H:i:s').$timeZone__;


$scriptName = explode('/',$_SERVER['SCRIPT_NAME']);
$queryString = $_SERVER['QUERY_STRING'];
$scriptName2 = $scriptName[count($scriptName)-1];
$scriptName10 = substr($scriptName2,0,10);

$GET_ = "?";
$IS_GET_ = strpos($_SERVER['REQUEST_URI'], $GET_);
$GETPARAMS_ = array();
$TMPURI_ = explode($GET_,$_SERVER['REQUEST_URI']);
$TMPURL_ = explode('/',$TMPURI_[0]);
$SCRIPTNAME_ = $TMPURL_[count($TMPURL_)-1];

$SCRIPTNAME2_ = explode('.',$SCRIPTNAME_);
unset($SCRIPTNAME2_[count($SCRIPTNAME2_)-1]);
$SCRIPTNAME2X_ = implode('.',$SCRIPTNAME2_);

unset($TMPURL_[count($TMPURL_)-1]);
$URLCURRENTPATH_ = join('/',$TMPURL_);

if($IS_GET_ !== FALSE) {
  //echo $TMPURI_[1],'<br>';
  $GETS_ = explode('&',$TMPURI_[1]);
  $IG_ = 0;
  foreach($GETS_ as $k => $v) {
    //echo '[',$k,'] = ',$v,'<br>';
    $vv = explode('=',$v);
    $exception_v = array("clickPage","gotoPage");
    if(!in_array($vv[0],$exception_v)) {
      $GETPARAMS_[$IG_]['k'] = $vv[0];
      $GETPARAMS_[$IG_]['v'] = $vv[1];
      $IG_++;
    }
  }
}

$SYSNAME__ = 'VMS';
$SYSCODE__ = 'vms001';
$SYSNAME_LOWER__ = strtolower($SYSNAME__);
$SYSNAMELEN__ = strlen($SYSNAME__);
$SYSNAME_DSP_ICO__ = 'Viboonsiri';
//$SYSICO__ = 'img/v2_logo_1x.png';
$SYSICO__ = 'v_logo.svg';
$SYSICO_ALT__ = 'VMS Logo';
$SYSICO_DIM__ = [ 'x' => 48, 'y' => 48 ];
$USRPROFILE_DIM__ = 48;
$TITLE_PREFIX__ = 'VMS - ';
$favicon_filename_ = 'favicon_v.ico';
#$DBNAME__ = 'triplefo_vms';
$DBNAME__ = 'vms_master';
$AjaxURL = '_ajax.core.php';

$PUBLIC_NAME__ = "Matinoshoes";
$PUBLIC_URL__ = "matinoshoes.com";
$PUBLIC_BASEURL__ = "http://".$PUBLIC_URL__;
$PUBLIC_BASEURL_S__ = "https://".$PUBLIC_URL__;

function createSocialMeta($ogTitle="",$ogDesc="Description..........................",$ogImgPath="") {
  global $PUBLIC_BASEURL__,$ogUpdatedTime,$PUBLIC_NAME__,$LANG__;
  $metas = '
  <meta name="keywords" content="keyword1">
  <meta name="description" content="'.$ogDesc.'">
  <meta name="subject" content="'.$ogTitle.'">
  <meta name="language" content="'.strtoupper($LANG__).'">
  <meta name="medium" content="products">
  
  <meta property="og:site_name" content="'.$PUBLIC_NAME__.'"/>
  <meta property="og:type" content="article"/>
  <meta property="og:description" content="'.$ogDesc.'"/>
  <meta property="og:title" content="'.$ogTitle.'"/>
  <meta property="og:url" content="'.$PUBLIC_BASEURL__.$_SERVER['REQUEST_URI'].'"/>
  <meta property="og:updated_time" content="'.$ogUpdatedTime.'"/>
  <meta property="og:image" content="'.$ogImgPath.'"/>
  ';
  echo $metas;
}


// Paging
$C__ = 0; // default
$W__ = ""; // default
//$SHOWN_RECORDS_SET__ = [8,24,48,100]; // OLD COL-* BASED
$SHOWN_RECORDS_SET__ = [10,20,50,100]; // NEW FOR FLEX
$SHOWN_RECORDS__ = $_COOKIE[$SYSNAME__.'shown_records__'] ?: $SHOWN_RECORDS_SET__[0];
$SHOWN_VIEWTYPE__ = $_COOKIE[$SYSNAME__.'shown_viewtype__'] ?: 'grid';
setcookie($SYSNAME__.'shown_records__', $SHOWN_RECORDS__);
setcookie($SYSNAME__.'shown_viewtype__', $SHOWN_VIEWTYPE__);

  $CUR_PAGE_TMP__ = 0;
  if($_GET['clickPage'] > 0) { $CUR_PAGE_TMP__ = $_GET['clickPage']; }
  else if($_GET['gotoPage'] > 0) { $CUR_PAGE_TMP__ = $_GET['gotoPage']; }
$CUR_PAGE__ = ($CUR_PAGE_TMP__ === 0) ? 1 : $CUR_PAGE_TMP__;
$START_RECORD__ = (($CUR_PAGE__-1)*$SHOWN_RECORDS__);
$START_RECORD_DSP__ = (($CUR_PAGE__-1)*$SHOWN_RECORDS__)+1;
$LAST_RECORD__ = (($CUR_PAGE__-1)*$SHOWN_RECORDS__)+$SHOWN_RECORDS__;

$ICO_PROVIDER__ = 'ion';
$ICO_SYS__['ion']['refresh'] = 'ion-android-refresh';
$ICO_SYS__['ion']['firstPage'] = 'ion-ios-skipbackward-outline';
$ICO_SYS__['ion']['lastPage'] = 'ion-ios-skipforward-outline';
$ICO_SYS__['ion']['prev'] = 'ion-ios-play-outline';
$ICO_SYS__['ion']['next'] = 'ion-ios-play-outline';



// Common Variable
$searchString = $_GET['search'] ?: '';


// CSS
$CSS_HDR_H_ = 60;

$BRAND_ID_ = 2;
$MANAGE_PATH_ = '../4/';


$PATH_UPLOAD_IMG__ = '_uploaded/img';
$ThumbPrefix__ = 'sq2.';
$ThumbSizeW = '320';
$ThumbSizeH = '320';
$ThumbSizeSQ = '320';


require('_core.function.inc.php');
require('_core.dictionary.php');
#require('_core.menu.main.php');
require('_core.menu.personal.php');
?>