<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main class="bg-eee col-12 col-md-9 col-xl-10 p-2">

<div>
  <form name="qo" action="./api/item/" id="formQo">
    <article class="container">
    <div class="row">
      <div class="col-2 d-flex align-items-center py-1">สินค้า</div>
      <div class="col-6 d-flex align-items-center py-1">
        <select class="form-control select2" id="item">
          <option v-for="item in items" :value="item.cmdy_code+'|'+item.item_code">{{item.cmdy_code}} - {{item.item_code}} {{item.item_name_th}}</option>
        </select>


      </div>
      <div class="col-2 d-flex align-items-center py-1">
        <button type="button" class="btn btn-primary mr-auto btn-block" onclick="addItemQo()">Add</button>
      </div>
      <div class="col-2 d-flex align-items-center py-1">
        <a href="pdf.php" class="btn btn-primary mr-auto btn-block"
        target="_blank">Preview</a>
      </div>
      
    </div>
    </article>
    
    <article class="container" id="qoH">
    <div class="row">
    
      <div class="col-2 d-flex align-items-center">เรียน<input
      type="hidden" name="type" value="update_header"><input
      type="hidden" name="qo_no" value="Q20180611111"></div>
      <div class="col-4 d-flex align-items-center py-1"><input type="text" name="to" class="form-control" required></div>
    
      <div class="col-2 d-flex align-items-center">เบอร์โทร / อีเมล</div>
      <div class="col-4 d-flex align-items-center py-1"><input type="text" name="contact" class="form-control"></div>
      
      <div class="col-2 d-flex align-items-center">บริษัท</div>
      <div class="col-4 d-flex align-items-center py-1"><input type="text" name="company" class="form-control" required></div>      
      
      <div class="col-2 d-flex align-items-center">ยืนราคา (วัน)</div>
      <div class="col-4 d-flex align-items-center py-1"><select name="price_hold_days" class="form-control">
        <option value="7">7 วัน</option>
        <option value="15">15 วัน</option>
        <option value="30">30 วัน</option>
        <option value="60">60 วัน</option>
        <option value="90">90 วัน</option>
        <option value="120">120 วัน</option>
      </select></div>
      
      <div class="col-2 d-flex align-items-center">แสดงส่วนลด</div>
      <div class="col-4 d-flex align-items-center py-1">
      <span><input type="checkbox" value="1" name="show_discount"> แสดง</span></div>
      
      <div class="col-2 d-flex align-items-center">ชำระเงิน</div>
      <div class="col-4 d-flex align-items-center justify-content-around py-1">
      <span><input type="radio" name="mode" value="credit" checked> Credit</span>
      <span><input type="radio" name="mode" value="cash"> Cash</span>
      </div>
      
      <div class="col-2 d-flex align-items-center">หมายเหตุ</div>
      <div class="col-10 d-flex align-items-center py-1"><textarea type="text" name="remark" class="form-control" rows="2"></textarea></div>
    
      <div class="col-12 d-flex align-items-center justify-content-end">
        <button class="btn btn-primary px-5" type="sumit">Save</button>
      </div>
    
    </div>
    <!--
    <div class="row">
        <div class="col-12 d-flex align-items-center py-1">
          <button type="button" class="btn btn-primary mr-auto">Back</button>
          <button type="button" onclick="next();" class="btn btn-primary">Next</button>
        </div>
      </div>
      -->
    </article>
  </form>

<hr>

<style>
input.form-control, select.form-control { font-size: 0.9rem; padding: 1px 4px !important; text-align: right; }
</style>

<article id="result">


  <table class="table table-bordered table-sm table-striped bg-white">
    <thead>
      <tr style="text-align: center; font-size: 0.8rem; font-weight: bold;">
          <td width="35">#</td>
          <td width="50">Brand</td>
          <td>สินค้า</td>
          <td width="70">Qty</td>
          <td width="100">Price</td>
          <td width="50">ส่วนลด<br>(%)</td>
          <td width="90">ราคา<br>สุทธิ</td>
          <td width="100">ราคารวม</td>
          <td width="70">กำหนดส่ง<br>(วัน)</td>
          <td width="120">Action</td>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(item,index) in items" style="font-size: 0.85rem;">
          <td class="text-right">{{ index+1 }}</td>
          <td>{{ item.cmdy_code }}</td>
          <td>{{ item.item_code }}
          <br>{{ item.item_name_th }}</td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_qty'"
            name="qty" type="text" class="form-control" :value="item.qty">
          </td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_price'"
            name="price" type="text" class="form-control" :value="item.price">
          </td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_discount'"
            name="discount" type="text" class="form-control" :value="item.discount">
          </td>
          <td class="text-right">{{ (item.price*(100-parseInt(item.discount))/100).toLocaleString(undefined, {minimumFractionDigits: 2}) }}</td>
          <td class="text-right font-weight-bold">{{ (item.qty * item.price*(100-parseFloat(item.discount))/100).toLocaleString(undefined, {minimumFractionDigits: 2}) }}</td>
          <td>
            <input :id="item.cmdy_code+'|'+item.item_code+'_shipping_day'"
            name="shipping_day" type="text" class="form-control" :value="item.shipping_day">
          </td>          
          <td>
            <button class="btn btn-primary btn-sm" type="button" v-on:click="saveQO(item.cmdy_code+'|'+item.item_code)">บันทึก</button>
            <button class="btn btn-danger btn-sm" type="button" v-on:click="delQO(item.cmdy_code+'|'+item.item_code)">ลบ</button>
          </td>
      </tr>
    </tbody>
    <!--
    <tfoot>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
    </tfoot>
    -->
  </table>

</article>

</div>

</main>



</div></div>
<!-- Body | finish -->


<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>
<script>
function next(){
  var po = document.getElementById('po');
  var poValue = po.options[po.selectedIndex].value;
  console.log(poValue);
}

var tt = new Vue({
  el: '#formQo',
  data: {
    items: []
  },
  mounted(){
    this.get()
  },
  methods: {
    get: function() {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'detail',
          keyword: '6302',
          // keyword: '63',
        } //end params

      })
      .then(function(response){
        tt.items = response.data.result
       // console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    }
  }
})


var appDet = new Vue({
  el: '#result',
  data: {
    items: []
  },
  mounted(){
    this.get()
  },
  methods: {
    get: function() {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_detail',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
        } //end params

      })
      .then(function(response){
        appDet.items = response.data.result
        console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    },
    saveQO: function(itemCode) {
      var itemDiscount = document.getElementById(itemCode+'_discount').value
      var itemPrice = document.getElementById(itemCode+'_price').value
      var itemQty = document.getElementById(itemCode+'_qty').value
      var itemShippingDay = document.getElementById(itemCode+'_shipping_day').value
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_detail_save',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
          item_code: itemCode,
          item_discount: itemDiscount,
          item_price: itemPrice,
          item_qty: itemQty,
          item_shipping_day: itemShippingDay,
        } //end params

      })
      .then(function(response){
        // console.log( '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?> saved!' )
        tt.get()
        appDet.get()
       // console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    },
    
    delQO: function(itemCode) {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_detail_delete',
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
          item_code: itemCode,
        } //end params

      })
      .then(function(response){
        console.log( '[' + 'Q20180611111' + '] deleted!' )
        tt.get()
        appDet.get()
       // console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    }
  }
})




function getItemCode() {
  var item = document.getElementById('item');
  var itemValue = item.options[item.selectedIndex].value;
  return itemValue
}

function addItemQo() {
  var itemCode = getItemCode()
  //console.log(poValue)
  axios({
    method: 'get',
    url: './api/item/',
    params: {
      type: 'insert_item',
      username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
      item_code: itemCode,
    } //end params

  })
  .then(function(response){
    //tt.poList = response.data.result
    //console.log( '[' + 'Q20180611111' + '] added!' )
    tt.get()
    appDet.get()

    // console.log(response)
  })
  .catch(function(error){
    console.log(error)
  })
}

/*
const qoH = new Vue({
  el: '#qoH',
  data: {
    inputs: []
  },
  mounted(){
    this.get()
  },
  methods: {
    get: function() {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          type: 'qo_header',
          qo_no: 'Q20180611111',
        } //end params
      })
      .then(function(response){
        qoH.inputs = response.data.result
        console.log(response)
      })
      .catch(function(error){
        console.log(error)
      })
    },
  }
})
*/
$('.select2').select2();
$('form').ajaxForm();
</script>