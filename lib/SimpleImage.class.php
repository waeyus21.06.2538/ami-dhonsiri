<?php
class SimpleImage {
  var $image;
  var $image_type;

  function load($filename) {
    $image_info = getimagesize($filename);
    $this->image_type = $image_info[2];
    if($this->image_type == IMAGETYPE_JPEG ) {
      $this->image = imagecreatefromjpeg($filename);
    } elseif($this->image_type == IMAGETYPE_GIF ) {
      $this->image = imagecreatefromgif($filename);
    } elseif($this->image_type == IMAGETYPE_PNG ) {
      $this->image = imagecreatefrompng($filename);
    }
   }

  function getWidth() {
    return imagesx($this->image);
  }

  function getHeight() {
    return imagesy($this->image);
  }

  function resize($width,$height) {
    $new_image = imagecreatetruecolor($width, $height);
    imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
    $this->image = $new_image;
  }

  function resizeCrop($sqSize,$x,$y,$smallestSide) {
    $new_image = imagecreatetruecolor($sqSize, $sqSize);
    imagecopyresampled($new_image, $this->image, 0, 0, $x, $y, $sqSize, $sqSize, $smallestSide, $smallestSide);
    $this->image = $new_image;
  }

  function resizeToHeight($height) {
    $ratio = $height / $this->getHeight();
    $width = $this->getWidth() * $ratio;
    $this->resize($width,$height);
  }

  function resizeToWidth($width) {
    $ratio = $width / $this->getWidth();
    $height = $this->getHeight() * $ratio;
    $this->resize($width,$height);
  }

  function cropToSquare($sqSize) {
	$w = $this->getWidth();
	$h = $this->getHeight();
    if($w>$h) {
      $y = 0;
      $x = ($w - $h) / 2;
      $smallestSide = $h;
	} else {
      $x = 0;
      $y = ($h - $w) / 2;
      $smallestSide = $w;
	}
    $this->resizeCrop($sqSize,$x,$y,$smallestSide);
  }

  function resizeCrop2($xx,$yy,$x,$y,$w,$h,$sqSize) {
    $new_image = imagecreatetruecolor($sqSize, $sqSize);
    $whiteBackground = imagecolorallocate($new_image, 255, 255, 255);
    imagefill($new_image,0,0,$whiteBackground);
    
    imagecopyresampled($new_image, $this->image, $x, $y, 0, 0, $xx, $yy, $w, $h);
    $this->image = $new_image;
  }

  function cropToSquare2($sqSize) {
	$w = $this->getWidth();
	$h = $this->getHeight();
    if($w>$h) {
      $min = ceil($h/($w/$sqSize));
      $x = 0; $xx = $sqSize;
      $yy = $min;
      $y = ($sqSize -  $min) / 2;
	} else {
      $min = ceil($w/($h/$sqSize));
      $y = 0; $yy = $sqSize;
      $xx = $min;
      $x = ($sqSize -  $min) / 2;
	}
    $this->resizeCrop2($xx,$yy,$x,$y,$w,$h,$sqSize);
  }



  function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
    if($image_type == IMAGETYPE_JPEG ) {
      imagejpeg($this->image,$filename,$compression);
    } elseif($image_type == IMAGETYPE_GIF ) {
      imagegif($this->image,$filename);
    } elseif($image_type == IMAGETYPE_PNG ) {
      imagepng($this->image,$filename);
    }
    if($permissions != null) {
      chmod($filename,$permissions);
    }
  }
}
?>