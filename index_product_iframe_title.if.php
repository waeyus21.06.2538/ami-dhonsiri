<!-- title | start -->
  <div class="col-12 bg-cus1 text-cus1-txt font-weight-bold py-2 text-center fsz_rem12"><?=$title?></div>
  <div id="product" v-cloak class="bg-eee col-12 d-flex align-items-center py-2 text-center fsz_rem10">
    <span class="mr-2">Brand:<span class="ml-2 font-weight-bold">{{ product.disp }}</span></span>
    <span class="mx-2">Product name:<span class="ml-2 font-weight-bold">{{ product.product_name_default }}</span></span>
  </div>
<!-- title | finish -->