<?php require('api/inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('api/inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main class="bg-eee col-12 col-md-9 col-xl-10 p-2">

<div>
<form id="formCategory" name="category">

<article class="container"><div class="row">
  <!--search item | start -->
  <div class="col-12 col-md-4 d-flex align-items-center py-1">
    <div class="input-group w-100">
      <input class="form-control" type="search" id="searchBox" placeholder="Search">
      <!-- <input class="form-control" type="search" id="searchBox" placeholder="Search" onkeyup="search();"> -->
      <div class="input-group-append">
        <button class="btn btn-primary" type="button" onclick="search();">
          <span class="ion-search"></span>
        </button>
      </div>
    </div>
  </div>
    
  <div class="col-12 offset-md-4 col-md-4 d-flex align-items-center">
  <!--Add item-->
  <a href="index_quotation_create.php" class="btn btn-block btn-primary">สร้างใบเสนอราคา</a>
    
</div></article>
</form>
</div>


<hr>


<section id="pagingControl">
<div class="container"><div class="row">

  <div class="col-12 d-block d-md-flex align-items-center justify-content-between">
    <span class="d-flex align-items-center form-inline">
      <button disabled v-if="config.page == 1"
      type="button" class="btn mr-1 btn-sm px-3 btn-primary" onclick="backPage();">Back</button>
      <button v-else
      type="button" class="btn mr-1 btn-sm px-3 btn-primary" onclick="backPage();">Back</button>
      <button disabled v-if="config.page == config.totalPage || config.totalPage == 0"
      type="button" class="btn mr-1 btn-sm px-3 btn-primary" onclick="nextPage();">Next</button>
      <button v-else
      type="button" class="btn mr-1 btn-sm px-3 btn-primary" onclick="nextPage();">Next</button>

      Page&nbsp;&nbsp;<select id="pageBox" v-model="config.page"
      class="form-control-sm mb-1" onchange="gotoPage(this.options[this.selectedIndex].value);">
        <option v-for="(n, index) in config.totalPage" :value="index+1"
        >{{ index+1 }}</option>
      </select>&nbsp;&nbsp;of&nbsp;&nbsp;{{ config.totalPage }}
    </span>



    <span class="d-inline-block">
      {{ config.startRecord }} - {{ config.endRecord }} of {{ config.allRecords }} records
    </span>

    <span class="d-inline-block form-inline">
      Show&nbsp;&nbsp;<select id="limitBox" v-model="config.limit"
      class="form-control-sm" onchange="setLimit(this.options[this.selectedIndex].value);">
        <option v-for="(n, index) in config.limitOptions" :value="n"
        >{{ n }}</option>
      </select>
    </span>
  </div>


</div></div>
</section>



<article id="result">
  <div class="container"><div class="row">

  
    <div v-if="pagingConfig.config.totalPage == 0"
    class="col-12 text-center p-5 font-weight-bold">No records found.</div>
    <div v-else class="col-12 py-2">
    <table style="font-size: 0.8rem;"
    class="table table-bordered table-striped table-sm bg-white">
    <thead>
    <tr>
      <th width="50">#</th>
      <th width="120">Quotation No</th>
      <th width="300">ลูกค้า</th>
      <th>...</th>
      <th width="70">Action</th>
    </tr>
    </thead>

    <tbody>
      <tr v-for="(qo, index) in qoList">
        <td class="text-right align-middle">{{ pagingConfig.config.startRecord+index }}</td>
        <td class="font-weight-bold align-middle">{{qo.qo_no}}</td>
        <td class="align-middle">{{qo.to}} ({{qo.company}})<br>{{qo.contact}}</td>
        <td></td>
        <td class="text-left align-middle">
          <button class="btn btn-success btn-sm" type="button" v-on:click="viewQo(qo.qo_no,qo.qo_date)">View</button>
        </td>
      </tr> 
      </tbody>
      </table>
    </div>

  </div></div>
  </article>  


</main>
  
<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>

<script>
window.addEventListener('load', function () {
  var limit = localStorage.getItem('limit')
  if(limit == null) {

  } else {
    setLimit(limit)
  }
  console.log(limit)
}, false);


function nextPage() { pagingConfig.config.page = pagingConfig.config.page + 1; qoDet.get(); }
function backPage() { pagingConfig.config.page = pagingConfig.config.page - 1; qoDet.get(); }
function gotoPage(pageNo) { pagingConfig.config.page = pageNo; qoDet.get(); }
function setLimit(limitNo) { pagingConfig.config.limit = limitNo;
  localStorage.setItem('limit', limitNo); qoDet.get(); }
function search() { 
  var searchBox = document.getElementById('searchBox');
  pagingConfig.config.keyword = searchBox.value; 
  qoDet.get(); 
}


var pagingConfig = new Vue({
  el: '#pagingControl',
  data: {
    config: {
      keyword: "",
      limit: 10,
      limitOptions: [10,25,50],
      records: 10,
      startRecord: 1,
      endRecord: 10,
      allRecords: 12,
      page: 1,
      totalPage: 0,
    }
  },
})

var cat_name = new Vue({
  el: '#cat_name_group',
  data: {
    name: ""
  }
})


var p = 0
var qoDet = new Vue({
  el: '#result',
  data: {
      qoList: []
  },
  mounted(){
    this.get()
  },
  methods: {
    get: function() {
      axios({
        method: 'get',
        url: './api/item/',
        params: {
          keyword: pagingConfig.config.keyword,
          username: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
          type: 'qo_list',
          page: pagingConfig.config.page,
          page_limit: pagingConfig.config.limit,
        } //end params
      })
      .then(function(response){
        qoDet.qoList = response.data.result
        pagingConfig.config.totalPage = response.data.totalPage
        pagingConfig.config.page = response.data.page
        pagingConfig.config.limit = response.data.limit
        pagingConfig.config.records = response.data.records
        pagingConfig.config.startRecord = response.data.startRecord
        pagingConfig.config.endRecord = response.data.endRecord
        pagingConfig.config.allRecords = response.data.allRecords
      })
      .catch(function(error){
        console.log(error)
      })
    },
    viewQo: function(qo_no,qo_date) {
      var url = 'index_quotation_view.php?qo_no='+qo_no+'&qo_date='+qo_date
      var win = window.open(url, '_blank');
      win.focus();
    }
  }
  })
  
function popupAlert(txt='xx',popupType='global',popupClass='success',position='bottom left') {
  if(popupType=='global') {
    $.notify(txt, {
      className: popupClass,
      position: position
    })
  } else {
    $("#"+popupType).notify(txt, {
      className: popupClass,
      position: position
    })
  }
}
  
</script>