<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
$title = "Change brand";
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main class="bg-eee col-12 col-md-9 col-xl-8 p-0" style="border-right: 1px solid #ddd; border-left: 1px solid #ddd;">

<?php #var_dump($_SESSION); ?>

<!-- Header | start -->
<div class="container mb-2">
  <div class="row">
    <div class="col-md-12 py-3 bg-white font-weight-bold fsz_rem15 pt-4 text-center"><?=$title?></div>
</div>
<!-- Header | finish -->




<!-- <input class="w-100" name="REQ_tags" id="tags"> -->



<div class="container-fluid"><div class="row">

  <div class="col-12 px-2 pb-2 pt-3">
  <?php if( isset($_GET['http_referer']) ) { ?>
    <a href="<?=$_GET['http_referer']?>" class="btn btn-primary w-25">Back</a>
  <?php } else { ?>
    <a href="<?=$_SERVER['HTTP_REFERER']?>" class="btn btn-primary w-25">Back</a>
    <!-- <button type="button" onclick="window.history.back()" class="b0tn btn-primary w-25">Back</button> -->
  <?php } ?>
    <!-- <iframe class="d-none" src="null.php" name="topTarget"></iframe> -->
  </div>

<?php $sqlB = "select system_id,brand_name nm,brand_name_disp nmdisp,`hash` h from `{$DBNAME__}`.`@master_brand`
where system_id = {$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']} and record_status = '1'";
$qB = mysqliQuery($sqlB);
foreach($qB['res'] as $k => $v) { ?>

<div class="col-12 col-sm-6 p-1">
<form name="main" method="post" action="_sys.changebrand.update.php">
<input type="hidden" name="http_referer" value="<?=$_SERVER['HTTP_REFERER']?>">
<input type="hidden" name="brand_name" value="<?=$v['nm']?>">
<input type="hidden" name="brand_name_disp" value="<?=$v['nmdisp']?>">
<input type="hidden" name="h" value="<?=$v['h']?>">
  <div class="w-100 <?=($v['nm'] == $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname'])?' bg-cus1 text-cus1-txt':'bg-white'?> px-2 py-1 border border-ddd rounded">
    <span class="d-block p-2 font-weight-bold"><?=$v['nmdisp']?></span>
    <?php if($v['nm'] == $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname']) { ?>
    <span class="d-block text-right py-1 font-weight-bold"><button type="button" class="w-25 btn btn-sm disabled" disabled>Current</button></span>
    <?php } else { ?>
    <!--
    <span class="d-block text-right py-1 font-weight-bold"><button onclick="changeBrand('<?=$v['h']?>','<?=$v['nm']?>','<?=$v['nmdisp']?>')"
    type="button" class="w-25 btn btn-sm btn-primary">Change</button></span> -->
    <span class="d-block text-right py-1 font-weight-bold"><button type="submit" class="w-25 btn btn-sm btn-primary">Change</button></span>
    <?php } ?>
  </div></form>
</div>

<?php } ?>
</div></div>







</main>


<?php require('_html.right_menu.inc.php'); ?>
</div></div>
<!-- Body | finish -->


<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>

</html>
<script>

function submitFormAPI(formID) {
  document.referrer
  //var formData = $("#"+formID).serializeArray();
  //console.log(formData);
}

function changeBrand(hash,nm,nmdisp) {
  console.log(hash)
  axios({
    method: 'post',
    url: '_sys.changebrand.update.php',
    data: {
      brand_name: nm,
      brand_name_disp: nmdisp,
      h: hash,
    }
  })
  .then(function (response) {
    console.log(response)
    //window.location.href = window.location.href+'?result=1'
  })
  .catch(function (error) {
    console.log(error)
  })
}

<?php switch($RESULT) {
  case 1: $notifyTxt = "Brand changed!"; break;
  default: $notifyTxt = "";
} ?>

$.notify('<?=$notifyTxt?>', {
  position: 'bottom center',
  className: 'success'
});

<?php if($RESULT>0) { ?>window.history.replaceState(null, null, window.location.pathname);<?php } ?>
</script>