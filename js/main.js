//---[a]---
function axiosCallReload(params,url,option='reload') {
  console.log(params)
  axios({
    method: 'get',
    url: url,
    params: params
  })
  .then(function (response) {
    console.log(response)
    return response;
  })
  .catch(function (error) {
    console.error(error)
    return Promise.reject(error);
  })
  if(option == 'reload') {
    window.location.href = window.location.href
  }
}

// Added: 2018-03-27, Gonatee

//---[b]---
//---[c]---
function countChar201803(id,len) {
  var a = document.getElementById(id)
  var b = document.getElementById(id+"_counter")
  //console.log(a.value.length)
  var c = parseInt(len) - parseInt(a.value.length)
  //console.log(c)
  b.innerHTML = c+" / "+len
}

// Added: 2018-03-21, Gonatee

//---[d]---
//---[e]---
//---[f]---

//---[g]---
function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1)
  var sURLVariables = sPageURL.split('&')
  for(var i=0; i<sURLVariables.length; i++)  {
    var sParameterName = sURLVariables[i].split('=')
    if(sParameterName[0] == sParam) {
      return sParameterName[1]
    }
  }
}


// Added: 2017-11-21, Gonatee
// Source: https://stackoverflow.com/a/22607328/3676008

//---[h]---
//---[i]---
//---[j]---
//---[k]---
//---[l]---

//---[m]---
function modalOpen(modalType,url="#") {
  if(modalType == 'cart') {
    resizeModal(modalType)
    $('#cartModal').modal()
  }
}
// Added: 2018-02-17, Gonatee

//---[n]---
//---[o]---
//---[p]---
//---[q]---

//---[r]---
function resizeModal(modalType,resizeType='reload') {
  if(modalType == 'cart') {
    var cartModal = document.getElementById('cartModal')
    var modalContent = document.getElementById('modal_cart_content')
    var modalBody = document.getElementById('modal_cart_body')
    modalContent.style.height = (parseFloat(document.documentElement.clientHeight)-16)+"px"
    if(resizeType == 'init') {
      cartModal.classList.add('modal')
    }
    modalBody.src = modalBody.src
  }
}
// Added: 2018-02-19, Gonatee


//---[s]---
var max = -1;
function setH(identifier) {
  $(identifier).each(function() {
    max = Math.max(max, $(this).height());
  });
  $(identifier).height(max);
}

// Added: 2018-03-27, Gonatee

//---[t]---
function toggleHidden(identifier,type,display='block') {
  if(type == 'hide') {
    $(identifier).each(function() {
      $(this).addClass('d-none');
      $(this).removeClass('d-'+display);
      window.location.href = window.location.href
    });
  } else {
    $(identifier).each(function() {
      $(this).removeClass('d-none');
      $(this).addClass('d-'+display);
    });
  }
}

// Added: 2018-03-27, Gonatee


//---[u]---

//---[v]---
function vmsOpenModal(url='null.php') {
  var a = document.getElementById('vmsModalIframe')
  a.src = url;
  toggleHidden('.vmsCustomModal','show')
}

// Added: 2018-03-27, Gonatee

//---[w]---
//---[x]---
//---[y]---
//---[z]---
