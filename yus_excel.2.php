<?php

/** Include PHPExcel */
require('lib/excel/PHPExcel.php');
require('inc/require.php');

$sql = "select cmdy_code 'ยีห้อ', item_code 'รหัสสินค้า', item_name_th 'ชื่อสินค้า', unit 'หน่วย', co 'บริษัท', dept 'แผนก', default_price 'ราคา'
from `dhonsiri`.`@ms_item` a where a.record_status = 'A';";
// echo $sql;
$q = mysqliQuery($sql);
// echo "<pre>";
//     var_dump($q['res']);
// echo "</pre>";
// exit();
// Create new PHPExcel object


$objPHPExcel = new PHPExcel();

// // Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

foreach(range('A','G') as $columnID) {
	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
		->setAutoSize(true);
}

// $objPHPExcel->getActiveSheet()->freezePane('E2');
// $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(40);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(60);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(120);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(90);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(40);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(40);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(50);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(50);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(15);
// $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
// $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);
// $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setSize(14);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'ยีห้อ')
			->setCellValue('B1', 'รหัสสินค้า')
			->setCellValue('C1', 'ชื่อสินค้า')
			->setCellValue('D1', 'หน่วย')
			->setCellValue('E1', 'บริษัท')
			->setCellValue('F1', 'แผนก')
			->setCellValue('G1', 'ราคา');

// // Add some data
$objPHPExcel->setActiveSheetIndex(0)
			->fromArray(
				$q['res'], 
				NULL,        
				'A2'
			);

$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'Yusree');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Report');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
// header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment;filename=".date('Ymd-His')."_FFF.xlsx");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;

