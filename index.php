<?php
session_start();
require('_config_customer/_cfg.customer.req.inc.php');
$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="favicon_v.ico">
  <title></title>
  <link rel="stylesheet" href="css/main.css">
  <style>
html { width: 100%; height: 100%; min-height: 100%; }
body { height: 100%; min-height: 100%; padding: 0; margin: 0; }
#bg { width: 100%; min-height: 100%; position: relative; }
  </style>
</head>

<body>

<section id="bg" class="d-flex align-items-center bg-eee">
<div class="container">
  <div class="row">

    <div class="col-12 col-md-4 bg-white py-3">

      <div class="w-100 d-flex font-weight-bold px-2 pt-2 pb-3 align-items-center justify-content-center">
        <img src="v_logo.svg" alt="VMS Logo" height="24" class="mr-2"><span>เข้าสู่ระบบ</span>
      </div>

      <div class="w-100"><form autocomplete="off" name="main" id="main" class="form-horizontal" method="post" action="index_authen.php">
        <input name="a1" id="a1" type="text" style="display: none;">
        <input name="a2" id="a2" type="password" style="display: none;">
        <div class="form-group px-2 pt-2">
          <label for="username">ชื่อผู้ใช้</label>
          <input type="text" class="form-control" name="username" id="username" placeholder="ชื่อผู้ใช้" autocomplete="new-password"
          autofocus required onkeypress="return submitFormWithEnter(event,'main');">
        </div>
        <div class="form-group px-2 pt-1">
          <label for="password">รหัสผ่าน</label>
          <input type="password" class="form-control" name="password" id="password" placeholder="รหัสผ่าน" autocomplete="new-password"
          required onkeypress="return submitFormWithEnter(event,'main');"><input type="hidden" id="password2" name="password2" value="">
        </div>
      </form></div>

      <div class="w-100 p-2"><?php if($RESULT==9) { ?>
        <div class="w-100 py-2 px-0 fsz_rem09 text-center text-danger rounded">ชื่อ / รหัสผ่าน ผิด กรุณาลองอีกครั้ง</div>
      <?php } else if($RESULT==8) { ?>
        <div class="w-100 py-2 px-0 fsz_rem09 text-center text-warning rounded">กรุณาลองอีกครั้ง (เปิดค้างเกินกำหนดเวลา)</div>
      <?php } else if($RESULT==5) { ?>
        <div class="w-100 py-2 px-0 fsz_rem09 text-center text-success rounded">ออกจากระบบเรียบร้อย</div>
      <?php } else { ?>
        <div class="w-100 py-2 px-0 fsz_rem09 text-center rounded">&nbsp;</div>
      <?php } ?></div>

      <div class="w-100 px-2 pt-2 text-right">
        <button type="button" onclick="r('main');" class="btn btn-secondary mr-2">เริ่มใหม่</button><!--
        --><button type="button" onclick="x('main');" class="btn btn-primary px-5">ตกลง</button>
      </div>


    </div>

<!-- Right panel | start -->
    <div class="col-12 col-md-8 bg-cus1 text-cus1-txt d-flex align-items-center justify-content-center">

      <div class="py-2"><?php if($CFG_CUST['url_logo'] != "") { ?>
      <span class="d-block text-center"><img src="<?=$CFG_CUST['url_logo']?>" alt="Customer logo" height="80"></span><?php } else {
      ?><span class="d-block font-weight-bold fsz_rem32 m-2 text-center"><?=$CFG_CUST['name']?></span>
      <?php } ?><span class="d-block font-weight-bold fsz_rem16 m-2 text-center"><?=$CFG_CUST['slogan']?></span>
      </div>

    </div>
<!-- Right panel | finish -->

  </div>
</div>
</section>


<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>

<script>
var un = document.getElementById('username');
var pw = document.getElementById('password');
un.value = "";
pw.value = "";

function x(formID) {
  var f = document.getElementById(formID);
  f.action = 'index_authen.php';
  var a1 = document.getElementById('a1');
  var a2 = document.getElementById('a2');
  var un = document.getElementById('username');
  var pw = document.getElementById('password');
  var pw2 = document.getElementById('password2');
  if(un.value == '') { alert('Please input username');
  un.focus(); } else
  if(pw.value == '') { alert('Please input your password');
  pw.focus(); } else {
    //pw2.value = cnvStrMD5(pw.value);
    //pw.value = '';
    a1.value = '';
    a2.value = '';
    f.submit();
  }
}

function r(formID) {
  var f = document.getElementById(formID);
  f.reset();
  var un = document.getElementById('username');
  un.focus();
}

function cnvStrMD5(string) {
  var newStr = CryptoJS.MD5(string);
  return newStr;
}

function submitFormWithEnter(e,formID) {
  var charCode;
  if(window.event) // IE
  { charCode = e.keyCode; }
  else if(e.which) // Netscape/Firefox/Opera
  { charCode = e.which; }
if (charCode==13) {
    submitFormId(formID);
  } else { return true }
}
function submitFormId(formID) {
  x(formID);
}

<?php if($RESULT>0) { ?>window.history.replaceState(null, null, window.location.pathname);<?php } ?>
</script>