<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>


<?php $add_url_ = "index_product_add.php"; ?>

<main class="bg-eee col-12 col-md-9 col-xl-8 p-0" style="border-right: 1px solid #ddd; border-left: 1px solid #ddd;">

<div class="w-100 bg-ddd p-2">
  <a href="<?=$add_url_?>" class="btn btn-primary">Add product</a>
</div>



<div class="w-100 bg-eee d-flex align-items-center px-2 py-1">
  <span class="d-inline-block p-1"><span class="d-none d-md-inline-block">Show&nbsp;
  </span>
    <span class="font-weight-bold" id="vueXrecords_c0">{{ pagingDat.vueXrecords.c0 }}</span>-<span
    class="font-weight-bold" id="vueXrecords_cx">{{ pagingDat.vueXrecords.cx }}</span>
    of <span class="font-weight-bold" id="vueXrecords_ca">{{ pagingDat.vueXrecords.ca }}</span>
  </span>
  
  <span class="d-flex ml-auto p-0 align-items-center fsz_rem09">
    Show <select class="d-inline-block ml-2 bg-white" style="width: 60px; padding: 8px 4px; border-radius: 4px; border: none; text-align-last: right;"
        id="vue_pagingLimitOptions" onchange="setPagingLimit(this.options[this.selectedIndex].value)">
        <option v-for="option in pagingDat.vue_pagingLimitOptions" v-bind:value="option.value" class="text-right" dir="rtl"
         :selected="option.value == pagingData.prdLimit">
        {{ option.text }}
        </option>
      </select>
    
    <select class="d-inline-block ml-2 bg-white" style="padding: 8px 4px; border-radius: 4px; border: none; text-align-last: right;"
        id="vue_activeOptions" onchange="setPagingActive(this.options[this.selectedIndex].value)">
        <option v-for="option in pagingDat.vue_activeOptions" v-bind:value="option.value" class="text-right" dir="rtl"
         :selected="option.value == pagingData.prdActive">
        {{ option.text }}
        </option>
      </select>

  </span>
</div>


<div class="w-100 bg-white d-flex align-items-center justify-content-between px-2 py-1">
  <button class="btn btn-primary px-3 py-1" id="txt_filter_back" onclick="setPageNo(parseInt(localStorage.getItem('pageNo'))-1)">Back</button>
  
  <span class="d-inline-block p-1">Page
    <select class="d-inline-block bg-eee" style="width: 80px; padding: 8px 4px; border-radius: 4px; border: none; text-align-last: right;"
      id="vueXrecords_pages" onchange="setPageNo(this.options[this.selectedIndex].value)">
      <option v-for="i in pagingDat.vueXrecords.pages" v-bind:value="i" class="text-right" dir="rtl"
       :selected="i == pagingDat.pageNo">{{ i }}
      </option>
    </select>
  </span>
  
  <button class="btn px-3 py-1 btn-primary" id="txt_filter_next" onclick="setPageNo(parseInt(localStorage.getItem('pageNo'))+1)">Next</button>
</div>

<div class="container-fluid">
  <div class="row">
      <div class="col-md-12 py-1 px-2 bg-cus1 text-cus1-txt font-weight-bold d-flex align-items-center"
    >Brand: <?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname_disp']?> <a href="_sys.changebrand.php" class="ml-auto btn btn-sm btn-cus1-txt">Change</a></div>
  </div>
</div>


<div class="w-100 p-2">
<div class="container-fluid">
  <div class="row" id="products">

<!-- Product loop | start -->
<div class="col-6 col-sm-4 col-xl-3 p-1" v-for="product in products">
  <div class="bg-white px-2 py-1 border border-ddd rounded">
    <span class="d-flex align-items-center px-0 pt-2 pb-1 font-weight-bold">

      <button type="button" class="btn btn-sm btn-primary mr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="ion-navicon-round"></span>
      </button>
      <div class="dropdown-menu fsz_rem09 py-1">
        <a class="dropdown-item" :href="'javascript:vmsOpenModal(\'index_product_inven.if.php?product_code='+product.product_code+'\')'">Inventory</a>
        <div class="dropdown-divider my-0"></div>
        <a class="dropdown-item" :href="'https://test.matinoshoes.com/index_product_display.php?brandname='+product.brand_name+'&product_code='+product.product_code"
        target="_blank">Preview product</a>
        <div class="dropdown-divider my-0"></div>
        <a v-if="product.record_status == '0'" class="dropdown-item text-success" href="#">Activate</a>
        <a v-else class="dropdown-item disabled" href="javascript:void(0)">Activated</a>
        <a v-if="product.record_status == '1'" class="dropdown-item text-warning" href="#">Disable</a>
        <a v-else class="dropdown-item disabled" href="javascript:void(0)">Disabled</a>
      </div>
    {{ product.product_code }}</span>

    <span class="d-block px-0 py-1 rounded" style="overflow: hidden !important; border-top: 1px solid #eee; border-bottom: 1px solid #eee;">

      <div v-if="product.cover > 0" class="productPhoto">
        <img class="w-100" :src="product.cover_path">
      </div>
      <div v-else class="productPhoto d-flex bg-eee w-100 justify-content-center align-items-center">
      <span class="text-center font-weight-bold fsz_rem12">No<br>Photo</span>
      </div>

    </span>



    <div v-if="product.product_discount_retail > 0" class="d-flex align-items-center py-1">
      <!-- <a :href="'javascript:vmsOpenModal(\'index_product_price.if.php?product_code='+product.product_code+'\')'"
      class="btn btn-sm btn-eee fsz_rem08"><span class="ion-social-usd mr-1"></span><span class="d-none d-md-inline"> Edit</span></a></a> -->
      <span class="ml-auto text-danger fsz_rem08"><del>{{ product.product_price_retail.toLocaleString() }}</del></span>
      <span class="ml-1 font-weight-bold fsz_rem09 pl-1">{{ product.product_discount_retail.toLocaleString() }}</span>
    </div>
    
    <div v-else class="d-flex align-items-center py-1">
      <span class="ml-auto text-danger fsz_rem08"></span>
      <span class="ml-1 font-weight-bold fsz_rem09 pl-1">{{ product.product_price_retail.toLocaleString() }}</span>
    </div>
    
    
    <div class="d-flex align-items-center">
      <div class="btn-group w-100" role="group" aria-label="First group">
        <a :href="'javascript:vmsOpenModal(\'index_product_photo.if.php?product_code='+product.product_code+'\')'"
        class="btn btn-eee fsz_rem08 d-flex align-items-center">
          <span class="ion-images mr-1"></span><span class="d-none d-md-inline"> Photo</span>
        </a>
        <a :href="'index_product_add.php?product_code='+product.product_code" class="btn btn-block btn-primary"><span class="ion-edit mr-1"></span> Edit</a></a>
      </div>
    </div>
    
    
  </div>
</div>
<!-- Product loop | finish -->

  </div>
</div>

</div></main>



<?php require('_html.right_menu.inc.php'); ?>
</div></div>
<!-- Body | finish -->



<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>


<script>
// Paging
var pagingEnabled = 1

var pagingData = {
  vueXrecords: { c0: 0, cx: 0, ca: 0, pages: 0 },
  prdLimit: localStorage.getItem("prdLimit") ? localStorage.getItem("prdLimit") : 12,
  prdActive: localStorage.getItem("prdActive") ? localStorage.getItem("prdActive") : 'all',
  hash: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash']?>',
  vue_priceMin: 0,
  vue_priceMax: 999999,
  vue_viewType: {
    en: [
      { text: 'Grid', value: "grid" },
      { text: 'List', value: "list" },
    ],
    th: [
      { text: 'ช่อง', value: "grid" },
      { text: 'แถว', value: "list" },
    ]
  },
  vue_pagingLimitOptions: [
    { text: '12', value: "12" },
    { text: '24', value: "24" },
    { text: '48', value: "48" },
    { text: '96', value: "96" },
  ],
  vue_activeOptions: [
    { text: 'All', value: "all" },
    { text: 'Active', value: "1" },
    { text: 'Disabled', value: "0" },
  ],
}

var pagingDat = {}
var vueElementsPaging = []

if(pagingEnabled == 1) {
  for (var key in pagingData) {
    pagingDat[key] = pagingData[key]
    if(key.substring(0,4) == 'vue_') {
        vueElementsPaging[key] = new Vue({ el: '#'+key, data: { val: pagingDat } })
    } else if(key.substring(0,4) == 'vueX') {
        for (var key2 in pagingData[key]) {
          vueElementsPaging[key+"_"+key2] = new Vue({ el: '#'+key+"_"+key2, data: { val: pagingDat } })
        }
      } else { }
  }
}

var vp = new Vue({
  el: '#products',
  data: {
    products: [],
  },
  mounted() {
    this.get()
  },
  methods: {
    get: function() {
      //console.log(" | "+pagingDat.prdLimit)
      axios({
        method: 'get',
        url: './api/product/',
        params: {
          type: 'backend',
          limit: pagingDat.prdLimit,
          page: pagingDat.pageNo,
          hash: pagingDat.hash,
          active: pagingDat.prdActive,
          price_min: pagingDat.vue_priceMin,
          price_max: pagingDat.vue_priceMax,
        }
      })
      .then(function (response) {
        //console.log(response.data)
        vp.products = response.data.result
        pagingDat.vueXrecords = {
          c0: response.data.startRecord,
          cx: response.data.endRecord,
          ca: response.data.allRecords,
          pages: Math.ceil(response.data.allRecords/pagingDat.prdLimit),
        }
        localStorage.setItem("pages", Math.ceil(response.data.allRecords/pagingDat.prdLimit))
      })
      .then(function (response) {
        setTimeout(function(){
          setH('.productPhoto')
        }, 50)
      })
      .catch(function (error) {
        console.log(error)
      })
    }
  }
})


function setPageNo(pageNo) {
  console.log("page_no = "+pageNo)
  localStorage.setItem("pageNo", pageNo)
  pagingDat.pageNo = pageNo
  refreshList()
  if(pageNo == 1) {
    document.getElementById('txt_filter_back').disabled = true
  } else {
    document.getElementById('txt_filter_back').disabled = false
  }

  //console.log(pageNo+" | "+pagingDat.vueXrecords.c0)
  if(pageNo == localStorage.getItem("pages")) {
    document.getElementById('txt_filter_next').disabled = true
  } else {
    document.getElementById('txt_filter_next').disabled = false
  }
  
}
if (localStorage.getItem("pageNo") === null) { localStorage.setItem("pageNo", 1) }
//setPageNo(localStorage.getItem("pageNo"))


function setPagingLimit(pageLimit) {
  localStorage.setItem("prdLimit", pageLimit)
  pagingDat.prdLimit = pageLimit
  //console.log(pageLimit+" | "+pagingDat.prdLimit)
  //console.log(vp)
  setPageNo(1)
}

function setPagingActive(pageActive) {
  localStorage.setItem("prdActive", pageActive)
  pagingDat.prdActive = pageActive
  //console.log(pageLimit+" | "+pagingDat.prdLimit)
  //console.log(vp)
  setPageNo(1)
}

if (localStorage.getItem("prdLimit") === null) { localStorage.setItem("prdLimit", 12) }
if (localStorage.getItem("prdActive") === null) { localStorage.setItem("prdActive", 'all') }
setPagingLimit(localStorage.getItem("prdLimit"))
setPagingActive(localStorage.getItem("prdActive"))


function refreshList() {
  vp.get()
}


function sizingPrep() {
  setH('.productPhoto');
}

window.onresize = function() {
  sizingPrep();
}



<?php switch($RESULT) {
  case 1: $notifyTxt = "Product added successfully!"; break;
  case 2: $notifyTxt = "Product updated successfully!"; break;
  default: $notifyTxt = "";
} ?>

$.notify('<?=$notifyTxt?>', {
  position: 'bottom center',
  className: 'success'
});

<?php if($RESULT>0) { ?>window.history.replaceState(null, null, window.location.pathname);<?php } ?>
</script>