var vp = new Vue({
  el: '#product',
  data: {
    product: [],
  },
  mounted() {
    this.get()
  },
  methods: {
    get: function() {
      axios({
        method: 'get',
        url: './api/product/',
        params: {
          type: 'productName',
          hash: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash']?>',
          product_code: '<?=$product?>',
        }
      })
      .then(function (response) {
        //console.log(response)
        vp.product = response.data.result[0]
      })
      .catch(function (error) {
        console.log(error)
      })
    }
  }
})