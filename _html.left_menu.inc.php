<!-- Left Menu | start -->
<div id="leftMenu" class="col-12 col-md-3 col-xl-2 px-0">
<form id="leftMenuForm" name="leftMenuForm">

<!--
<div id="leftSearchBox" class="w-100 bg-white px-3 py-2">
  <label for="search" class="d-flex p-1 m-0 fsz_rem08 justify-content-center align-items-center">
    <img src="v_logo.svg" alt="VMS Logo" height="18" class="mr-1">Search</label>

  <div class="input-group">
    <input type="text" name="search" class="form-control" placeholder="Search">
    <div class="d-md-none input-group-append pl-1">
      <button class="btn btn-eee" type="button" data-toggle="collapse" data-target="#leftMenuNav"
      ><span class="ion-navicon-round"></span></button>
    </div>
  </div>

</div>
-->
<div id="leftMenuNav" class="collapse bd-links">

<?php foreach($CFG_MODULE as $k => $v): 
  if($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_role'] == 'superadmin' && ($v['name'] == 'ใบเสนอราคา' || $v['name'] == 'สินค้า' || $v['name'] == 'ผู้ใช้งาน')):
?>  
    <div class="leftMenu-nav w-100 px-0">
      <a href="<?=$v['url']?>" class="d-block px-3 py-2 font-weight-bold fsz_rem09<?=($SCRIPTNAME_===$v['url'] || in_array($SCRIPTNAME_,$v['url_list']))?' text-primary bg-eee':' text-x888'?>"><?=$v['name']?></a>
    </div>
<?php
  elseif($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_role'] == 'user' && $v['name'] == 'ใบเสนอราคา'):
?>
    <div class="leftMenu-nav w-100 px-0">
      <a href="<?=$v['url']?>" class="d-block px-3 py-2 font-weight-bold fsz_rem09<?=($SCRIPTNAME_===$v['url'] || in_array($SCRIPTNAME_,$v['url_list']))?' text-primary bg-eee':' text-x888'?>"><?=$v['name']?></a>
    </div>
<?php
  elseif($v['name'] == 'หน้าแรก'):
?>
    <div class="leftMenu-nav w-100 px-0">
      <a href="<?=$v['url']?>" class="d-block px-3 py-2 font-weight-bold fsz_rem09<?=($SCRIPTNAME_===$v['url'] || in_array($SCRIPTNAME_,$v['url_list']))?' text-primary bg-eee':' text-x888'?>"><?=$v['name']?></a>
    </div>
<?php
  endif;
endforeach; ?>
</div>

</form>
</div>
<!-- Left Menu | finish -->