<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>



<main class="bg-eee col-12 col-md-9 col-xl-10 p-2">

<div class="container">
  <div class="row">

<?php $i = 0; foreach($CFG_MODULE as $k => $v): if($i > 0) { 
  if($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_role'] == 'user' && $v['name'] == 'ใบเสนอราคา'):
  ?>
    <div class="col-12 col-sm-6 p-1">
      <div class="bg-white px-2 py-1 border border-ddd rounded">
        <span class="d-block p-2 font-weight-bold"><?=$v['name']?></span>
        <span class="d-block text-right py-1 font-weight-bold"><a href="<?=$v['url']?>" class="w-25 btn btn-sm btn-eee">Go</a></span>
      </div>
    </div>
<?php elseif($_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_role'] == 'superadmin' && ($v['name'] == 'ใบเสนอราคา' || $v['name'] == 'สินค้า' || $v['name'] == 'ผู้ใช้งาน')): ?>
  <div class="col-12 col-sm-6 p-1">
      <div class="bg-white px-2 py-1 border border-ddd rounded">
        <span class="d-block p-2 font-weight-bold"><?=$v['name']?></span>
        <span class="d-block text-right py-1 font-weight-bold"><a href="<?=$v['url']?>" class="w-25 btn btn-sm btn-eee">Go</a></span>
      </div>
    </div>
<?php
  endif;
} $i++; 
endforeach; ?>

  </div>
</div>

</main>



</div></div>
<!-- Body | finish -->


<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>
<script>

<?php switch($RESULT) {
  case 5: $notifyTxt = "เปลี่ยนรหัสผ่านสำเร็จ";
    break;
  default: $notifyTxt = "";
} ?>

$.notify('<?=$notifyTxt?>', {
  position: 'bottom center',
  className: 'success'
});

<?php if($RESULT>0) { ?>window.history.replaceState(null, null, window.location.pathname);<?php } ?>
</script>