<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$is_edit = isset($_GET['product_code']) ? 1:0;

$title = ($is_edit == 0) ? "Add new product": "Update product";


?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>


<main class="bg-eee col-12 col-md-9 col-xl-8 p-0" style="border-right: 1px solid #ddd; border-left: 1px solid #ddd;">


<!-- Header | start -->
<div class="container mb-2">
  <div class="row">
    <div class="col-md-12 py-3 bg-white font-weight-bold fsz_rem15 pt-4 text-center"><?=$title?></div>
    <div class="col-md-12 py-1 px-2 bg-cus1 text-cus1-txt font-weight-bold d-flex align-items-center"
    >Brand: <?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname_disp']?> <a href="_sys.changebrand.php" class="ml-auto btn btn-sm btn-cus1-txt">Change</a></div>
  </div class="row">
</div>
<!-- Header | finish -->



<?php
$formInput = [];
$i = 0;
$formInputREQ = [0 => "", 1 => " required "];
$formInputAF = [0 => "", 1 => " autofocus "];
$formInputDB = [0 => "", 1 => "REQ_"];
$formInputVmodel = [0 => "name", 1 => "v-model"];
# $formInput[ labelTxt, require?[0/1], v-model?[0/1], name, type, id, length ];

$formInput[$i] = ["Product code",$formInputDB[1],$formInputVmodel[0],"product_code", "text", "product_code", 100, $formInputREQ[1], $formInputAF[1], 'Unique product code']; $i++;
$formInput[$i] = ["Retail price",$formInputDB[1],$formInputVmodel[0],"product_price_retail", "number", "product_price_retail", 100, $formInputREQ[1], $formInputAF[1], 'Retail price']; $i++;
$formInput[$i] = ["Discount price",$formInputDB[1],$formInputVmodel[0],"product_discount_retail", "number", "product_discount_retail", 100, $formInputREQ[1], $formInputAF[1], 'Discount price']; $i++;
$formInput[$i] = ["Product name (EN)",$formInputDB[0],$formInputVmodel[1],"en_productName", "text", "en_productName", 100, $formInputREQ[1], $formInputAF[0], 'English name']; $i++;
$formInput[$i] = ["ชื่อสินค้า (ไทย)",$formInputDB[0],$formInputVmodel[1],"th_productName", "text", "th_productName", 100, $formInputREQ[0], $formInputAF[0], 'ชื่อภาษาไทย']; $i++;
$formInput[$i] = ["Tags",$formInputDB[1],$formInputVmodel[0],"product_tags", "tags", "product_tags", 255, $formInputREQ[0], $formInputAF[1], 'Add tags...']; $i++;

?>



<!-- <input class="w-100" name="REQ_tags" id="tags"> -->


<form id="main" name="main" method="post">

<div class="container" id="product">
  <!-- <input name="REQ_system_id" type="hidden" value="<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']?>"> -->
  <input name="REQ_product_name" type="hidden" v-model="productName">

<?php $ii = 100; foreach($formInput as $k => $v): ?>
  <div class="row py-1">
      <label class="col-12 col-sm-3 px-2 col-form-label d-flex align-items-center"
      for="<?=$v[1],$v[3]?>"><?=$v[0]?></label>
      <div class="input-group col-12 col-sm-9 px-2">
        <input tabindex="<?=$ii?>" type="<?=$v[4]?>" class="form-control" id="<?=$v[5]?>" <?=$v[2]?>="<?=$v[1],$v[3]?>" <?=$v[7],$v[8]?>
        placeholder="<?=$v[9]?>" onkeyup="countChar201803('<?=$v[5]?>',<?=$v[6]?>)" maxlength="<?=$v[6]?>">
        <div class="input-group-append fsz_rem06 d-none d-sm-flex text-right align-items-center" style="width: 55px;">
          <span class="w-100 p-1" id="<?=$v[5]?>_counter"><?=$v[6]?>/<?=$v[6]?></span>
        </div>
      </div>
  </div>
<?php $ii++; endforeach; ?>
</div>

<div class="container" id="attr">

  <div class="row px-2 py-1" v-for="(input, index) in inputs">

    <div class="col-3 col-sm-2 d-flex align-items-center p-1">
      <button type="button" class="btn btn-danger btn-block" @click="deleteRow(index)">Delete</button>
    </div>
    <div class="col-9 col-sm-3 d-flex align-items-center p-1">
      <span class="fsz_rem07 pr-2">#{{ index+1 }}</span> <input type="text" class="form-control" v-model="input.name" placeholder="Fieldname" required>
    </div>
    <div class="col-12 col-sm-7 d-flex align-items-center p-1">
      <input type="text" class="form-control" v-model="input.val" placeholder="Value" required>
    </div>

  </div>

  <div class="row">
    <div class="col-12 col-md-3 p-1">
      <!-- <span v-cloak>{{ v }}</span> -->
      <input type="hidden" name="REQ_attr" v-model="v" readonly="1">
      <?php if($is_edit == 1) { ?>
      <input type="hidden" name="REQ_is_edit" value="<?=$is_edit?>">
      <?php } ?>
      <button type="button" class="btn btn-block btn-info" @click="addRow()">Add field</button>
    </div>
    
    <div class="col-12 col-md-9 p-1 text-right">
      <button id="resetBtn" class="btn btn-secondary mr-2" type="reset" onclick="submitFormAPI('main')">Reset</button><!--
      --><button id="checkBtn" class="btn btn-primary px-5" type="button" onclick="addProduct('main')">Check</button>
      <!-- <button class="btn btn-primary px-5" type="button" onclick="addProduct('main')">Save</button> -->
      <!-- <button class="btn btn-primary px-5" type="button" onclick="submitFormAPI('main')">Save</button> -->
    </div>
    
    <div class="col-12 p-1">
      <button id="saveBtn" class="btn btn-primary btn-block px-5" type="submit" disabled>Save</button>
    </div>
    
    <div class="col-12 p-1">
      <input type="hidden" name="product_tags" id="out"></p>
      <textarea id="html_desc" name="html_desc"></textarea>
    </div>
    
  </div>

  

</div>

</form>


</main>


<?php require('_html.right_menu.inc.php'); ?>
</div></div>
<!-- Body | finish -->

<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>

</html>
<script>
var vm = new Vue({
  el: '#product',
  data: {
    en_productName: '',
    th_productName: '',
  },
  computed: {    
    productName: {
      get: function() {
        var name = {
          default: { product_name: this.en_productName || this.th_productName || "undefined" },
          en: {
            product_name: this.en_productName,
          },
          th: {
            product_name: this.th_productName,
          }
        }
        //return name
        return JSON.stringify(name)
      },
      set: function(newValue) {
        // mmmm
      }
    }
  }
})

// Inputs is ATTR from DB
const app = new Vue({
  el: '#attr',
  data: {
    inputs: []
  },
  computed: {
    v: {
      get: function() {
        var V = {
          attr: this.inputs
        }
        //console.log(V)
        return JSON.stringify(V)
        //return V
      },
      set: function(newValue) {
        console.log(newValue.i)
        console.log("Set: "+newValue)
      }
    }
  },
  methods: {
    addRow(index) {
      this.inputs.push({
        name: '',
        val: '',
        i: index
      })
    },
    deleteRow(index) {
      this.inputs.splice(index,1)
    }
  }
})


var b = 0;
function addProduct(d) {
  var a = document.getElementById('product_code')
  a.value = a.value.toUpperCase()
  var f = document.getElementById(d)
  var s = document.getElementById('saveBtn')
  var paramprd = {
    type: 'checkUnique',
    product_code: a.value,
    hash: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash']?>',
  }

  $.ajax({
    url: './api/product/',
    data: paramprd,
    method: 'GET',
    success: function(response) {
      <?php if($is_edit == 1) { echo "b = 0;"; } else { echo "b = response.allRecords;"; } ?>
      if(a.value == null || a.value == '' || b > 0) {
        s.disabled = true
        f.action = ''
        if(b > 0) {
          $("#product_code").notify("This product code is already exists", "error");
        } else {
          $("#product_code").notify("Please insert product code", "warn");
        }
        a.select()
      } else {
        s.disabled = false
        f.action = 'index_product_add.exec.php'
      }
    }
  })
}



  
function showResponse() { 
  <?php if($is_edit == 1) { echo "location.href = 'index_product.php?result=2'"; }
  else { echo "location.href = 'index_product.php?result=1';"; } ?>
}

var formOptions = { 
  target: '#main',
  url: 'index_product_add.exec.php',
  //beforeSubmit: showRequest,
  success: showResponse,
  type: 'post',
  clearForm: true,
  //resetForm: true,
}

$('#main').submit(function() { 
  $(this).ajaxSubmit(formOptions); 
  //console.log(formOptions)
  return false; 
})

<?php if($is_edit == 1) { ?>
function loadData() {
  //console.log('<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash']?>')
  var data = {
    type: 'productDetail',
    product_code: '<?=$_GET['product_code']?>',
    hash: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash']?>',
  }
  axios({
    method: 'get',
    url: './api/product/',
    params: data
  })
  .then(function (response) {
    var xx = JSON.parse(response.data.result[0].attr)
    product_code.value = response.data.result[0].product_code
    //console.log(response.data.result[0])
    product_price_retail.value = response.data.result[0].product_price_retail
    product_discount_retail.value = response.data.result[0].product_discount_retail
    product_tags.value = response.data.result[0].product_tags
    initTagsInput()
    
    var html_desc = document.getElementById('html_desc')
    $("#html_desc").summernote("code", response.data.result[0].html_desc);
    product_code.readOnly = true
    vm.en_productName = response.data.result[0].en_productName
    vm.th_productName = response.data.result[0].th_productName
    for(var i = 0; i < xx.length; i++) {
      app.inputs.push( xx[i] )
    }
  })
  .catch(function (error) {
    console.log(error)
  })
  document.getElementById('resetBtn').disabled = true
}
loadData()
<?php } ?>


$(document).ready(function() {
  $('#html_desc').summernote({
    placeholder: 'HTML Description of product',
    fontNames: ['Tahoma', 'Arial', ],
    tabsize: 1,
    height: 150,
  });
});



/* TAGS */
// log events as they happen:
function initTagsInput() {
  var t = $('#product_tags')[0];
  t.addEventListener('input', log);
  t.addEventListener('change', log);
  function log(e) {
    $('#out')[0].value = `${this.value.replace(/,/g,', ')}`;
  }
  // hook 'em up:
  Array.from($('input[type="tags"]')).forEach(tagsInput);
}
<?php if($is_edit == 0) { ?>
initTagsInput()
<?php } ?>
</script>