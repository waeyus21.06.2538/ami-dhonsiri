<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="favicon_v.ico">
  <title><?=$CFG_CUST['name']?> - Management System</title>
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="lib/selectize.css">
</head>