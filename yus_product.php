<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$RESULT = isset($_GET['result']) ? $_GET['result'] : 0;
?>

<body>
    <?php require('_html.header.inc.php'); ?>
    <!-- Body | start -->
    <div class="container-fluid">
        <div class="row">
            <?php require('_html.left_menu.inc.php'); ?>
            <?php 
                $add_url_ = "yus_product_add.php"; 
                $excel_url_ = "yus_excel.php"; 
            ?>

            <main class="bg-eee col-12 col-md-9 col-xl-10 p-0"
                style="border-right: 1px solid #ddd; border-left: 1px solid #ddd;">
                
                <form id="formCategory" name="category">
                <article class="container">
                    <div class="row">

                        <!--search item | start -->
                        <div class="col-12 col-md-6 d-flex align-items-center py-2">
                            <div class="input-group w-100">
                            <select class="w-100" id="itemm" placeholder="2"></select>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" onclick="search();">
                                <span class="ion-search"></span> ค้นหา
                                </button>
                            </div>
                            </div>
                        </div>
                        
                        <div class="col-12 offset-md-2 col-md-4 align-items-center mt-2">
                            <!--Add item-->
                            <a href="<?=$add_url_?>" class="btn btn-block btn-primary">เพิ่มสินค้า</a>
                            <a href="<?=$excel_url_?>" class="btn btn-block btn-primary">
                                Export ข้อมูลสินค้าทั้งหมด 
                                <small>( ไฟล Excel )</small>
                            </a>
                        </div>
                    </div>
                </article>
                </form>
                <hr>

                    <div class="container"><div class="row">
                    <div class="col-12 d-block d-md-flex align-items-center justify-content-between">
                        <span class="d-flex align-items-center form-inline">
                        <button class="btn btn-primary m-1" id="txt_filter_back"
                        onclick="setPageNo(parseInt(localStorage.getItem('pageNo'))-1)">Back</button>
                        <button class="btn btn-primary m-1" id="txt_filter_next"
                        onclick="setPageNo(parseInt(localStorage.getItem('pageNo'))+1)">Next</button>

                        Page&nbsp;&nbsp;<select class="d-inline-block"
                                style="width: 80px; padding: 8px 4px; border-radius: 4px; border: none; text-align-last: right;"
                                id="vueXrecords_pages" onchange="setPageNo(this.options[this.selectedIndex].value)">
                                <option v-for="i in pagingDat.vueXrecords.pages" v-bind:value="i" class="text-right"
                                    dir="rtl" :selected="i == pagingDat.pageNo">{{ i }}
                                </option>
                            </select>
                        </span>
            
                        <span class="d-inline-block p-1"><span class="d-none d-md-inline-block">Show&nbsp;
                        </span>
                        <span class="font-weight-bold" id="vueXrecords_c0">{{ pagingDat.vueXrecords.c0 }}</span>-<span
                            class="font-weight-bold" id="vueXrecords_cx">{{ pagingDat.vueXrecords.cx }}</span>
                        of <span class="font-weight-bold" id="vueXrecords_ca">{{ pagingDat.vueXrecords.ca }}</span>
                        </span>

                        <span class="d-inline-block form-inline">
                        Show <select class="d-inline-block ml-2 bg-white"
                            style="width: 60px; padding: 8px 4px; border-radius: 4px; border: none; text-align-last: right;"
                            id="vue_pagingLimitOptions"
                            onchange="setPagingLimit(this.options[this.selectedIndex].value)">
                            <option v-for="option in pagingDat.vue_pagingLimitOptions" v-bind:value="option.value"
                                class="text-right" dir="rtl" :selected="option.value == pagingData.prdLimit">
                                {{ option.text }}
                            </option>
                        </select>
                        </span>
                    </div>

                    </div></div>

                <div class="w-100 p-2">
                <div class="container"><div class="row" id="products">
                            <!-- <div v-if=""
                            class="col-12 text-center p-5 font-weight-bold">No records found.</div>
                            <div v-else class="col-12 py-2"> -->
                            <table style="font-size: 0.8rem;"
                                class="table table-bordered table-striped table-sm bg-white">
                            <thead>
                            <tr>
                                <th class="text-center" width="50">#</th>
                                <th width="120">รหัสสินค้า</th>
                                <th width="80">ยี่ห้อ</th>
                                <th width="120">ชื่อสินค้า</th>
                                <th width="80">หน่วย</th>
                                <th width="120">บริษัท</th>
                                <th width="120">ราคา</th>
                                <th class="text-center" width="50">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <!-- Product loop | start -->
                            <tr v-for="(product, index) in products">
                                <td class="text-center align-middle">{{ pagingDat.vueXrecords.c0+index }}</td>
                                <td class="font-weight-bold align-middle">{{product.item_code}}</td>
                                <td class="align-middle">{{product.cmdy_code}}</td>
                                <td class="align-middle">{{product.item_name_th}}</td>
                                <td class="align-middle">{{product.unit}}</td>

                                <!-- vue start if -->
                                <td v-if="product.co == 1" class="align-middle">บริษัท ธนศิริดีเซล จำกัด</td>
                                <td v-else-if="product.co == 2" class="align-middle">บริษัท ธนเสริม จำกัด</td>
                                <td v-else-if="product.co == 3" class="align-middle">บริษัท รีแดท (ไทยเเลนด์) จำกัด</td>
                                <td v-else class="align-middle"> ไม่มีข้อมูล</td>
                                <!-- vue end if -->

                                <td class="align-middle">{{product.default_price | numFormat}}</td>
                                <td class="d-flex justify-content-center">
                                    <a :href="'yus_product_add.php?item_code='+product.item_code+'&cmdy_code='+product.cmdy_code"
                                        class="btn btn-primary m-1"><span class="ion-edit mr-1"></span>
                                        แก้ไข</a>
                                    <button class="btn btn-danger m-1 btn-delete" :data-item-code="product.item_code" :data-cmdy-code="product.cmdy_code">
                                        <span class="ion-trash-a mr-1"></span>ลบ
                                    </button>
                                </td>
                            </tr> 
                            <!-- Product loop | finish -->
                            </tbody>
                            </table>
                            </div>
                        </div></div>


                        </div>
                    </div>

                </div>
            </main>



            <?php //require('_html.right_menu.inc.php'); ?>
        </div>
    </div>
    <!-- Body | finish -->



    <?php require('_html.footer.inc.php'); ?>
    <?php require('_html.footer_js.req.inc.php'); ?>
</body>

</html>


<script>
    // Paging
    var pagingEnabled = 1

    var pagingData = {
        vueXrecords: {
            c0: 0,
            cx: 0,
            ca: 0,
            pages: 0
        },
        prdLimit: localStorage.getItem("prdLimit") ? localStorage.getItem("prdLimit") : 12,
        prdActive: localStorage.getItem("prdActive") ? localStorage.getItem("prdActive") : 'all',
        hash: '<?=$_SESSION[$SYSNAME__.'
        _ '.$CFG_CUST['
        abbr '].'
        _ '.'
        hash ']?>',
        vue_priceMin: 0,
        vue_priceMax: 999999,
        vue_viewType: {
            en: [{
                    text: 'Grid',
                    value: "grid"
                },
                {
                    text: 'List',
                    value: "list"
                },
            ],
            th: [{
                    text: 'ช่อง',
                    value: "grid"
                },
                {
                    text: 'แถว',
                    value: "list"
                },
            ]
        },
        vue_pagingLimitOptions: [{
                text: '10',
                value: "10"
            },
            {
                text: '30',
                value: "30"
            },
            {
                text: '50',
                value: "50"
            },
            {
                text: '100',
                value: "100"
            },
        ],
        vue_activeOptions: [{
                text: 'All',
                value: "all"
            },
            {
                text: 'Active',
                value: "1"
            },
            {
                text: 'Disabled',
                value: "0"
            },
        ],
    }

    var pagingDat = {}
    var vueElementsPaging = []

    if (pagingEnabled == 1) {
        for (var key in pagingData) {
            pagingDat[key] = pagingData[key]
            if (key.substring(0, 4) == 'vue_') {
                vueElementsPaging[key] = new Vue({
                    el: '#' + key,
                    data: {
                        val: pagingDat
                    }
                })
            } else if (key.substring(0, 4) == 'vueX') {
                for (var key2 in pagingData[key]) {
                    vueElementsPaging[key + "_" + key2] = new Vue({
                        el: '#' + key + "_" + key2,
                        data: {
                            val: pagingDat
                        }
                    })
                }
            } else {}
        }
    }

    var vp = new Vue({
        el: '#products',
        data: {
            products: [],
        },
        mounted() {
            this.get()
        },
        methods: {
            get: function () {
                //console.log(" | "+pagingDat.prdLimit)

                var itemValue = $('#itemm').val();

                axios({
                        method: 'get',
                        url: './api/product/',
                        params: {
                            type: 'backend',
                            page_limit: pagingDat.prdLimit,
                            page: pagingDat.pageNo,
                            hash: pagingDat.hash,
                            active: pagingDat.prdActive,
                            price_min: pagingDat.vue_priceMin,
                            price_max: pagingDat.vue_priceMax,
                            itemValue: itemValue,
                        }
                    })
                    .then(function (response) {
                        console.log(response.data.result)
                        vp.products = response.data.result

                        // alert(vp.products[0].item_code);
                        pagingDat.vueXrecords = {
                            c0: response.data.startRecord,
                            cx: response.data.endRecord,
                            ca: new Intl.NumberFormat().format(response.data.allRecords),
                            pages: Math.ceil(response.data.allRecords / pagingDat.prdLimit),
                        }
                        localStorage.setItem("pages", Math.ceil(response.data.allRecords / pagingDat
                            .prdLimit))
                    })
                    .then(function (response) {
                        setTimeout(function () {
                            setH('.productPhoto')
                        }, 50)
                    })
                    .catch(function (error) {
                        console.log(error)
                    })
            }
        }
    })


    function setPageNo(pageNo) {
        console.log("page_no = " + pageNo)
        localStorage.setItem("pageNo", pageNo)
        pagingDat.pageNo = pageNo
        refreshList()
        if (pageNo == 1) {
            document.getElementById('txt_filter_back').disabled = true
        } else {
            document.getElementById('txt_filter_back').disabled = false
        }

        //console.log(pageNo+" | "+pagingDat.vueXrecords.c0)
        if (pageNo == localStorage.getItem("pages")) {
            document.getElementById('txt_filter_next').disabled = true
        } else {
            document.getElementById('txt_filter_next').disabled = false
        }

    }
    if (localStorage.getItem("pageNo") === null) {
        localStorage.setItem("pageNo", 1)
    }
    //setPageNo(localStorage.getItem("pageNo"))


    function setPagingLimit(pageLimit) {
        localStorage.setItem("prdLimit", pageLimit)
        pagingDat.prdLimit = pageLimit
        //console.log(pageLimit+" | "+pagingDat.prdLimit)
        //console.log(vp)
        setPageNo(1)
    }

    function setPagingActive(pageActive) {
        localStorage.setItem("prdActive", pageActive)
        pagingDat.prdActive = pageActive
        //console.log(pageLimit+" | "+pagingDat.prdLimit)
        //console.log(vp)
        setPageNo(1)
    }

    if (localStorage.getItem("prdLimit") === null) {
        localStorage.setItem("prdLimit", 12)
    }
    if (localStorage.getItem("prdActive") === null) {
        localStorage.setItem("prdActive", 'all')
    }
    setPagingLimit(localStorage.getItem("prdLimit"))
    setPagingActive(localStorage.getItem("prdActive"))


    function refreshList() {
        vp.get()
    }


    function sizingPrep() {
        setH('.productPhoto');
    }

    window.onresize = function () {
            sizingPrep();
        }

<?php
    switch ($RESULT) {
        case 1:
            $notifyTxt = "Product added successfully!";
            break;
        case 2:
            $notifyTxt = "Product updated successfully!";
            break;
        default:
            $notifyTxt = "";
    } ?>

    $.notify('<?=$notifyTxt?>', {
        position: 'bottom center',
        className: 'success'
    });

<?php if ($RESULT > 0) {?> 
    window.history.replaceState(null, null, window.location.pathname); 
<?php }?>

// ---------------------------------------

$(document).on('click','.btn-delete', function(){
    var item_code = $(this).attr('data-item-code');
    var cmdy_code = $(this).attr('data-cmdy-code');

    var r = confirm("กรุณายืนยันการลบข้อมูลสินค้า");
    if (r == true) {
        $.ajax({
            type: "POST",
            url: './api/product/index.php',
            data: {
                type: 'item_delete',
                item_code: item_code,
                cmdy_code: cmdy_code
            },
            success: function(response){
                if(response.result == null) {
                    $.notify("ลบข้อมูลสำเร็จ",{position: 'bottom center',className: 'success'});
                    setTimeout(function(){ 
                        vp.get();
                    }, 3000);
                } else {
                    $.notify("ลบข้อมูลไม่สำเร็จ",{position: 'bottom center',className: 'error'});
                }
            },
            error: function (response) {
                console.log('An error occurred.');
            },
        });
        console.log('Ok')
    } else {
        console.log('Noooooo')
    }
});

function search() {
    vp.get()
}

function getItemCode() {
    var item = document.getElementById('itemm');
    var itemValue = item.options[item.selectedIndex].value;
    console.log(itemValue)
    return itemValue
}

var $select = $('#itemm').selectize({
  valueField: 'item_key',
  searchField: 'item_code',
  placeholder: 'ใส่รหัสอย่างน้อย 4 หลัก',
  onClear: function() {
    $('#btnAddItemQo').attr('disabled',true)
  },
  onDelete: function() {
    $('#btnAddItemQo').attr('disabled',true)
  },
  onItemAdd:  function() {
    $('#btnAddItemQo').attr('disabled',false)
  },
  render: {
    option: function(data, escape) {
      return '<div>'
      + '<strong>'       
      + escape(data.item_code) + ' - ' 
      + '</strong>(' 
      + escape(data.cmdy_name) + ') ' 
      + escape(data.item_name_th)
      + '</div>';
    },
    item: function(data, escape){
      return '<div>'
      + '<strong>'       
      + escape(data.item_code) + '- ' 
      + '</strong>(' 
      + escape(data.cmdy_name) + ') ' 
      + escape(data.item_name_th)
      + '</div>';
    }
  },
  options: [],
  persist: false,
  loadThrottle: 600,
  create: false,
  allowEmptyOption: true,
  load: function(query, callback) {
    if(!query.length) return callback()
    $.ajax({
      url: './api/item/',
      type: 'GET',
      dataType: 'json',
      data: {
        type: 'detail',
        keyword: query,
      },
      error: function() {
        callback();
      },
      success: function(res) {
        callback(res.result);
        console.log(res.result)
      }
    });
  }
})
</script>