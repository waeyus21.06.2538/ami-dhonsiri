<?php

$CFG_CUST = [];

$CFG_CUST['abbr'] = "dsg";
$CFG_CUST['name'] = "Dhonsiri Group";
$CFG_CUST['fullname'] = "Dhonsiri Group";

$CFG_CUST['slogan'] = "Management Information System";
$CFG_CUST['system_id'] = 3;

$CFG_CUST['url_logo'] = "_config_customer/Dhonsiri-Logo-trans.png";
$CFG_CUST['url_logo_notext'] = "";