<?php

if(!session_id()) { session_start(); }
$session_id = session_id();

$ROOT_LEVEL = 0; $ROOT_DS_ = '';
$SESSION_PREFIX_LEN = strlen($SESSION_PREFIX);
$DS__ = DIRECTORY_SEPARATOR;
//$DS__ = '\\';

$today_ = date('Y-m-d');
$now_ = date('Y-m-d H:i:s');

$remote_ip_ = "inet_aton('".$_SERVER['REMOTE_ADDR']."')";
$full_date_ = date('Y-m-d'); $full_time_ = date('H:i:s');
$leapYear_ = date('L'); $year_ = date('Y');
$month_ = date('m'); $monthLong_ = strtolower(date('F'));
$day_ = strtolower(date('D')); $dow_ = date('w');
$dayLong_ = strtolower(date('l')); $date_ = date('d');
$dateJ_ = date('j'); $hour_ = date('H');

// COOKIE's aging
$secInMin = 60; $minInHour = 60; $hourInDay = 24; $dayInWeek = 7;
$secInHour = $secInMin * $minInHour;
$secInDay = $secInHour * $hourInDay;
$secInWeek = $secInDay * $dayInWeek;
$COOKIE_aging_day = 14;

// DATETIME in Server local timezone
$dd__ = date('d'); $mm__ = date('m'); $yyyy__ = date('Y'); $yy__ = date('y');
$hh__ = date('H'); $ii__ = date('i'); $ss__ = date('s'); $dow__ = date('w');
$now__ = date('Y-m-d H:i:s'); $today__ = date('Y-m-d'); $timenow__ = date('H:i:s');

$scriptName = explode('/',$_SERVER['SCRIPT_NAME']);
$queryString = $_SERVER['QUERY_STRING'];
$scriptName2 = $scriptName[count($scriptName)-1];

?>