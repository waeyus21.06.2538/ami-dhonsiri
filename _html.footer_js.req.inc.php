<!-- Footer JS | start -->
<script src="lib/vue.min.js"></script>
<script src="lib/axios.min.js"></script>
<script src="lib/jquery-3.2.1.min.js"></script>
<script src="lib/jquery.form.min.js"></script>
<script src="lib/bootstrap.bundle.min.js"></script>
<script src="lib/hash_md5.js"></script>
<script src="lib/selectize.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.3.1/web-animations.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/muuri/0.5.3/muuri.min.js"></script> -->
<!-- <script src="../lib/web-animations.min.js"></script> -->
<!-- <script src="../lib/hammer.min.js"></script> -->
<script src="lib/jquery.tagsinput.min.js"></script>
<script src="lib/muuri.min.js"></script>
<script src="lib/dropzone.js"></script>
<script src="lib/notify.min.js"></script>
<script src="lib/tags-input.js"></script>
<script src="lib/summernote/summernote-bs4.min.js"></script>
<script src="js/main.js"></script>
<!-- Footer JS | finish -->