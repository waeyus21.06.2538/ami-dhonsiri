<?php

/** Include PHPExcel */
require('lib/excel/PHPExcel.php');
require('inc/require.php');
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=".date('Ymd-His')."_report.xls");

$sql = "select cmdy_code 'ยีห้อ', item_code 'รหัสสินค้า', item_name_th 'ชื่อสินค้า', unit 'หน่วย', co 'บริษัท', dept 'แผนก', default_price 'ราคา'
from `dhonsiri`.`@ms_item` a where a.record_status = 'A';";
// echo $sql;
$q = mysqliQuery($sql);
// echo "<pre>";
//     var_dump($q['res']);
// echo "</pre>";
// exit();

echo '
<table border="1">
<thead>
<tr><td colspan="7" style="text-align: center;"><b>รายงานสินค้าทั้งหมด ('.date('Y/m/d - H:i:s').')</b></td></tr>
<tr>';
foreach($q['res'][0] as $k => $v) {
    echo '<td><b>',$k,'</b></td>';
}
echo '
</tr>
</thead>
<tbody>
';
foreach($q['res'] as $k => $v) {
echo '<tr>';
    #echo $k,' = ';
    foreach($v as $kk => $vv) {
        echo '<td>',$vv,'</td>';
        #echo $kk,' = ',$vv,'<br>';
    }
echo '</tr>';
}
echo '
</tbody>
</table>
';
