<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');

$username_ = isset($_POST['username']) ? strtolower($_POST['username']) : '';
$password_ = isset($_POST['password']) ? $_POST['password'] : '';

$s1 = "select password p,system_id sid,role from `{$DBNAME__}`.`@user_backend` where system_id = ? and lower(username) = ? and active = 1;";
$b1[] = ['i',$CFG_CUST['system_id']];
$b1[] = ['s',$username_];
$q1 = mysqliQuery($s1,$b1);
$pwd = $q1['res'][0]['p'];

#echo $password_,' = ',$pwd,'<br>';

$verified = password_verify($password_,$pwd);
#var_dump($verified);

session_regenerate_id();

if($verified):


  $sCo = "select SUBSTRING_INDEX(company,',',1) co from `{$DBNAME__}`.`@user_backend` where username = '{$username_}';";
  $qCo = mysqliQuery($sCo);
  #var_dump($qCo);

  $url = "index_main.php";
  $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'last_activity'] = time();
  $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username'] = $username_;
  $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_co'] = $qCo['res'][0]['co'];
  $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id'] = $q1['res'][0]['sid'];
  $_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'user_role'] = $q1['res'][0]['role'];
  $action = "login_passed";
else:
  $url = "index.php?result=9";
  $action = "login_failed";
endif;

/*
$sl = "insert into `{$DBNAME__}`.`@log` (`log_dt`,`log_tm`,`log_y`,`log_m`,`log_d`,`log_h`,`log_i`,`log_s`
,`log_action`,`system_id`,`username`,`phpsessionid`,`ip`,`ip_no`) values (
'{$dt}','{$tm}','{$yyyy__}','{$mm__}','{$dd__}','{$hh__}','{$ii__}','{$ss__}'
,'{$action}','{$CFG_CUST['system_id']}','{$username_}','".session_id()."','{$ip_}',{$ip_no_});";
*/
$sl = "insert into `{$DBNAME__}`.`@log` (`log_dt`,`log_tm`,`log_action`,`system_id`,`username`,`phpsessionid`,`ipv4`) values (
'{$dt}','{$tm}','{$action}','{$CFG_CUST['system_id']}','{$username_}','".session_id()."','{$ip_}');";
$ql = mysqliQuery($sl);

$sl2 = "update `{$DBNAME__}`.`@log` set log_y = year(log_dt),log_m = month(log_dt),log_d = day(log_dt)
,log_h = hour(log_tm),log_i = minute(log_tm),log_s = second(log_tm),ip_no = inet_aton(ipv4) where log_y is null;";
$ql2 = mysqliQuery($sl2);

echo "<script>window.location.href = '".$url."';</script>";
#echo $url;