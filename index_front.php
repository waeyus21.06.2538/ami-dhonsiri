<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');
?>
<body>
<?php require('_html.header.inc.php'); ?>
<!-- Body | start -->
<div class="container-fluid"><div class="row">
<?php require('_html.left_menu.inc.php'); ?>


<main class="bg-eee col-12 col-md-9 col-xl-8 p-2">

<div class="container">
  <div class="row">
<?php foreach($CFG_MODULE[$modID]['sub'] as $k => $v): ?>

<div class="col-12 col-sm-4 p-1">
  <div class="bg-white px-2 py-1 border border-ddd rounded">
    <span class="d-block p-2 font-weight-bold"><?=$v['name']?></span>
    <span class="d-block text-right py-1 font-weight-bold"><a href="<?=$v['url']?>" class="w-25 btn btn-sm btn-eee">Go</a></span>
  </div>
</div>

<?php endforeach; ?>

  </div>
</div>

</main>



<?php require('_html.right_menu.inc.php'); ?>
</div></div>
<!-- Body | finish -->

<?php require('_html.footer.inc.php'); ?>
<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>
<script>

</script>