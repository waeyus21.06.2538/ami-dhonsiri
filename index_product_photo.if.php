<?php require('inc/require.php');
require('_config_customer/_cfg.customer.req.inc.php');
require('_config_module/_cfg.module.req.inc.php');
require('inc/session_chk.php');
require('_html.head.inc.php');

$title = "Manage photo";
$product = $_GET['product_code'] ?: "";
?>
<body>

<div class="container-fluid"><div class="row">
<?php include('index_product_iframe_title.if.php'); ?>

<!-- action | start -->


<style>
.dz-preview.dz-image-preview { background: none !important; margin: 0 0 8px 8px !important; }
.dz-remove { color: #ff0 !important; }
</style>

<div class="col-12 p-2">
  <form method="post" action="index_product_photo_handler.if.php"
  class="dropzone w-100 rounded bg-primary text-white text-center p-4 fsz_rem11" id="dropzone01" method="post" enctype="multipart/form-data">
  <div class="fallback">
    <input id="imgUploaderProduct" name="file[]" type="file" multiple />
    <input type="hidden" name="product_code" value="<?=$product?>">
  </div>
  <!-- <button type="submit" class="btn btn-outline-primary btn-sm mt-1 px-4">Upload</button> -->
  </form>
  <!-- <button type="button" onclick="submitForm('dropzone01');" class="btn btn-primary btn-sm mt-1 px-4">Upload</button> -->
</div>

</div>


<div class="row bg-eee py-3 px-2">
<?php
$sqlPhoto = "select a.*,ifnull(c.photo_id,0) cover
 from `{$DBNAME__}`.`@product_photo` a
 left join `{$DBNAME__}`.`@product_photo_cover` c on c.system_id = a.system_id and c.brand_name = a.brand_name and c.product_code = a.product_code and a.photo_id = c.photo_id and c.record_status = '1'
 where a.system_id = {$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'system_id']} and a.brand_name = '{$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'brandname']}'
 and a.product_code = '{$product}' order by a.ordering,a.crt_dtm,photo_id;";
$qPhoto = mysqliQuery($sqlPhoto);
$i = 0; foreach($qPhoto['res'] as $k => $v) { ?>

  <div class="col-6 col-md-3 col-xl-2 p-1 movingBlock" id="mvb_<?=$v['photo_id']?>"
  data-photo-id="<?=$v['photo_id']?>">
  <div class="bg-white p-2 d-flex align-items-center">
    <script>
    var param<?=$i?> = {
      hash: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'hash']?>',
      product_code: '<?=$product?>',
      usr: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
      photo_id: <?=$v['photo_id']?>,
      type: 'setCover',
    }
    </script>
    <button class="btn btn-eee btn-sm fsz_rem08"
    onclick="moveBlock('before','movingBlock','mvb_<?=$v['photo_id']?>')"
    >Move</button>
    <button class="ml-auto btn btn-eee btn-sm fsz_rem08"
    onclick="moveBlock('after','movingBlock','mvb_<?=$v['photo_id']?>')"
    >Move</button>
  </div>
  <div class="bg-white px-2">
    <span class="d-block p-1 bg-white w-100 text-center">
      <img src="<?=$v['path'],'sq2.',$v['filename_re']?>" class="w-100">
    </span>
  </div>
  
  <div class="bg-white p-2 d-flex align-items-center">
    <div class="btn-group w-100" role="group"><?php if($v['cover'] != $v['photo_id']) { ?>
      <button type="button" class="btn btn-ccc w-100" onclick="axiosCallReload(param<?=$i?>,'./api/product/')"
    ><span class="d-none d-md-inline-block mr-2 ion-star"></span> Set cover</button><?php } else { ?>
    <button type="button" class="btn btn-eee w-100 disabled">Current cover</button>
    <?php } ?>

      <div class="btn-group" role="group">
        <button id="btnGroupDrop1" type="button" class="btn btn-eee dropdown-toggle" data-toggle="dropdown"></button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#"><span class="ion-trash-a"></span> Delete</a>
        </div>
      </div>
    </div>
  </div>
  
  </div>

<?php $i++; } ?>
<!-- action | finish -->


</div></div>



<?php require('_html.footer_js.req.inc.php'); ?>
</body>
</html>



<script>
<?php include('index_product_iframe_title.vue.php'); ?>



var acceptedFileTypes = "image/*";

//Dropzone.autoDiscover = false;
Dropzone.options.dropzone01 = {
  init: function() {
    //this.on("addedfile", function(file) { alert("Added file."); });
    this.on("success", function(file) { window.location.href = window.location.href; });
    this.on("complete", function(file) { console.log(file + "Added file."); });
  },
  paramName: "file",
  uploadMultiple: true,
  params: {
    product_code: '<?=$product?>'
  },
  maxFilesize: 20000,
  resizeWidth: 2880,
  maxFiles: 100,
  acceptedFiles: acceptedFileTypes,
  parallelUploads: 10,
  forceFallback: false,
  autoProcessQueue: true,
  addRemoveLinks: true,  
  dictCancelUpload: "Cancel upload",
  dictCancelUploadConfirmation: true,
  dictRemoveFile: "Remove",
  dictDefaultMessage: "Drop files or click here to instantly upload",
  headers: {"MyAppname-Service-Type": "Dropzone"}
}






function moveBlock(movingType,classname,obj) {
  var numItems = $('.'+classname).length;
  var listItem = document.getElementById(obj);
  var listNo = $('.'+classname).index(listItem);

  if(numItems > 1) {

  if(movingType == 'before') {
    var newNo = listNo-1;
    if(newNo >= 0) {
      $('#'+obj).insertBefore( $('.'+classname+':eq('+newNo+')' ) );
      //console.log(newNo);
      //ajaxCall('movingBlock','',id,'POST','',movingType,newNo);
    }
  } else if(movingType == 'after') {
    var newNo = listNo+1;
    if(newNo < numItems) {
      $('#'+obj).insertAfter( $('.'+classname+':eq('+newNo+')' ) );
      //ajaxCall('movingBlock','',id,'POST','',movingType,newNo);
    }
  }
  
  //var d = document.getElementsByClassName(classname)
  var photo_ids = []
  
  $('.'+classname).each(function() {
    //console.log( $(this).attr('data-photo-id') )
    photo_ids.push($(this).attr('data-photo-id'))
    //$(this).removeClass('d-none');
    //$(this).addClass('d-'+display);
  });
  
  var params = {
    usr: '<?=$_SESSION[$SYSNAME__.'_'.$CFG_CUST['abbr'].'_'.'username']?>',
    photo_ids: photo_ids,
    type: 'setPhotoOrdering',
  }
  axiosCallReload(params,'./api/product/','noreload')
  console.log( photo_ids )
  
  }
}



function swapDiv(event,elem){
    elem.parentNode.insertBefore(elem,elem.parentNode.firstChild);
}




</script>